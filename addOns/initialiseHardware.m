function vr = initialiseHardware(vr)
% Initialise all VR-related hardware and return the relevant handles in the
% 'vr' structure output. To return the correct outputs, this function needs
% to know:
% 1) which NI card/USB box attached (this is provided by the user as
% a virmen 'variable'.
% 2) which movement hardware the mouse is on (i.e. ball or wheel). This is
% taken to be indicated by the virmen 'movementFunction'.

daqreset; %reset DAQ in case it's still in use by a previous Matlab program
devName = ['Dev1']; % For now, we are always using the 6321, so no need for this. % vr.NIHardwareName];  % Get the name of the NI card.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) Mouse movement input.
if strcmp(  func2str(vr.exper.movementFunction),  'moveWithRotaryEncoder'  )   
    % (1a) Counter input for poition reading %
    vr.daqSessRotEnc = daq.createSession('ni');
    vr.counterCh = vr.daqSessRotEnc.addCounterInputChannel(devName,'ctr0','Position');
    vr.counterCh.EncoderType = 'X4';
    
elseif strcmp(  func2str(vr.exper.movementFunction),  'moveWithTwoMiceUDP'  )
    % (1b) Move in 2D on airball, mouse data from labview is streamed to
    % virmen via UDP.
    vr.XYUDPPorts(1) = udp('127.0.0.1','LocalPort',61557,'DatagramTerminateMode','off');  
    vr.XYUDPPorts(2) = udp('127.0.0.1','LocalPort',61559,'DatagramTerminateMode','off');  
    for ii=1:2;   fopen(vr.XYUDPPorts(ii));   flushinput(vr.XYUDPPorts(ii));   end
    
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2) Analog output to drive reward.
vr.rewardAO = analogoutput('nidaq',devName);
addchannel(vr.rewardAO,0,'voltage');
set(vr.rewardAO,'samplerate',eval(vr.exper.variables.sRateSolanoid));
vr.rewardPattern = [ ones(eval(vr.exper.variables.rewardDropSize),1).*5; 0; 0; 0; 0];
    
% % %%%%% Analog output to drive pump
% vr.pumpAO = analogoutput('nidaq',devName);
% addchannel(vr.pumpAO,1,'voltage');
% set(vr.pumpAO,'samplerate',eval(vr.exper.variables.sRateSolanoid));
% vr.pumpSignal = [ ones(eval(vr.exper.variables.pumpDuration),1).*5; 0; 0; 0; 0];


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (3) 2P Microscope control
% (3a) DIO for microscope start and stop signals %
vr.daqSess2PStartStop = daq.createSession('ni');
vr.daqSess2PStartStop.addDigitalChannel(devName,'port0/line0:1','outputonly');  % One line for on, one for off.
vr.daqSess2PStartStop.outputSingleScan([0 0]);

% (3b) 2P microscope frame clock input %
vr.daqSess2PFrameClock = daq.createSession('ni');
vr.daqSess2PFrameClock.addCounterInputChannel(devName, 'ctr3', 'EdgeCount');  % Use counter 3, as ctr 1 (PFI3) has a broken contact on breakout (as of 2016-12-10), and ctr2 clashes with ctr0 when in 'position' mode.

%%% line clock for flicker control
% vr.daq2PLineClock = daq.createSession('ni');
% vr.daq2PLineClock.Rate = 250000;
% vr.daq2PLineClock.addAnalogInputChannel('Dev2','ai1','Voltage');

% vr.scanLog = [0 0 0 0 0];
% vr.timeLog = [0 0 0 0 0];
% vr.daq2PLineClock = analoginput('nidaq', 'Dev2');
% addchannel(vr.daq2PLineClock ,1, 'Voltage'); %creates the session for daq licking
% set(vr.daq2PLineClock ,'samplerate',10000);
% % set(vr.daq2PLineClock ,'SamplesPerTrigger',11) ;
% 
% vr.flicker_on = 0;
% vr.flicker_trigger = 0;
% vr.flicker_abort = 0;
% 
% vr.flicker_signal = daq.createSession('ni');
% addAnalogOutputChannel(vr.flicker_signal,'Dev2',1,'Voltage');
% vr.flicker_signal.IsContinuous = true;
% vr.flicker_signal.Rate = 10000;
% 
% test = fopen('C:\Users\VR_machine\Documents\log.bin','r')
% [data,count] = fread(test,[2,inf],'double');
% fclose(test)
% vr.triggerPattern = data(2,:);