function [prop_fix_trig, prop_cue_trig] = analysis_generalization(varargin)
close all
if nargin == 0
    [file,path] = uigetfile('D:\MouseData')
else 
    file = varargin{1};
    path = varargin{2};
end
file_root = split(file,'.');
file_root = file_root{1};
filename = [file_root,'.dat'];
cd(path)
tempFid = fopen(filename,'r');
tempData = fread(tempFid,'double');
fclose(tempFid)
tempData = reshape(tempData(2:end),tempData(1),[]);
tempData = tempData';

vr = load(file);
goal_locations = NaN(1,size(vr.exper.worlds,2));
track_lengths = NaN(1,size(vr.exper.worlds,2));

for envs_ind = 1:size(vr.exper.worlds,2)
    var_fields = fieldnames(vr.exper.worlds{envs_ind}.variables);
    for field_list = 1:size(var_fields,1)
        if regexp( var_fields{field_list}, regexptranslate('wildcard','rewardZone*'))
            goal_locations(envs_ind) = eval(getfield(vr.exper.worlds{envs_ind}.variables,var_fields{field_list}));
        end
        if contains(var_fields{field_list},'linearTrackLength')
            track_lengths(envs_ind) = eval(getfield(vr.exper.worlds{envs_ind}.variables,var_fields{field_list}));
        end
    end
    clear var_fields field_list
end


if size(tempData,2) > 13
    cue = 1;
else
    cue = 0;
end
lick_data = [file_root,'_PhotoLicking.data'];
tempFidLick = fopen(lick_data,'r');
lickDat = fread(tempFidLick,[2,inf],'double')';
fclose(tempFidLick)

[~,lickDatInd,vrInd]=intersect(lickDat(:,1),tempData(:,1));
lickArray=zeros(size(tempData,1),1);
lickArray(vrInd)=lickDat(lickDatInd,2);

%define lick bouts
licks = find(lickArray);
lick_mask = zeros(size(lickArray));
lick_mask(licks)= 1;
lick_times = tempData(licks,1);
lick_diff = diff([lick_times(1) lick_times']); % in seconds?

lick_interval_thresh = 0.25;
min_conseq_licks = 3;
potent_bout_onsets = find(lick_diff <= lick_interval_thresh);
lick_mask_temp = zeros(size(lick_diff));
lick_mask_temp(potent_bout_onsets) = 1;
filt_licks = bwareaopen(lick_mask_temp,min_conseq_licks);

filt_lick_ind = find(filt_licks);
onsets = [];
offsets = [];
on_flag = 0;
off_flag = 1;
in_bout = 0;
for vec_ent = 1:size(filt_licks,2)
    if filt_licks(vec_ent) == 1
        if on_flag == 0
            on_flag = 1;
            in_bout = 1;
            onsets  = [onsets,vec_ent];
        end
    elseif filt_licks(vec_ent) == 0
        if in_bout == 1;
            on_flag = 0;
            in_bout = 0;
            offsets = [offsets, vec_ent - 1];
        end
    end
                 
end
onsets = licks(onsets);
offsets = licks(offsets);
if size(onsets,1) > size(offsets,1)
    offsets = [offsets',licks(end)]'
end

onsets_offsets = [onsets, offsets];
lick_bout_vec = zeros(size(lickArray));
for bout = 1:size(onsets_offsets,1)
    lick_range = onsets_offsets(bout,1):onsets_offsets(bout,2);
   lick_bout_vec(lick_range) = 1;
end

lick_mask_final = zeros(size(lickArray));
lick_mask_final(licks(filt_lick_ind))= 1;

figure
plot(tempData(:,7))

figure
hold on
plot(lick_bout_vec,'b','LineWidth',2)
plot(lick_mask,'r')
plot(lick_mask_final,'b')

figure
ax1 = subplot(4,1,1)
plot(tempData(:,3));
ax2 = subplot(4,1,2)
if cue
    plot(tempData(:,14));
end
ax3 = subplot(4,1,3)
plot(tempData(:,9));
hold on
plot(lickArray .* 4)
ax4 = subplot(4,1,4)
if size(tempData,2) > 17
    plot(tempData(:,18));
end
linkaxes([ax1,ax2,ax3],'x')

if cue
    cue_loc = tempData(:,14);
    cue_hist = cue_loc(find(diff(cue_loc)));
    figure
    histogram(cue_hist,'BinWidth',5)
    xlim([0 500])
    set(gca,'fontsize',14)
    xlabel('Cue Position, cm (binned)')
    ylabel('n Trials')
    box off
end

turns_logic_all = tempData(:,11)';
CandRuns=find(diff([1 turns_logic_all]));
RunStarts=find(turns_logic_all(CandRuns)==0);

IndexRuns=1:size(CandRuns,2);
Odds=IndexRuns(1:2:end);
Evens=IndexRuns(2:2:end);

if RunStarts(1)==1;
    
    onsets=CandRuns(Odds);
    offsets=CandRuns(Evens);
else
    onsets=CandRuns(Evens);
   offsets=CandRuns(Odds);
end

if size(onsets,2) ~= size(offsets,2);
    if onsets(1) > offsets(1)
        onsets=([NaN onsets]);
    else
        offsets=([offsets NaN]);
    end
end

turnsOn=reshape(onsets,[],1);
turnsOff=reshape(offsets,[],1);

runs=horzcat(onsets',offsets');

runs(any(isnan(runs),2),:)=[];
run_data_all = rangeConcat(runs);

track_and_licking = intersect(licks,run_data_all);
pos_licking = tempData(track_and_licking,3);
goal_adj_licking = pos_licking;

figure
histogram(pos_licking,'BinWidth',10,'Normalization','probability')

% figure
% plot(tempData(:,3),'k')
% hold on
% for i=1:size(runs,1)
%     line([runs(i,1) runs(i,1)],ylim)
%     line([runs(i,2) runs(i,2)],ylim,'color','r')
% end

figure
hold on
n_lick_trig = 0;
n_fix_trig = 0;
n_cue_trig = 0;

world_frames = tempData(:,12);
env = mode(world_frames(world_frames ~=4));

center_reward_zone = eval(vr.exper.variables.max_cue_reward_dist);
reward_zone_radius = eval(vr.exper.variables.rewardTriggerRadius);
for obj = 1:size(vr.exper.worlds{env}.objects,2)
    if regexp( vr.exper.worlds{env}.objects{obj}.name, regexptranslate('wildcard','rewardZone'))
        break
    end
end

fixed_goal_zone = vr.exper.worlds{env}.objects{obj}.y;
cue_reward_zone_hist = zeros(size(runs,1)-1,41);
distractor_zone_hist = zeros(size(runs,1)-1,41);
cue_zone_hist = zeros(size(runs,1)-1,41);
zone_holder = -100:5:100;
[test_case,test_edgs] = histcounts([-10 -5 0 5 10],[-inf zone_holder]);
reward_zone = find(test_case);

%define where cue is in this hist
test_case_cue = histcounts(-1*center_reward_zone,[-inf zone_holder]);
cue_zone = find(test_case_cue);
for run = 2:size(runs,1)
   lick_lims_min = [];
   lick_lims_max = [];
   if cue
       if mode(tempData(runs(run,1):runs(run,2),15)) == 0 % if cue invisible is off (i.e. not a probe trial)
           cue_on = 1;
           cue_loc_run = tempData(runs(run,1):runs(run,2),14);
           cue_loc_run = mode(cue_loc_run);
           plot(cue_loc_run - center_reward_zone,run*2,'x','color','r','LineWidth',2);
           x = [cue_loc_run-reward_zone_radius cue_loc_run+reward_zone_radius cue_loc_run+reward_zone_radius cue_loc_run-reward_zone_radius];
           y = [(run*2)-0.5 (run*2)-0.5 (run*2)+0.5 (run*2)+0.5];
           r=patch(x,y,'red','EdgeColor','none');
           r.FaceAlpha = 0.4;
       else
           cue_on = 0;
           x = [0 700 700 0 ];
           y = [(run*2)-0.5 (run*2)-0.5 (run*2)+0.5 (run*2)+0.5];
           r=patch(x,y,'yellow','EdgeColor','none');
           r.FaceAlpha = 0.4;
       end
   end
   if size(tempData,2) >= 18
       if mode(tempData(runs(run,1):runs(run,2),15)) == 0
           mov_cue_loc_run = tempData(runs(run,1):runs(run,2),18);
           mov_cue_loc_run = mode(mov_cue_loc_run);
           plot(mov_cue_loc_run,run*2,'x','color','b','LineWidth',2);
           clear mov_cue_loc_run
       end
   end
   
   pos_dat_run = tempData(runs(run,1):runs(run,2),3);
   lick_dat_run = lickArray(runs(run,1):runs(run,2));
   if cue_on
       lick_frames = find(lick_dat_run);
       if ~isempty(lick_frames)
        pos_licks_frames = pos_dat_run(lick_frames) - cue_loc_run;
        cue_reward_zone_hist(run,:) = histcounts(pos_licks_frames,[-inf zone_holder]);
       end
   end
       
       
   s1 = scatter(pos_dat_run, lick_dat_run*(run*2),'filled','k','MarkerFaceAlpha',0.2);
   s1.MarkerFaceAlpha = 0.2;
   if size(tempData,2) > 16
       lick_trig = tempData(runs(run,1):runs(run,2),17);
       lick_trig = find(lick_trig);
       if ~isempty(lick_trig)
           lick_trig_pos = pos_dat_run(lick_trig);
           for lick_n = 1:length(lick_trig_pos)
               if lick_trig_pos(lick_n) >= fixed_goal_zone -reward_zone_radius
                   n_fix_trig = n_fix_trig + 1;
               else
                   n_cue_trig = n_cue_trig + 1;
               end
           end
           n_lick_trig = n_lick_trig + 1;
           for lick = 1:size(lick_trig,1)
               s2 = plot(pos_dat_run(lick_trig(lick)),lick_dat_run(lick_trig(lick))*(run*2),'.','Color','g','Markersize',20);
               s3 = plot(pos_dat_run(lick_trig(lick)),lick_dat_run(lick_trig(lick))*(run*2),'o','Color','k');
           end
       end
   end
   
   clear pos_dat_run lick_dat_run lick lick_trig cue_loc_run lick_trig_pos lick_n
end


curr_y = ylim;
ylim([1 curr_y(2)])

list=get(gca,'YTick');
list=list / 2;
ylab=cell(1,length(list));
for lab_ind=1:length(list)
    ylab{lab_ind}=num2str(list(lab_ind));
end

set(gca,'YTickLabel',ylab)
set(gca,'fontsize',14)

    
ax = gca;
ax.YDir = 'reverse'
set(gca,'fontsize',14)
xlabel('Position, cm')
ylabel('Trial #')
ax.LineWidth = 2;

figure
imagesc(cue_reward_zone_hist)
ax = gca
ylim_n = ax.YLim;
hold on
line([reward_zone(1) reward_zone(1)],[0 ylim_n(2)],'Color','w','LineWidth',2)
line([reward_zone(end) reward_zone(end)],[0 ylim_n(2)],'Color','w','LineWidth',2)
line([cue_zone cue_zone],[0 ylim_n(2)],'Color','r','LineWidth',2) 

test = sum(cue_reward_zone_hist)

prop_lick_trig = n_lick_trig/size(runs,1)
prop_fix_trig = n_fix_trig /size(runs,1)
prop_cue_trig = n_cue_trig/size(runs,1)