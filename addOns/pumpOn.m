function pumpOn
%Run pump


pumpAO = analogoutput('nidaq','VR_6321');

% open solanoid
addchannel(pumpAO,1,'voltage');
putdata(pumpAO,5);   
start(pumpAO);

disp('#################  Press any key to exit!  #################');
pause

% close solanoid
putdata(pumpAO,0);   
start(pumpAO);

end
