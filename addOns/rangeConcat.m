function rangeArray= rangeConcat(array)

for i=1:size(array,1)
    if i ==1
        rangeArray=[array(1,1):array(1,2)]';
    else
        range_row=[array(i,1):array(i,2)]';
        rangeArray=vertcat(rangeArray,range_row);
    end
end


end