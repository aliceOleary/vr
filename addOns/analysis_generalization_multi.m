clear all
[file,path] = uigetfile('S:\DBIO_BarryLab_DATA\aliceO\DATA\Virmen','MultiSelect','On')
prop_trig = zeros(size(file,2),1)
prop_cue =  zeros(size(file,2),1)

for file_n = 1:size(file,2)
    temp = split(file{file_n},'_');
    date(file_n) = str2num(temp{2});
    mouse_name = temp{1};
    clear temp
end

[sorted_dates,ind] = sort(date,'ascend');
file = file(ind)
for file_n = 1:size(file,2)
    [prop_trig(file_n), prop_cue(file_n)] = analysis_generalization(file{file_n},path);
end

figure
plot(prop_trig,'-o','LineWidth',2)
hold on
plot(prop_cue,'-o','LineWidth',2)
legend('Fixed Goal','Cue Goal')
ylabel('Prop Rewards = Lick-triggered')
box off
xticks(1:length(file))

xlab=cell(1,length(file));
for lab_ind=1:length(file)
    xlab{lab_ind}=num2str(sorted_dates(lab_ind));
end
set(gca,'XTickLabel',xlab)
xtickangle(45)
title(mouse_name)