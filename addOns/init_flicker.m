function vr = init_flicker(vr)
disp('init fake fliker');
vr.flicker_trigger = 0;
vr.flicker_on = 1;
long_flicker = repmat(vr.triggerPattern,1,2)';
lh = addlistener(vr.flicker_signal,'DataRequired', @(src,event) src.queueOutputData(long_flicker));
queueOutputData(vr.flicker_signal,long_flicker)
startBackground(vr.flicker_signal);

end