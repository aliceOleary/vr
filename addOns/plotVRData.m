function plotVRData(saveFig,legacyFlag)
%
%   plots VR behavioural data after UI input
%   saveFig = true/false
%   legacyFlag = true/false

%legacy call for track only data
if legacyFlag
    plotVRDataLegacy(saveFig);
    return;
end


dir2save = cd; 
maxPosY = 250;

xOffset = 0;
yOffset = 0;
%UI input for data - make sure files are ordered by date (new - old) in
%folder or plots not be in chronological order
[fName,path] = uigetfile('*.dat','Select VR data to plot','MultiSelect','on');

%fail gracefully
if ~iscell(fName) && ~ischar(fName)
    warning('Loading cancelled! AHHHHHHHH');
    return
end
%multi/single file input
if iscell(fName)
    fName = fliplr(fName); %make sure files are ordered by date
else
    fName = cellstr(fName);
end

%for title str ident
strInd = strfind(fName{1},'_');
%open canvas
figure('name',[fName{1}(1:strInd(1)-1) '_VRData_part1'],'units','normalized','outerposition',[0.01 0.01 0.98 0.95]);

for f = 1:size(fName,2)
    %open file
    tempFid = fopen([path fName{f}],'r');
    tempData = fread(tempFid,'double');
    fclose(tempFid);
    %format data
    tempData = reshape(tempData(2:end),tempData(1),[]);
    tempData = tempData';
    %index for holding environment samples
    indHP = tempData(:,12) == 1;
    telePortInd = diff(tempData(:,12)) ~= 0; %teleport points
    tempData(telePortInd,3) = NaN; %set to NaN (for ease of plotting transitions)
    %absolute distance
    distHP = nansum(sqrt(diff(tempData(indHP,3)).^2))/100;
    distLIN = nansum(sqrt(diff(tempData(~indHP,3)).^2))/100;
    %plot
    axes('position',[0.04+xOffset 0.75-yOffset 0.3 0.2]);
    %plot HP data
    plot(tempData(indHP,1),tempData(indHP,3),'--','linewidth',1.0,'color',[0.5 0.5 0.5]);
    hold on
    %plot lin track data
    plot(tempData(~indHP,1),tempData(~indHP,3),'k-','linewidth',1.5); 
    %plot rewards
    plot(tempData(tempData(:,9) == 1,1),tempData(tempData(:,9) == 1,3),'ro','linestyle','none');
    %plot teleports
    plot([tempData(telePortInd,1),tempData(telePortInd,1)]',[zeros(sum(telePortInd),1),repmat(maxPosY,sum(telePortInd),1)]','r:');   
    hold off
    rangeTime = 0:2:floor(max(tempData(:,1))/60);
    xTickLabel = cellstr(num2str(rangeTime(:)))';
    xTickLabelLoc = rangeTime * 60;
    set(gca,'ylim',[0,maxPosY],'xlim',[0 max(tempData(:,1))],'xtick',xTickLabelLoc,'xticklabel',xTickLabel);
    ylabel('position');
    xlabel('time (min)');
    title([fName{f}(1:strInd(2)-1) ' HP: ' num2str(round(distHP)) 'm; Track: ' num2str(round(distLIN)) 'm'],'interpreter','none');
    
    %bump offsets
    if yOffset >= 0.6
        yOffset = 0;
        xOffset = xOffset + 0.35;    
    else
        yOffset = yOffset + 0.3;
    end
    
    if xOffset > 0.7
        %open new canvas if all full
        nFigs = findobj('-regexp','Name','\w*VRData');
        figure('name',[fName{1}(1:strInd(1)-1) '_VRData_part' num2str(length(nFigs)+1)],'units','normalized','outerposition',[0.01 0.01 0.98 0.98]);
        %reset offsets
        yOffset = 0;
        xOffset = 0;
    end 
end
if saveFig
   nFigs = findobj('-regexp','Name','\w*VRData');
   for n = 1:length(nFigs)
       figure(nFigs(n).Number);
       set(gcf,'paperorientation','landscape');
       print(gcf,'-painters','-loose','-fillpage','-dpdf',[dir2save '\' nFigs(n).Name '.pdf']); 
   end
end
end

