function vr = sample_line_scan(vr)

    % line clock is + 5 when scanning line. otherwise < 0.1
    frame_rate = 30;
    frame_dur = 1./ frame_rate;
    
    sampleVolts = getsample(vr.daq2PLineClock);
    tmp = [vr.timeElapsed];
    scanning = sampleVolts > 1 ;
    vr.scanLog = [vr.scanLog(2: end) scanning];
    vr.timeLog = [vr.timeLog(2: end) tmp];
    if ~any(vr.scanLog) 
        %disp('no scanning');
        if vr.flicker_on == 0;
            vr.flicker_trigger = 1;
        end
    else
        %disp('scanning');
        if vr.flicker_on == 1;
            vr.flicker_abort = 1;
        end
            
    end
end