function [ vr ] = RotaryEncoderSettings( vr )
%%  Sets rotary encoder 

vr.RotaryEncoder.daqSessRotEnc = daq.createSession('ni'); % opens the session...
vr.RotaryEncoder.counterCh = vr.RotaryEncoder.daqSessRotEnc.addCounterInputChannel('Dev2', 'ctr0', 'Position');
vr.RotaryEncoder.counterCh.EncoderType = 'X4';%it was X4 from TW...


end

