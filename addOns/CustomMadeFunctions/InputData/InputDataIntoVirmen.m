function [vr ] = InputDataIntoVirmen( vr)
%Will ask for some information where to save and store the data
%recorded...

vr.Path = ['C:\Users\Giulio\Documents\'];
vr.FileStorePath = ['Z:\giulioC\Data\VR\'];
%% Trial information...
vr=InsertRatNameAndTrial(vr);
vr.RatName = [vr.TrialInfo.Answers{find(strcmp(vr.TrialInfo.Prompt,'Mouse:'))} '\'];
vr.Day = [vr.TrialInfo.Answers{find(strcmp(vr.TrialInfo.Prompt,'Date:'))} '\'];
vr.Session = [vr.TrialInfo.Answers{find(strcmp(vr.TrialInfo.Prompt,'Session:'))} '\'];
%vr.DirectoryName = [vr.Path vr.Experiment '\' vr.RatName vr.Day 'Session' vr.Session] ; %
vr.DirectoryName = [vr.Path vr.Experiment '\' vr.RatName vr.Day ] ; 
%vr.FileStoreDirectoryName = [vr.FileStorePath vr.Experiment '\' vr.RatName vr.Day 'Session' vr.Session] ; 
vr.FileStoreDirectoryName = [vr.FileStorePath vr.Experiment '\' vr.RatName vr.Day ] ; 



%% Behaviour information...
vr=InsertBehaviourRequired(vr);
vr.DetectLicking = [str2double(vr.BehaviourInfo.Answers{find(strcmp(vr.BehaviourInfo.Prompt, 'Detect licking:'))}) ];
vr.RewardIfLicking = [str2double(vr.BehaviourInfo.Answers{find(strcmp(vr.BehaviourInfo.Prompt, 'Include Licking Criteria:'))}) ];
vr.MaxNumberOfLicks = [str2double(vr.BehaviourInfo.Answers{find(strcmp(vr.BehaviourInfo.Prompt, 'Max number of licks:' ))}) ];
vr.RewardWindowWidth = [str2double(vr.BehaviourInfo.Answers{find(strcmp(vr.BehaviourInfo.Prompt, 'Reward Window Width:' ))}) ];
if strcmp(vr.Experiment,'Multicompartment');
vr.DiscriminateRewardedAreas = [str2double(vr.BehaviourInfo.Answers{find(strcmp(vr.BehaviourInfo.Prompt, 'Discriminate the rewarded areas:' ))}) ];
end
%% Create directories...
mkdir(vr.DirectoryName);
mkdir(vr.FileStoreDirectoryName);
cd(vr.DirectoryName);
end

