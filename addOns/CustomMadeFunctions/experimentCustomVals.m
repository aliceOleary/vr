function [vr]=experimentCustomVals(vr)
experimentCustomVals.debugMode=1;
experimentCustomVals.interTrialDelay=2; %gap between trials set to 2 seconds
experimentCustomVals.movementGain=1;
experimentCustomVals.monitorAspectRatio=1.77
experimentCustomVals.perspectiveScale=1.8;
experimentCustomVals.wheelCircumference=56.5;
experimentCustomVals.default2PhotonRecordingDuration=300;
experimentCustomVals.rewardDropSize=2;
experimentCustomVals.maxPossRewardDropSize=10;
experimentCustomVals.sRateSolanoid=20;
end