%% 
Parameters.MotherDirectory = 'Z:\giulioC\Data\VR\';
Parameters.FlagFigure = 0;
Parameters.SaveFlagFigure=1;
Parameters.surfaces = {'VR' 'OpenField' } ;
Parameters.Experiment = 'Multicompartment';
Parameters.Mouse ='m2924' ;
Parameters.Date = '20171017';
Parameters.RunKKWick = 0 ;
Vars = daVarsStruct ;
Parameters.FilePath = [ Parameters.MotherDirectory Parameters.Experiment '\' Parameters.Mouse  '\' Parameters.Date  '\' ];
cd(Parameters.FilePath);

list_name_set = dir([Parameters.FilePath '*.set']);
SetFiles  = natsort(list_trials( list_name_set )); clear list_name_set;

for iFile = 1 : length(SetFiles)
SetPath{iFile} = [Parameters.FilePath SetFiles{iFile}(1:end-4)];
end

%isdir('kwiktint') 
if exist('kwiktint') ==7 & Parameters.RunKKWick
Parameters.dlg_title = 'KK warning';
Parameters.num_lines = 1;
Parameters.Prompt  = {'Run Automated KK via Tint again?'};
defaultans = {num2str(Parameters.RunKKWick)};
Answers =  (inputdlg(Parameters.Prompt,Parameters.dlg_title,Parameters.num_lines ,defaultans) );
Parameters.RunKKWick = str2double(Answers{1});
end
    
if Parameters.RunKKWick
kwiktint([],[],[SetFiles{1}(1:end-4)],SetFiles,SetPath)
end;

[VRs,OpenField,FilePath] = SynchVRAndAxonaEphys(Parameters.Experiment,Parameters.Mouse,Parameters.Date ,0);
Sessions = fields(VRs);
for Session = 1 : numel(Sessions)
%eval(['VR = VRs.' Sessions{Session} ';' ]);
eval(['VRs.' Sessions{Session} '.pos.xy_cm(find(VRs.' Sessions{Session} '.pos.xy_cm(:,1)<0),1)=0;' ]);
eval(['VRs.' Sessions{Session} '.pos.xy_cm(:,2) = rand(size(VRs.' Sessions{Session} '.pos.xy_cm(:,1),1),1) ;' ])
eval(['EEG.Data.VR_' Sessions{Session}  '= VRs.' Sessions{Session} ';' ]);
eval(['EEG.Surfaces.VR_' Sessions{Session}  ' = [' char(39) 'VR_' Sessions{Session} char(39) '];' ]);
end
EEG.Data.OpenField = OpenField;
EEG.Surfaces.OpenField = 'OpenField';
EEG.Control = 'OpenField';
cd(Parameters.FilePath )
Parameters.ThetaFigurePath = [FilePath 'Theta\' ]; if isdir(Parameters.ThetaFigurePath );cmd_rmdir('Theta');end;         mkdir(Parameters.ThetaFigurePath);
Parameters.CellsFigurePath = [FilePath 'Cells\' ]; if isdir(Parameters.CellsFigurePath );cmd_rmdir('Cells');end;mkdir(Parameters.CellsFigurePath);
Parameters.LickingFigurePath = [FilePath 'Licking\' ]; if isdir(Parameters.LickingFigurePath );cmd_rmdir('Licking');end;mkdir(Parameters.LickingFigurePath);
Parameters.PopulationEnsembleFigurePath = [FilePath 'PopulationEnsemble\' ]; if isdir(Parameters.PopulationEnsembleFigurePath );cmd_rmdir('PopulationEnsemble');end;mkdir(Parameters.PopulationEnsembleFigurePath);

%% Analysis of LFP theta...

[~, MEC_VR_OpenFieldThetaBehaviourStats,EEG] = ReadAllFloorWallOpenFieldEEGDataSet (EEG,Parameters.Mouse,Parameters.FlagFigure,num2str(1),Parameters.ThetaFigurePath,{Parameters.Date},1,[],Parameters.SaveFlagFigure,'Mouse','PowerSpectrum');
LickColor = Vars.VR.Display.LickColor;
SpikeColor = Vars.VR.Display.SpikeColor;SpikeDotSize = 2;RateMapLineWidth = 2;
TrackColor = Vars.VR.Display.TrackColor;TrackLineSize = 1;
RewardColor = Vars.VR.Display.RewardColor;
InsideRewardAreaLick= Vars.VR.Display.InsideRewardAreaLick;
OutsideRewardAreaLick= Vars.VR.Display.OutsideRewardAreaLick;
if strcmp(VRs.Session_1.Virmen.vr.exper.name,'MultiCompartmentExperiment' );XTickForSpikePlot = Vars.VR.Display.XTickForSpikePlot;else;XTickForSpikePlot = 100;end

%% Detect boundaries of the environments....
VR = VRs.Session_1;
Compartments = VR.Virmen.vr.EnvironmentSettings.Evironments( find(cellfun(@isempty,strfind(VR.Virmen.vr.EnvironmentSettings.Evironments,'Pipe'))));
Boundaries = [];
ForEachCompartmentVisits = {};
RewardedEnvironmentIndex = [];
WorldWithRewardArea = VR.Virmen.vr.WorldIndexWithReward;
TotalVisits = 0;
if ~isfield(VR.pos,'NumberOfVisits') & strcmp(VR.Virmen.vr.Experiment,'Multicompartment')
Boundaries =[50 350;400 700;750 1050;1100 1400];
ForEachCompartmentVisits ={'A_1','B_1','B_2','C_1'};
RewardedArea(:,:,1) =[530   570;630   670];
RewardedArea(:,:,2) =[880   920;980  1020];
Compartments = {'A'    'B'    'C'};
else
for iEnvironment  = 1 : numel(Compartments)

MaxVisits = unique( VR.pos.NumberOfVisits( VR.pos.Environment== VR.Virmen.vr.EnvironmentSettings.LabelEnvironment(find(strcmp(VR.Virmen.vr.EnvironmentSettings.Evironments ,Compartments{iEnvironment})))));   
for  iVisit = 1 : max( MaxVisits)
TotalVisits = TotalVisits + 1;
Boundaries = [Boundaries; 10*round(FindExtremesOfArray(VR.pos.xy_cm( ...
 find(VR.pos.Environment== VR.Virmen.vr.EnvironmentSettings.LabelEnvironment(find(strcmp(VR.Virmen.vr.EnvironmentSettings.Evironments ,Compartments{iEnvironment}))) ...
& VR.pos.NumberOfVisits == iVisit) ))/10)] ; 
ForEachCompartmentVisits{end+1} = [Compartments{iEnvironment} '_' num2str(iVisit) ];
if strcmp(Compartments{iEnvironment},VR.Virmen.vr.WorldNameWithReward)
RewardedEnvironmentIndex =[RewardedEnvironmentIndex;TotalVisits];
end
%eval(['RateMap.Compartments.Compartment_' Compartments{iEnvironment} '_Visit_' num2str(iVisit) '= RateMap.map ( FindElementsWithMultipleBoundaries(RateMap.Pos, Boundaries(end,:),' char(39) 'inside' char(39) ') )  ;' ])
end
end
end
%%%
if strcmp(VRs.Session_1.Virmen.vr.exper.name,'MultiCompartmentExperiment' )
GoalCueAreasInRewardedEnvironment = [   VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y - VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.radius ...
                                        VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y + VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.radius  ] ;
end

for iEnvironment = 1 : numel(RewardedEnvironmentIndex)                                  
for iReward = 1 : VR.Virmen.vr.NumberOfCues 
RewardedArea(:,:,iEnvironment) = Boundaries(RewardedEnvironmentIndex(iEnvironment),1) + GoalCueAreasInRewardedEnvironment ;
end
end
                                   
%% Produce rate maps of cells...
AllCells = [];
for Tetrode = 1 : numel(VRs.Session_1.tetrode)
    Clusters  = [];
    for iSession = 1 : numel(Sessions)
    eval(['Clusters = [Clusters;[unique(VRs.Session_' num2str(iSession) '.tetrode(Tetrode).cut)] ];' ]);
    end
    Clusters = unique(Clusters); Clusters =Clusters(~ismember(Clusters,0));
    for Cluster = 1: length(Clusters) ;
    Cell = Clusters(Cluster) ;
 
    figure(1);SpikeWaweforms.Ch1 = [];SpikeWaweforms.Ch2 = [];SpikeWaweforms.Ch3 = [];SpikeWaweforms.Ch4 = [];
    RateMaps = [];
    for iSession = 1 : numel(Sessions)
    %% Spike plots...
        subaxis(3,numel(Sessions)+2,iSession,...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
    
    eval(['VR = VRs.Session_' num2str(iSession) ';' ]);SpikePlotForVR(VR,Tetrode,Cell,SpikeColor,SpikeDotSize,TrackColor,TrackLineSize,Boundaries);subplot1 = gca;title([ Sessions{iSession}(1:end-2) ' ' num2str(iSession)]);
    %% Rate maps.....
    subaxis(3,numel(Sessions)+2,iSession+numel(Sessions)+2, ...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
    RateMap = RateMapForVR(VR,Tetrode,Cell,SpikeColor,RateMapLineWidth,Boundaries,Compartments,ForEachCompartmentVisits) ;
    
    subaxis(3,numel(Sessions)+2,iSession+(numel(Sessions)+2)*2, ...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);

    RateMap =ComputeCorrelationsBetweenCompartments(RateMap,VR);
 
    eval(['RateMaps.Session_' num2str(iSession) '= RateMap ;' ]);
    
    for iChannel = 1 : 4;
        eval(['SpikeWaweforms.Ch' num2str(iChannel) ' = [SpikeWaweforms.Ch' num2str(iChannel) '; VRs.Session_' num2str(iSession) '.tetrode(Tetrode).ch' num2str(iChannel) '(find(VRs.' Sessions{iSession} '.tetrode(Tetrode).cut == Cell),:)] ;' ]) 
    end
    end;
    %% Open Field
    %Spike plot...
    subplot(4,numel(Sessions)+2,numel(Sessions)+1);SpikePos = OpenField.tetrode(Tetrode).pos_sample( find(OpenField.tetrode(Tetrode).cut == Cell));plot(OpenField.pos.xy_cm(:,1),OpenField.pos.xy_cm(:,2),'Color',TrackColor);axis tight;hold on ;axis square;axis off;scatter(OpenField.pos.xy_cm(SpikePos,1),OpenField.pos.xy_cm(SpikePos,2),'MarkerFaceColor',[SpikeColor],'MarkerEdgeColor',[SpikeColor],'SizeData',SpikeDotSize) ; title([ 'OF']);
    %... and rate map
    subplot(4,numel(Sessions)+2,+2+iSession+numel(Sessions)+1);in_UNsm_rm = make_in_struct_for_rm (OpenField,Tetrode,Cell,50,5,[Vars.rm.binSizePosCm],[Vars.rm.binSizeDir],0,'rate',0,0) ;in = in_UNsm_rm;RateMap = GetRateMap ( in); ProduceRM(flipud(RateMap.map'));title([ 'Rate map' ]);axis tight square;axis off;set(gca,'XTickLabel', strread(num2str([get(gca,'XTick')*Vars.rm.binSizePosCm]),'%s'))
    %  and HD
    subplot(4,numel(Sessions)+2,(+2+iSession+numel(Sessions)+1)+numel(Sessions)+2);in_UNsm_rm = make_in_struct_for_rm (OpenField,Tetrode,Cell,50,5,[Vars.rm.binSizePosCm],[Vars.rm.binSizeDir],1,'dir',0,0) ;in = in_UNsm_rm;PolarMap = GetRateMap ( in);% ProduceRM(flipud(RateMap.map'));title([ 'Rate map' ]);axis tight square;axis off;set(gca,'XTickLabel', strread(num2str([get(gca,'XTick')*Vars.rm.binSizePosCm]),'%s'))

    subplot(4,numel(Sessions)+2,(+2+iSession+numel(Sessions)+1)+numel(Sessions)+2+6);PlotCorrelationValuesBetweenCompartmentsAcrossSessions(RateMaps)
    
    AllCells =[AllCells;RateMaps];
    
    
    for iChannel = 1 : 4;
    eval(['SpikeWaweforms.Ch' num2str(iChannel) ' = [SpikeWaweforms.Ch' num2str(iChannel) '; OpenField.tetrode(Tetrode).ch' num2str(iChannel) '(find(OpenField.tetrode(Tetrode).cut == Cell),:)] ;' ])    
    subaxis(4,numel(Sessions)+2,(numel(Sessions)+2)*iChannel,'SpacingVertical',0.05,'SpacingHorizontal',0.05, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',.05,'MarginRight',.05,'MarginTop',.05,'MarginBottom',.05)
    in.title =['Ch ' num2str(iChannel) ];
    eval(['in.meanFreqPerBin = nanmean(SpikeWaweforms.Ch' num2str(iChannel) ');' ]);
    eval(['in.semFreqPerBin = nanstd(SpikeWaweforms.Ch' num2str(iChannel) ');' ]);
    in.linecolor = 'r';in.xlim =[-.2 .3];in.ylim =[-150 150];in.xlabel = '';in.ylabel = '';in.bins= linspace(-0.2,0.3,50);XYSemArea(in);set(gca,'XTick',[]);set(gca,'YTick',[]);axis off;
    end
   
    clear subplot1;
    %% Save figure...
    title_Cell = [Parameters.Mouse '_' Parameters.Date '_' Parameters.Experiment '_Tetrode_' num2str(Tetrode) '_Cell_' num2str(Cell) ];
    set(gcf,'NumberTitle','off','Name',[title_Cell ],'PaperOrientation','landscape','PaperUnits','normalized','PaperPosition', [0 0 1 1]);
    cd(Parameters.CellsFigurePath );saveas(gcf,title_Cell,'pdf');disp (['Figures from ' title_Cell ' properly saved and stored into correct directory'])
    close;clear iSession SpikeWaweforms;  
    end
end
%% 

%% Produce lick maps...
AllLickMap = [];
for iSession = 1 : numel(Sessions)
eval(['VR = VRs.' Sessions{iSession} ';' ])

subaxis(4,numel(Sessions),iSession+numel(Sessions)*0,...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);

VR = ProduceLickPlot(VR,TrackColor,OutsideRewardAreaLick,InsideRewardAreaLick,RewardColor,VR.Virmen.vr.WorldIndexWithReward,Boundaries,RewardedArea) ; 
    axis tight ; set(gca,'XTick',[min(round([get(gca,'XLim')])):XTickForSpikePlot:max(round([get(gca,'XLim')])) ],'YTick',[min(round([get(gca,'YLim')])) : XTickForSpikePlot :  max(round([get(gca,'YLim')])) ]);

subaxis(4,numel(Sessions),iSession+ numel(Sessions)*(1),...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
[VR,LickMap] = ProduceLickMap(VR,TrackColor,Vars,1,Boundaries,RewardedArea,ForEachCompartmentVisits) ;
subaxis(4,numel(Sessions),iSession+ numel(Sessions)*(2),...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
ProduceTemporalLickPlot(VR,1,10);

subaxis(4,numel(Sessions),iSession+ numel(Sessions)*(3),...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
    LickMap =ComputeCorrelationsBetweenCompartments(LickMap,VR);
    
    
    AllLickMap=[AllLickMap;LickMap];

end
title_Cell = [Parameters.Mouse '_' Parameters.Date '_' Parameters.Experiment ];
set(gcf,'NumberTitle','off','Name',title_Cell,'PaperOrientation','landscape','PaperUnits','normalized','PaperPosition', [0 0 1 1]) %,');
cd(Parameters.LickingFigurePath);saveas(gcf,title_Cell,'pdf');disp (['Figures from ' title_Cell ' properly saved and stored into correct directory']);clear title_Cell;
close;
%% Produce population ensemble maps and compare to behaviour...
CellsToBehaviourCorrelation =[];
for iSession = 1 : numel(Sessions)

subaxis(3,numel(Sessions),iSession,...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);
ProduceRM( nanmean(ConcatentateIntoStructureIndices(AllCells,['Session_' num2str(iSession) '.'],'Correlations',[],4),3),[-.4 .4]);colorbar;
set(gca,'XTick', [1:numel(ForEachCompartmentVisits)],'XTickLabel',ForEachCompartmentVisits, 'YTick', [1:numel(ForEachCompartmentVisits)],'YTickLabel',ForEachCompartmentVisits);
title([' All cells (Session ' num2str(iSession) ')'])

subaxis(3,numel(Sessions),iSession+numel(Sessions),...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom);

ProduceRM( (ConcatentateIntoStructureIndices(AllLickMap,['.'],'Correlations',[iSession],0))',[-.4 .4]);colorbar;
set(gca,'XTick', [1:numel(ForEachCompartmentVisits)],'XTickLabel',ForEachCompartmentVisits, 'YTick', [1:numel(ForEachCompartmentVisits)],'YTickLabel',ForEachCompartmentVisits);
title([' Licking (Session ' num2str(iSession) ')'])

CellsToBehaviourCorrelation =[CellsToBehaviourCorrelation; nancorr( ...
nanmean(ConcatentateIntoStructureIndices(AllCells,['Session_' num2str(iSession) '.'],'Correlations',[],4),3) , ...
ConcatentateIntoStructureIndices(AllLickMap,['.'],'Correlations',[iSession],0)' ,'Pearson')] ;   

end;
subaxis(3,numel(Sessions)+1,3+(1+numel(Sessions))*2,...
        'SpacingVertical',Vars.VR.Display.SpacingVertical,'SpacingHorizontal',Vars.VR.Display.SpacingHorizontal, ...
        'PaddingLeft',0,'PaddingRight',0,'PaddingTop',0,'PaddingBottom',0, ...
        'MarginLeft',Vars.VR.Display.MarginLeft,'MarginRight',Vars.VR.Display.MarginRight,'MarginTop',Vars.VR.Display.MarginTop,'MarginBottom',Vars.VR.Display.MarginBottom+0.02);
plot([1:numel(Sessions)],[CellsToBehaviourCorrelation],'color','k');hold on;p=plot([1:numel(Sessions)],[CellsToBehaviourCorrelation],'.','Color','r','MarkerSize',30);line([0 numel(Sessions)+1],[0 0],'LineStyle','--','Color','b');xlim([0 numel(Sessions)+1]);ylim([-1.2 1.2 ]);xlabel('Sessions');ylabel('Correlation');title('Cells to behaviour');set(gca,'XTick',[1 : numel(Sessions)]);
title_Cell = [Parameters.Mouse '_' Parameters.Date '_' Parameters.Experiment ];
set(gcf,'NumberTitle','off','Name',title_Cell,'PaperOrientation','landscape','PaperUnits','normalized','PaperPosition', [0 0 1 1]) %,');
cd(Parameters.PopulationEnsembleFigurePath);saveas(gcf,title_Cell,'pdf');disp (['Figures from ' title_Cell ' properly saved and stored into correct directory']);clear title_Cell;
close;

%%

