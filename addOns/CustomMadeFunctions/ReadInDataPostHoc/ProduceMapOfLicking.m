function LickMap = ProduceMapOfLicking(VR,varargin)
if numel(varargin)==0;
    FlagFigure=1;
FieldToPlot = 'spike';
elseif numel(varargin)==1;
    FlagFigure=varargin{1};
FieldToPlot = 'spike';;
elseif numel(varargin)==2;
    FlagFigure=varargin{1};
    FieldToPlot=varargin{2};
    
end


Vars= daVarsStruct;
in_UNsm_rm = make_in_struct_for_rm (VR,1,1,Vars.pos.sampleRate,Vars.rm.smthKernPos,Vars.rm.binSizePosCm,Vars.rm.binSizeDir ,1,'rate',0,0);

%VR.Virmen.Licking.PhotoLicking.pos


LickIndices = [find(diff((VR.Licking.Voltage < VR.Virmen.vr.PhotoLickDetection.VoltageThreshold ))>0)]+1 ;


in = in_UNsm_rm;in.spikePosInd= LickIndices; in.PLOT_ON=0;
VR.pos.xy_cm(:,2) = rand(numel(VR.pos.xy_cm(:,2)),1);
LickMap = GetRateMap ( in);LickMap.map =  LickMap.map' ;   LickMap.dwell =  LickMap.dwell' ;   LickMap.nodwell =  LickMap.nodwell' ;   LickMap.spike =  LickMap.spike' ;   
if FlagFigure
    
eval(['imagesc(LickMap.' FieldToPlot ');' ]);
%xlabel('pos');
set(gca,'XTickLabel', strread(num2str([get(gca,'XTick')*Vars.rm.binSizePosCm]),'%s')');
set(gca,'YTick',[]);
colormap('jet');

end
end



%BinSpeedOscillation     = intrinsic_freq_autoCorr(in);
