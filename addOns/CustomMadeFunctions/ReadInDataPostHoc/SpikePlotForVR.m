function SpikePlotForVR(VR,Tetrode,Cell,SpikeColor,SpikeDotSize,TrackColor,TrackLineSize,Boundaries);
Vars = daVarsStruct;
SpikePos = VR.tetrode(Tetrode).pos_sample( find(VR.tetrode(Tetrode).cut == Cell)  ) ; 
plot(VR.pos.xy_cm(:,1),VR.pos.ts,'Color' ,TrackColor, 'LineWidth' ,TrackLineSize);
hold on ;
s= scatter(VR.pos.xy_cm(SpikePos,1),VR.pos.ts(SpikePos),'MarkerFaceColor' ,[SpikeColor], 'MarkerEdgeColor' ,[SpikeColor], 'SizeData' ,[SpikeDotSize]) ; 
VRs.pos.xy_cm(:,2)= rand(1,length(VR.pos.xy_cm(:,1)));
xlabel('pos (cm)'); ylabel('time (s)');axis tight;hold on ;




% %% Draw lines for corresponding evnrionemnts...
% Compartments = {'A' 'B' 'C'};
% Boundaries = [];
% for iEnvironment  = 1 : numel(Compartments)
% MaxVisits = unique( VR.pos.NumberOfVisits( VR.pos.Environment== VR.Virmen.vr.EnvironmentSettings.LabelEnvironment(find(strcmp(VR.Virmen.vr.EnvironmentSettings.Evironments ,Compartments{iEnvironment})))));   
% for  iVisit = 1 : max( MaxVisits)
%  
% Boundaries = [Boundaries; 10*round(FindExtremesOfArray(VR.pos.xy_cm( ...
%  find(VR.pos.Environment== VR.Virmen.vr.EnvironmentSettings.LabelEnvironment(find(strcmp(VR.Virmen.vr.EnvironmentSettings.Evironments ,Compartments{iEnvironment}))) ...
% & VR.pos.NumberOfVisits == iVisit) ))/10)] ; 
% 
% end
% end


for iBound = 1: numel(Boundaries)
 hold on;
line([Boundaries(iBound) Boundaries(iBound)],[FindExtremesOfArray(VR.pos.ts)],...
    'Color','k','LineStyle','--');

end


axis tight ;
%set(gca,'XTick',round([min(VR.pos.xy_cm(:,1)) : Vars.VR.Display.XTickForSpikePlot : max(VR.pos.xy_cm(:,1)) ]),'YTick',round([min(VR.pos.ts(:,1)) : Vars.VR.Display.YTickForSpikePlot : max(VR.pos.ts(:,1)) ]));
set(gca,'XTick',[sort(Boundaries(1:2:end)) Boundaries(end) ],'YTick',round([min(VR.pos.ts(:,1)) : Vars.VR.Display.YTickForSpikePlot : max(VR.pos.ts(:,1)) ]));
end
    