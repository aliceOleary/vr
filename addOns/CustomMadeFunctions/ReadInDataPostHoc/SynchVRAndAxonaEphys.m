function [GrandVR,OpenField,FilePath] = SynchVRAndAxonaEphys(varargin)

if nargin == 0
prompt = {'Experiment:' 'Mouse:' 'Date:' 'Session:' 'VROnly' };
dlg_title = 'Input';
num_lines = 1;
defaultans = {'Training' 'm2924'  '20170731','2','1'};
Answers = inputdlg(prompt,dlg_title,num_lines,defaultans);
Experiment = Answers{1};
Mouse = Answers{2};
Date =  Answers{3};
Session = Answers{4};
VROnly = str2double(Answers{5});
elseif nargin == 4 ; 
Experiment = varargin{1};
Mouse = varargin{2};
Date =  varargin{3};
VROnly = (varargin{4}) ;
    
elseif nargin == 5 ; 
Experiment = varargin{1};
Mouse = varargin{2};
Date =  varargin{3};
Session = varargin{4};
VROnly = (varargin{5}) ;
end    

%% Preliminary Script To Synch Axona TTL with VR
Vars =daVarsStruct;
Parameters.MotherPath = 'Z:\giulioC\Data\VR\';
%FilePath =  [Parameters.MotherPath Experiment '\' Mouse '\' Date '\Session' num2str(Session) '\' ] ;
FilePath =  [Parameters.MotherPath Experiment '\' Mouse '\' Date '\'] ;
FileName = dir([FilePath '*.set']) ;
FileName  = list_trials( FileName ); % creates a series of set files...
if numel(FileName)>1 & VROnly==0; ; % it means there are more than one trial - i.e. open field and VR trial...
for iFileName = 1 : numel(FileName);FileNameSet{iFileName}= FileName{iFileName}(1:end-4);end;
cd(FilePath);
FileNameCut = dir([FilePath '*.cut']) ;   
%% Remove extra cut fileds....
RemoveTrials = [];
    for iFile =1 :numel(FileNameCut)
    FileCutName = textscan(char(FileNameCut(iFile).name), '%s' , 'Delimiter', 'VR');
    if numel(FileCutName{1})> 1
    RemoveTrials = [RemoveTrials; iFile];
    end
    end   
   FileNameCut(RemoveTrials) = [];
%% Now load Matlab file output from Virmen...
MatlabData = [];
FileMatlabName = dir([FilePath '*.mat']) ;
for iFile = 1 : numel(FileMatlabName)
tmpFileMatlabName= FileMatlabName(iFile).name;%(1:end-4);
load(tmpFileMatlabName);
eval(['MatlabData.' tmpFileMatlabName(1:end-4) ' = Data;' ])
clear Data tmpFileMatlabName;
end
%% Make sure the order of the cut file is right...
[~, ~, ~,OrderFileNameCut] = read_cut_file(FileNameCut(1).name) ; 
Input = repmat('%s ',1,numel(FileName))     ; 
OrderFileNameCut = textscan(char(OrderFileNameCut), Input , 'Delimiter', ','); clear Input;
if numel(FileName) ~= numel(OrderFileNameCut); disp(['Number of trials do not match...' ]); return; end;
for iFileName = 1 : numel(FileName);
  FileName{iFileName} = [ char(OrderFileNameCut{iFileName}) '.set'];
end
CombinedSessions = readAllDACQdata(FilePath,FileName) ; disp(['Combined trials loaded together...'])
if sum(diff(CombinedSessions.pos.ts) <= 0)>0;
    CombinedSessions.pos.ts = [0:Vars.pos.sampleRate^-1 : sum(CombinedSessions.pos.trial_duration+1)]'; 
    CombinedSessions.pos.ts(length(CombinedSessions.pos.xy_cm)+1:end)= [];
end
FirstTrialDuration = CombinedSessions.pos.trial_duration(1) ;
FirstTrialDurationPosNumber = FirstTrialDuration * Vars.pos.sampleRate ;
OriginalNumberOfPosNumber = numel(CombinedSessions.pos.ts) ;
%% Check EEG is right already...
for iEEG = 1 : numel(CombinedSessions.EEG)
if numel(CombinedSessions.EEG(iEEG).EEG) < OriginalNumberOfPosNumber* Vars.eeg.sampleRate/Vars.pos.sampleRate
CombinedSessions.EEG(iEEG).EEG(end+1:OriginalNumberOfPosNumber* Vars.eeg.sampleRate/Vars.pos.sampleRate) = NaN ;
end;
CombinedSessions.EEG(iEEG).EEG = CombinedSessions.EEG(iEEG).EEG(1:OriginalNumberOfPosNumber* Vars.eeg.sampleRate/Vars.pos.sampleRate);
end
%%
        OpenField = CombinedSessions; %% Create OpenField structure
        VR = CombinedSessions;        %% Create VR....
        VR.Virmen = MatlabData; clear MatlabData;
        
%%      Remove extra stuff from OpenField...
            %% Pos data...
            OpenField.pos.trial_duration = OpenField.pos.trial_duration(1);
            OpenField.pos.dir_coherent = OpenField.pos.dir;
            SubFields = fields(OpenField.pos); 
            for iSubField = 1 : numel(SubFields) ; 
            if eval( [ ' length(OpenField.pos.' SubFields{iSubField} ') == OriginalNumberOfPosNumber ' ])       
            eval( [ 'OpenField.pos.' SubFields{iSubField} '(FirstTrialDurationPosNumber+1:end,:) = []; ' ]) ;
            end
            end
            OpenField.pos.SurfPos = [1 : length(OpenField.pos.xy_cm)]; clear SubFields  iSubField;
            %% EEG data...
            for iEEG = 1 : numel(OpenField.EEG)
            OpenField.EEG(iEEG).EEG = OpenField.EEG(iEEG).EEG(1: FirstTrialDurationPosNumber * Vars.eeg.sampleRate/Vars.pos.sampleRate ) ;
            end
            %% Tetrodes...
            for Tetrode = 1 : numel(OpenField.tetrode)
            NSpikes = length(OpenField.tetrode(Tetrode).ts);
            SpikesToRemove = find(OpenField.tetrode(Tetrode).ts > FirstTrialDuration) ;
            TetrodeFields = fields(OpenField.tetrode(Tetrode));
            for iField = 1 : numel(TetrodeFields);
            if eval([ 'length(OpenField.tetrode(Tetrode).' TetrodeFields{iField} ') == NSpikes' ])
            eval(['OpenField.tetrode(Tetrode).' TetrodeFields{iField} '(SpikesToRemove,:) = [];' ]);            
            end
            end
            clear NSpikes SpikesToRemove TetrodeFields iField
            OpenField.tetrode(Tetrode).duration = OpenField.pos.trial_duration;
            end
            
            %OpenField.tetrode(Tetrode).ts(SpikesToRemove) = [];
            %OpenField.tetrode(Tetrode).pos_sample(SpikesToRemove) = [];
%             if ~isempty(OpenField.tetrode(Tetrode).cut);
%             OpenField.tetrode(Tetrode).cut= reshape(OpenField.tetrode(Tetrode).cut,[],[1]);
%             OpenField.tetrode(Tetrode).cut(SpikesToRemove) = [];
%             end
%              ;
%             clear SpikesToRemove;
%             end
        %% Now remove data from the VR...
          %% Pos data...
          
%for iSession =1 : numel( fields(Grand_VR.Virmen))         
   %iSession =1 ;
   %VR = [CombinedSessions]; 
   VR.pos.trial_duration = CombinedSessions.pos.trial_duration(2:end);
   SubFields = fields(VR.pos);
   TmpPos = zeros(sum(CombinedSessions.pos.trial_duration)*Vars.pos.sampleRate,1);
   
   
   for iSubField = 1 : numel(SubFields) ; 
   if eval( [ ' length(VR.pos.' SubFields{iSubField} ') == OriginalNumberOfPosNumber ' ])       
   eval( [ 'VR.pos.' SubFields{iSubField} '(1:FirstTrialDurationPosNumber,:) = []; ' ]) ;
   end
   end
   VR.pos.ts = VR.pos.ts - VR.pos.ts(1);%+Vars.pos.sampleRate^-1;
   VR.Ephys=VR.pos;
%% EEG data...
            for iEEG = 1 : numel(VR.EEG)
            VR.EEG(iEEG).EEG(1: FirstTrialDurationPosNumber * Vars.eeg.sampleRate/Vars.pos.sampleRate ) = []; 
            end
         %% Tetrodes...
         for Tetrode = 1 : numel(VR.tetrode)
            NSpikes = length(VR.tetrode(Tetrode).ts);
            SpikesToRemove = find(VR.tetrode(Tetrode).ts < FirstTrialDuration) ;          
            TetrodeFields = fields(VR.tetrode(Tetrode));
            for iField = 1 : numel(TetrodeFields);
            if eval([ 'length(VR.tetrode(Tetrode).' TetrodeFields{iField} ') == NSpikes' ])
            eval(['VR.tetrode(Tetrode).' TetrodeFields{iField} '(SpikesToRemove,:) = [];' ]);            
            end
            end
%             VR.tetrode(Tetrode).ts(SpikesToRemove) = [];
%             VR.tetrode(Tetrode).ts = VR.tetrode(Tetrode).ts-FirstTrialDuration;
%             VR.tetrode(Tetrode).pos_sample(SpikesToRemove) = [];
%             if ~isempty(VR.tetrode(Tetrode).cut);
%             VR.tetrode(Tetrode).cut(SpikesToRemove) = [];
%             end
            VR.tetrode(Tetrode).duration = VR.pos.trial_duration ;
            clear SpikesToRemove NSpikes TetrodeFields;
         end
        
        
else % if there is no open field data inside...
  %% Find VR trial...
  VROnly=1;
    RemoveTrials = [];
  for iFile =1 :numel(FileName)
  
    OrderFileNameCut = textscan(char(FileName(iFile)), '%s' , 'Delimiter', 'VR');
    if numel(OrderFileNameCut{1}) ==1
    RemoveTrials = [RemoveTrials; iFile];
    end
    end   
   FileName(RemoveTrials) = [];
    
FileName= char(FileName)
FileName =FileName(1:end-4) ;
cd(FilePath);
FileMatlabName = dir([FilePath '*.mat']) ;
FileMatlabName= FileMatlabName.name;
load( FileMatlabName)
VR = readDACQdataVR ( FilePath,FileName,Data );
%VR.Ephys.ts=[Vars.pos.sampleRate^-1: Vars.pos.sampleRate^-1:size(VR.Ephys.led_pos,1)/Vars.pos.sampleRate]';
OpenField = [];
end
%%
clear SubFields Tetrode iSubField iFileName CombinedSessions dlg_title defaultans

VirmenSessions = fields(VR.Virmen) ;
N_RecordedSessions = numel(VirmenSessions);
ID_RecordedSessions = [];

AcrossSessionsPos = [];

for iSession = 1 : N_RecordedSessions
eval(['ID_RecordedSessions(iSession) = str2double(VR.Virmen.' VirmenSessions{iSession}  '.vr.Session(1));' ]);
end

for iSession  = 1 : numel(VR.pos.trial_duration)
AcrossSessionsPos = [AcrossSessionsPos;    repmat(iSession,  VR.pos.trial_duration((iSession))*Vars.pos.sampleRate,1)];
end


for iSession = 1 : N_RecordedSessions

%% Adjust Pos
%ID_RecordedSessions
GoodPos = find(AcrossSessionsPos == ID_RecordedSessions(iSession) );
tmpVR = VR;
littmpVR.Ephys.trial_duration = VR.Ephys.trial_duration(ID_RecordedSessions(iSession));
PosFields = fields(tmpVR.Ephys);
for iField = 1 : numel(PosFields)
if eval(['length(tmpVR.Ephys.' PosFields{iField} ') == length(AcrossSessionsPos);' ])
eval(['tmpVR.Ephys.' PosFields{iField} ' = tmpVR.Ephys.' PosFields{iField} '(GoodPos,:) ;' ]);end
if strcmp(PosFields{iField},'ts');FirstTs = [tmpVR.Ephys.ts(1)];end
end

%% Adjust tetrodes

for Tetrode = 1 : numel(VR.tetrode)
tmpVR.tetrode(Tetrode).ts = tmpVR.tetrode(Tetrode).ts  - tmpVR.tetrode(Tetrode).ts(1);
SpikesToKeep = find(tmpVR.tetrode(Tetrode).ts > tmpVR.Ephys.ts(1) & tmpVR.tetrode(Tetrode).ts < tmpVR.Ephys.ts(end)) ;
NSpikes = numel(tmpVR.tetrode(Tetrode).ts) ;
TetrodeFields = fields(tmpVR.tetrode(Tetrode));
for iField = 1 : numel(TetrodeFields)
if eval(['size(tmpVR.tetrode(Tetrode).' TetrodeFields{iField}  ',1) == NSpikes;' ])
   eval(['tmpVR.tetrode(Tetrode).' TetrodeFields{iField}  ' = tmpVR.tetrode(Tetrode).' TetrodeFields{iField} '(SpikesToKeep,:) ;' ])
end           
end
% tmpVR.tetrode(Tetrode).ts = tmpVR.tetrode(Tetrode).ts(SpikesToKeep); 
% tmpVR.tetrode(Tetrode).cut = tmpVR.tetrode(Tetrode).cut(SpikesToKeep); 
% tmpVR.tetrode(Tetrode).duration = tmpVR.Ephys.trial_duration;
% tmpVR.tetrode(Tetrode).pos_sample = tmpVR.tetrode(Tetrode).pos_sample(SpikesToKeep); 
clear SpikesToKeep NSpikes TetrodeFields
tmpVR.tetrode(Tetrode).duration = tmpVR.Ephys.trial_duration;
end

%% Adjust EEG
for iEEG = 1 : numel(tmpVR.EEG)
 if numel(tmpVR.EEG(iEEG).EEG)/ 5 < OriginalNumberOfPosNumber ; tmpVR.EEG(iEEG).EEG(end: OriginalNumberOfPosNumber*50) = NaN;    end

    tmpVR.EEG(iEEG).EEG = tmpVR.EEG(iEEG).EEG(1: OriginalNumberOfPosNumber* Vars.eeg.sampleRate / Vars.pos.sampleRate);
    tmpVR.EEG(iEEG).EEG=reshape(tmpVR.EEG(iEEG).EEG,[],Vars.eeg.sampleRate / Vars.pos.sampleRate);
    tmpVR.EEG(iEEG).EEG = tmpVR.EEG(iEEG).EEG(GoodPos,:);
    tmpVR.EEG(iEEG).EEG = reshape(tmpVR.EEG(iEEG).EEG,[1],[]);
end

%% Select right Virmen Session....
eval(['tmpVR.Virmen = tmpVR.Virmen.' VirmenSessions{iSession} ' ;' ]);

%% Now resets pos and spike ts...
for Tetrode = 1 : numel(VR.tetrode)
tmpVR.tetrode(Tetrode).ts = tmpVR.tetrode(Tetrode).ts  - tmpVR.Ephys.ts(1) ;
end
tmpVR.Ephys.ts = tmpVR.Ephys.ts  - tmpVR.Ephys.ts(1) ;
tmpVR.flnmroot = VR.flnmroot{ID_RecordedSessions(iSession) +1} ;
%%

[ times, type, chan ] = read_INP_file( [FilePath tmpVR.flnmroot '.inp'] );

%eval([ 'SynchAxona = times(chan==tmpVR.Virmen.' VirmenSessions{iSession} '.vr.AxonaSynch.AxonaSystemUnitChannel);' ]);
SynchAxona = times(chan==tmpVR.Virmen.vr.AxonaSynch.AxonaSystemUnitChannel);


%% Adjust Pos

OriginalPos = numel(tmpVR.Ephys.ts);
PosTsToDelete = find(tmpVR.Ephys.ts < SynchAxona(1) |tmpVR.Ephys.ts > SynchAxona(end) );
tmpVR.Ephys.ts(PosTsToDelete)=[];
tmpVR.Ephys.ts = tmpVR.Ephys.ts - SynchAxona(1);
tmpVR.Ephys.ts=tmpVR.Ephys.ts;
tmpVR.Ephys.led_pos(PosTsToDelete,:) = [];
tmpVR.Ephys.led_pix(PosTsToDelete,:) = [];
tmpVR.Ephys.xy_pixels(PosTsToDelete,:) = [];
tmpVR.Ephys.xy_cm(PosTsToDelete,:) = [];
tmpVR.Ephys.dir(PosTsToDelete,:) = [];
tmpVR.Ephys.speed(PosTsToDelete,:) = [];
%% Correct EEG...
for iEEG = 1 : numel(VR.EEG)
 if numel(VR.EEG(iEEG).EEG)/ 5 < OriginalPos ; VR.EEG(iEEG).EEG(end: OriginalPos*50) = NaN;    end

    tmpVR.EEG(iEEG).EEG=VR.EEG(iEEG).EEG(1: OriginalPos* Vars.eeg.sampleRate / Vars.pos.sampleRate);
    tmpVR.EEG(iEEG).EEG=reshape(tmpVR.EEG(iEEG).EEG,[],Vars.eeg.sampleRate / Vars.pos.sampleRate);
    tmpVR.EEG(iEEG).EEG(PosTsToDelete,:)=[];
    tmpVR.EEG(iEEG).EEG = reshape(tmpVR.EEG(iEEG).EEG,[1],[]);
end
%% Adjust tetrodes... (correct across all tetrodes)....
for Tetrode=1:numel(tmpVR.tetrode)
InitialNumberOfSpikes = size(tmpVR.tetrode(Tetrode).ts,1);
ToDelete = find(tmpVR.tetrode(Tetrode).ts > SynchAxona(end) | tmpVR.tetrode(Tetrode).ts < SynchAxona(1) );
%LastPos = find( VR.tetrode(Tetrode).ts < SynchAxona(1) , 1, 'last');
Fields = fields(tmpVR.tetrode(Tetrode));
for iField = 1 : numel(Fields);
if eval(['size(tmpVR.tetrode(Tetrode).' Fields{iField} ',1) == InitialNumberOfSpikes' ])
eval(['tmpVR.tetrode(Tetrode).' Fields{iField} '(ToDelete,:) = [];']);
end
end
tmpVR.tetrode(Tetrode).ts = tmpVR.tetrode(Tetrode).ts -[SynchAxona(1)] ;
tmpVR.tetrode(Tetrode).pos_sample = ceil(tmpVR.tetrode(Tetrode).ts * Vars.pos.sampleRate);
%% Correcte for overhanging spikes...
OverHangingSpikes = find( tmpVR.tetrode(Tetrode).pos_sample > length(tmpVR.Ephys.xy_cm));
DeletedNumberOfSpikes = size(tmpVR.tetrode(Tetrode).ts,1);


for iField = 1 : numel(Fields);
if eval(['size(tmpVR.tetrode(Tetrode).' Fields{iField} ',1) == DeletedNumberOfSpikes' ])
eval(['tmpVR.tetrode(Tetrode).' Fields{iField} '(OverHangingSpikes,:) = [];']);
end
end

clear OverHangingSpikes DeletedNumberOfSpikes  Fields InitialNumberOfSpikes  ToDelete  iField         ;
%tmpVR.tetrode(Tetrode).duration = tmpVR.te
end;

%% Convert into cm... = Move the VR.pos data into the Ephys pos reference...
[~,~,bin_id] = histcounts(tmpVR.Ephys.ts,tmpVR.Virmen.pos.ts) ;bin_id = bin_id (bin_id~= 0) ;
[Y] = discretize(tmpVR.Ephys.ts,tmpVR.Virmen.pos.ts);Y = find(~isnan(Y));

tmpVR.Ephys.xy_cm = NaN(numel(tmpVR.Ephys.ts),1);
tmpVR.Ephys.xy_cm(Y) = tmpVR.Virmen.pos.pos(bin_id);
tmpVR.Ephys.Trial = NaN(numel(tmpVR.Ephys.ts),1);
tmpVR.Ephys.Trial(Y) = tmpVR.Virmen.pos.Trial(bin_id);
tmpVR.Ephys.Environment = NaN(numel(tmpVR.Ephys.ts),1);
tmpVR.Ephys.Environment(Y) = tmpVR.Virmen.pos.Environment(bin_id);
if isfield(tmpVR.Virmen.pos,'NumberOfVisits');
tmpVR.Ephys.NumberOfVisits = NaN(numel(tmpVR.Ephys.ts),1);
tmpVR.Ephys.NumberOfVisits(Y) = tmpVR.Virmen.pos.NumberOfVisits(bin_id);
end;

% VR.Ephys.xy_cm = NaN(numel(VR.Ephys.ts),1);
% VR.Ephys.xy_cm(bin_id) = VR.Virmen.pos.pos(bin_id);
% VR.Ephys.Trial = NaN(numel(VR.Ephys.ts),1);
% VR.Ephys.Trial(bin_id) = VR.Virmen.pos.Trial(bin_id);
% VR.Ephys.Environment = NaN(numel(VR.Ephys.ts),1);
% VR.Ephys.Environment(bin_id) = VR.Virmen.pos.Environment(bin_id);
%% Create a Licking structure of the same length as pos...
tmpVR.Licking.ts = tmpVR.Ephys.ts;
tmpVR.Licking.Voltage = NaN(numel(tmpVR.Ephys.ts),1);
%VR.Licking.Voltage(bin_id) = VR.Virmen.Licking.PhotoDetection.Voltage(bin_id);
tmpVR.Licking.Voltage(Y) = tmpVR.Virmen.Licking.PhotoDetection.Voltage(bin_id);

% and interpolate licking data...
missing_lick = find(isnan(tmpVR.Licking.Voltage)) ;
ok_lick = find(~isnan(tmpVR.Licking.Voltage));   
if ~isempty(missing_lick)
tmpVR.Licking.Voltage(missing_lick) = interp1(ok_lick,  tmpVR.Licking.Voltage(ok_lick), missing_lick, 'linear');
tmpVR.Licking.Voltage(missing_lick(missing_lick>max(ok_lick))) = tmpVR.Licking.Voltage(max(ok_lick));
tmpVR.Licking.Voltage(missing_lick(missing_lick<min(ok_lick))) = tmpVR.Licking.Voltage(min(ok_lick));
end
%% Interpolate missing pos...%% Pos not smoothed yet...
missing_pos = find(isnan(tmpVR.Ephys.xy_cm)) ;
ok_pos = find(~isnan(tmpVR.Ephys.xy_cm));   
if ~isempty(missing_pos)
tmpVR.Ephys.xy_cm(missing_pos) = interp1(ok_pos,  tmpVR.Ephys.xy_cm(ok_pos), missing_pos, 'linear');
tmpVR.Ephys.xy_cm(missing_pos(missing_pos>max(ok_pos))) = tmpVR.Ephys.xy_cm(max(ok_pos));
tmpVR.Ephys.xy_cm(missing_pos(missing_pos<min(ok_pos))) = tmpVR.Ephys.xy_cm(min(ok_pos));
end
%%

%% Speed worked out
tmpVR.Ephys.speed = diff(tmpVR.Ephys.xy_cm) * Vars.pos.sampleRate ; tmpVR.Ephys.speed(end+1) = NaN;
tmpVR.Ephys.xy_cm(:,2) = ones(1,numel(tmpVR.Ephys.xy_cm)) ;
tmpVR.Ephys.led_pos = tmpVR.Ephys.xy_cm;


%% Create new structure and remove that...
tmpVR.pos = tmpVR.Ephys;
tmpVR = rmfield(tmpVR,'Ephys');
tmpVR.pos.SurfPos = [1 : length(tmpVR.pos.xy_cm)]';
tmpVR.pos.dir = zeros(1, length(tmpVR.pos.xy_cm))';
tmpVR.pos.dir_coherent = tmpVR.pos.dir;
get_working_pc_and_cd;
eval(['GrandVR.Session_' num2str(ID_RecordedSessions(iSession))  '= tmpVR;' ]);
clear tmpVR;

end


end


% %subplot(1,2,1);plot(mtint.Ephys.ts,mtint.Ephys.xy_cm );
% %subplot(1,2,2);plot(mtint.Ephys.ts,mtint.Ephys.speed);ylim([0 100]);
% %% CreateSpikes =
% %numel(mtint.tetrode.cut)
% 
% 
% Nspikes = 100;
% mtint.tetrode.cut(randi([0 numel(mtint.tetrode.cut)],1,Nspikes))=1;;
% spikePos = mtint.tetrode(3).pos_sample(find(mtint.tetrode(3).cut==1)) ;
% %subplot(1,2,1);
% hold on ; 
% 
% plot(mtint.Ephys.xy_cm,mtint.Ephys.ts,'k');
% hold on;%
% scatter(mtint.Ephys.xy_cm(spikePos),mtint.Ephys.ts(spikePos),'r','filled');
%%
% [mtint.Ephys.ts(1:10) mtint.Vr.pos.ts(1:10)] 
% TimeWindow = [0 SynchAxona(end)-SynchAxona(1)];
% retEphys=hist_perc(mtint.Ephys.ts,Vars.pos.sampleRate^-1/2,[TimeWindow],0);
% retVR=hist_perc(mtint.Vr.pos.ts,Vars.pos.sampleRate^-1/2,[TimeWindow],0);
% 
% mtint.Combined.pos.ts = retEphys.x;
% mtint.Vr.pos.pos(retVR.bin)











