function VR = ProduceLickPlot(VR,TrackColor,OutsideLickColor,InsideLickColor,RewardColor,WorldWithRewardArea,Boundaries,RewardedArea) ; 
Vars = daVarsStruct;


% if ~isempty(WorldWithRewardArea);
% if isfield(VR.Virmen.vr,'RewardWindowWidth')
% RewardWindowWidth = VR.Virmen.vr.RewardWindowWidth;
% else
% RewardWindowWidth = 40;
% end
% if strcmp(VR.Virmen.vr.exper.name,'MultiCompartmentExperiment')
%  %VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue};   
% 
%  %[min,max] =FindExtremesOfArray(VR.pos.xy_cm(VR.pos.Environment == WorldWithRewardArea & VR.pos.NumberOfVisits ==1) ) ;
%   
% % EnvironmentMargins = [FindExtremesOfArray(VR.pos.xy_cm(VR.pos.Environment == WorldWithRewardArea & VR.pos.NumberOfVisits ==1) ) ; ...
% %                 FindExtremesOfArray(VR.pos.xy_cm(VR.pos.Environment == WorldWithRewardArea & VR.pos.NumberOfVisits ==2) ) ...
% %                ] ;
% %   
% GoalCueAreasInRewardedEnvironment = [   VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y - VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.radius ...
%                                         VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y + VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.radius  ] ;
% clear RewardedArea % = NaN(VR.Virmen.vr.NumberOfCues ,2 ,VR.Virmen.vr.NumberOfTimesRewardEnvironmentPresented  );
% Boundaries = [];
% for iEnvironment = 1 : VR.Virmen.vr.NumberOfTimesRewardEnvironmentPresented                                    
% for iReward = 1 : VR.Virmen.vr.NumberOfCues 
% RewardedArea(:,:,iEnvironment) = EnvironmentMargins(iEnvironment,1) + GoalCueAreasInRewardedEnvironment ;
% end
% Boundaries = [Boundaries ; RewardedArea(:,:,iEnvironment)];
% end
% 
% else
% RewardedArea = [...
%     VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y - ...
%     VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.height/2 - (RewardWindowWidth/2), ...
%     VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.y - ...
%     VR.Virmen.vr.exper.worlds{WorldWithRewardArea}.objects{VR.Virmen.vr.worlds{WorldWithRewardArea}.objects.indices.GoalCue}.height/2 + (RewardWindowWidth/2)];
% end;



hold on ;   

for iEnvironment = 1 : VR.Virmen.vr.NumberOfTimesRewardEnvironmentPresented                                    
for iReward = 1 : VR.Virmen.vr.NumberOfCues 
hArea = area(RewardedArea(iReward,:,iEnvironment),[ max(VR.pos.ts) max(VR.pos.ts)]) ;
 set(hArea,'FaceColor',Vars.SpeedModulation.FloorShadeColor,'EdgeColor',Vars.SpeedModulation.FloorShadeColor);
 uistack(hArea,'bottom' );
end
end;


hold on ;
plot(VR.pos.xy_cm(:,1),VR.pos.ts,'Color',TrackColor);
axis tight; hold on ; 

[OutsideArea] = FindElementsWithMultipleBoundaries(VR.pos.xy_cm(:,1),Boundaries,'outside');
[InsideArea] = FindElementsWithMultipleBoundaries(VR.pos.xy_cm(:,1),Boundaries,'inside');
Licks = find(VR.Licking.Voltage < VR.Virmen.vr.PhotoLickDetection.VoltageThreshold);

OutsideLicks = intersect(Licks,OutsideArea) ;
InsideLicks =intersect(Licks,InsideArea) ; 

%InsideLicks = find(VR.Licking.Voltage < VR.Virmen.vr.PhotoLickDetection.VoltageThreshold & [VR.pos.xy_cm(:,1) > RewardedArea(1) & VR.pos.xy_cm(:,1) < RewardedArea(2)]);


plot(VR.pos.xy_cm(OutsideLicks,1),VR.pos.ts(OutsideLicks),'.' ,'Color',OutsideLickColor);
plot(VR.pos.xy_cm(InsideLicks,1),VR.pos.ts(InsideLicks),'.' ,'Color',InsideLickColor);


Rewards = plot(VR.Virmen.Reward.pos,VR.Virmen.Reward.ts,'.' ,'Color',RewardColor);
title('Lick plot');axis ; hold off;xlabel('pos (cm)');ylabel('time (s)');
set(gca,'XTick',[0 : 40: ceil(max(VR.pos.xy_cm(:,1))) ]);


%end

%% Draw lines signaling for different environments...

clear Boundaries;
Boundaries=VR.pos.xy_cm( (find(diff(VR.pos.Environment)~=0)+1),1);
Boundaries = unique(round(Boundaries/10)*10);


for iBound = 1: numel(Boundaries)
 hold on;

 
line([Boundaries(iBound) Boundaries(iBound)],[FindExtremesOfArray(VR.pos.ts)],...
    'Color','k','LineStyle','--');
 
 
%  
% line([round( min(Data.pos.pos(Data.pos.Environment==iWorld))) round( min(Data.pos.pos(Data.pos.Environment==iWorld)))],[min(Data.pos.ts) max(Data.pos.ts)],...
%     'Color','k','LineStyle','--');
% line([round( max(Data.pos.pos(Data.pos.Environment==iWorld))) round( max(Data.pos.pos(Data.pos.Environment==iWorld)))],[min(Data.pos.ts) max(Data.pos.ts)],...
%     'Color','k','LineStyle','--');

%round(max(Data.pos.pos(Data.pos.Environment==iWorld)))
end

set(gca,'XTick',round([min(VR.pos.xy_cm(:,1)) : Vars.VR.Display.XTickForSpikePlot : max(VR.pos.xy_cm(:,1)) ]),'YTick',round([min(VR.pos.ts(:,1)) : Vars.VR.Display.YTickForSpikePlot : max(VR.pos.ts(:,1)) ]));
end

