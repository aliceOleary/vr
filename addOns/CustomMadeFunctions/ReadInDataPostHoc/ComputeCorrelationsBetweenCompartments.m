function RateMap =ComputeCorrelationsBetweenCompartments(RateMap,VR)

BetweenCompartmentsCorrelations = [];
CorrelationsBetweenDifferentEnvironments=[];

Compartments = fields(RateMap.Compartments);

RateMap.Correlations =[];

for iField = 1 : numel(Compartments)
    for jField = 1 : numel(Compartments)
    eval(['in.x  =    RateMap.Compartments.' Compartments{iField} ';' ]);
    eval(['in.y  =    RateMap.Compartments.' Compartments{jField} ';' ]);
    in.PLOT_ON = 0;
    ret = CorrelateXYAndRegression(in);
    RateMap.Correlations(iField , jField) = ret.r; clear in ret;
    if iField == jField  ; RateMap.Correlations(iField , jField) = NaN;end
    end
end


% Labels = [];
% 
% for iLabel = 1 : numel(Compartments)
%     tmp  = textscan((Compartments{iLabel}), '%s' , 'Delimiter', '_') ;    tmp = tmp{1};
%     tmp([find(~cellfun(@isempty,strfind(tmp,'Compartment'))) find(~cellfun(@isempty,strfind(tmp,'Visit')))])=[] ;
%   
%     Labels{iLabel}= strjoin(tmp) ;
% end
RateMap.Correlations(find(triu(RateMap.Correlations)==0)) = NaN ;
h = ProduceRM(RateMap.Correlations,[-1 1]);
set(gca,'XTick', [1:numel(Compartments)],'XTickLabel',Compartments, 'YTick', [1:numel(Compartments)],'YTickLabel',Compartments);
title('Pair comparisons');

CorrelationsBetweenDifferentEnvironments=[CorrelationsBetweenDifferentEnvironments; ...
nancorr( RateMap.Compartments.A_1, RateMap.Compartments.B_1 ,'Pearson') ; ...
nancorr( RateMap.Compartments.A_1, RateMap.Compartments.B_2 ,'Pearson') ; ...
nancorr( RateMap.Compartments.A_1, RateMap.Compartments.B_2 ,'Pearson') ; ...
nancorr( RateMap.Compartments.B_1, RateMap.Compartments.C_1 ,'Pearson') ; ...
nancorr( RateMap.Compartments.B_2, RateMap.Compartments.C_1 ,'Pearson') ] ;
CorrelationsBetweenSameEnvironments = nancorr( RateMap.Compartments.B_1, RateMap.Compartments.B_2 ,'Pearson') ; 

BetweenCompartmentsCorrelations.CorrelationsBetweenDifferentEnvironments = CorrelationsBetweenDifferentEnvironments;
BetweenCompartmentsCorrelations.CorrelationsBetweenSameEnvironments = CorrelationsBetweenSameEnvironments;

RateMap.BetweenCompartmentsCorrelations = BetweenCompartmentsCorrelations;






end






