function [VR,LickMap] = ProduceLickMap(VR,LickColor,Vars,WorldWithRewardArea,Boundaries,RewardedArea,ForEachCompartmentVisits) ; 




Vars = daVarsStruct;

LickMap = ProduceMapOfLicking(VR,0,'map') ; 
PosExtrema = round(FindExtremesOfArray(VR.pos.xy_cm(:,1))) ;
PosToRm = ([PosExtrema(1):Vars.rm.binSizePosCm:PosExtrema(2)])+Vars.rm.binSizePosCm/2 ;
PosToRm(PosToRm > PosExtrema(2))=[];
LickMap.Pos = PosToRm;

plot([PosToRm], LickMap.map(1:numel(PosToRm)),'Color',LickColor,'LineWidth',2);
xlabel('pos (cm)'); ylabel('Lick (Hz)');
title([ 'Lick rate map' ]);
hold on ;   
%%
for iVisit = 1 : numel(ForEachCompartmentVisits)
 
eval(['LickMap.Compartments.' ForEachCompartmentVisits{iVisit} '= LickMap.map ( FindElementsWithMultipleBoundaries(LickMap.Pos, Boundaries(iVisit,:),' char(39) 'inside' char(39) ') )  ;' ])

end


%%
for iEnvironment = 1 : VR.Virmen.vr.NumberOfTimesRewardEnvironmentPresented                                    
for iReward = 1 : VR.Virmen.vr.NumberOfCues 
hArea = area(RewardedArea(iReward,:,iEnvironment),[ max(get(gca,'YLim')) max(get(gca,'YLim'))]) ;
 set(hArea,'FaceColor',Vars.SpeedModulation.FloorShadeColor,'EdgeColor',Vars.SpeedModulation.FloorShadeColor);
 uistack(hArea,'bottom' );
end
end;
%end



hold on ;   


Rewards = VR.Virmen.Reward.pos ;


for iArea = 1 : size(Boundaries,1)                        
[InsideArea] = FindElementsWithMultipleBoundaries(Rewards,Boundaries(iArea,:),'inside');
hReward = area( [nanmean(VR.Virmen.Reward.pos(InsideArea))-StError(VR.Virmen.Reward.pos(InsideArea))  nanmean(VR.Virmen.Reward.pos(InsideArea))+StError(VR.Virmen.Reward.pos(InsideArea)) ] ,[ max(get(gca,'YLim')) max(get(gca,'YLim'))]); 
set(hReward,'FaceColor',Vars.SpeedModulation.FloorShadeColor,'EdgeColor',Vars.SpeedModulation.FloorColor);
%l=line([nanmean(VR.Virmen.Reward.pos(InsideArea)) nanmean(VR.Virmen.Reward.pos(InsideArea))]/,[ 0 max(get(gca,'YLim'))],'Color','r','LineWidth',2);
end 
 
 %% Draw lines ...  
clear Boundaries;
Boundaries=VR.pos.xy_cm( (find(diff(VR.pos.Environment)~=0)+1),1);
Boundaries = unique(round(Boundaries/10)*10);


for iBound = 1: numel(Boundaries)
 hold on;
line([Boundaries(iBound) Boundaries(iBound)],[FindExtremesOfArray(get(gca,'YLim'))],...
    'Color','k','LineStyle','--');
end

set(gca,'XTick',round([min(VR.pos.xy_cm(:,1)) : Vars.VR.Display.XTickForSpikePlot : max(VR.pos.xy_cm(:,1)) ] )  )  ;



end
