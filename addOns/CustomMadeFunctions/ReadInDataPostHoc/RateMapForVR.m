function RateMap = RateMapForVR(VR,Tetrode,Cell,SpikeColor,RateMapLineWidth,Boundaries,Compartments,ForEachCompartmentVisits) ;



 Vars = daVarsStruct;
 in_UNsm_rm = make_in_struct_for_rm (VR,Tetrode,Cell,Vars.pos.sampleRate,5,[Vars.rm.binSizePosCm],[Vars.rm.binSizeDir],0,'rate',0,0);
 in = in_UNsm_rm;RateMap = GetRateMap ( in); 
 
 
 
PosExtrema = round(FindExtremesOfArray(VR.pos.xy_cm(:,1))) ;
PosToRm = ([PosExtrema(1):Vars.rm.binSizePosCm:PosExtrema(2)])+Vars.rm.binSizePosCm/2 ;
PosToRm(PosToRm > PosExtrema(2))=[];
RateMap.Pos = PosToRm;
plot(PosToRm,flipud(RateMap.map(1:numel(PosToRm))'),'Color',SpikeColor,'LineWidth',RateMapLineWidth);
    
 %% Draw lines for corresponding evnrionemnts...
%Compartments = {'A' 'B' 'C'};
%Compartments = VR.Virmen.vr.EnvironmentSettings.Evironments( find(cellfun(@isempty,strfind(VR.Virmen.vr.EnvironmentSettings.Evironments,'Pipe'))));

%Boundaries = [];
for iVisit = 1 : numel(ForEachCompartmentVisits)
 
% Boundaries = [Boundaries; 10*round(FindExtremesOfArray(VR.pos.xy_cm( ...
%  find(VR.pos.Environment== VR.Virmen.vr.EnvironmentSettings.LabelEnvironment(find(strcmp(VR.Virmen.vr.EnvironmentSettings.Evironments ,Compartments{iEnvironment}))) ...
% & VR.pos.NumberOfVisits == iVisit) ))/10)] ; 
% 


eval(['RateMap.Compartments.' ForEachCompartmentVisits{iVisit} '= RateMap.map ( FindElementsWithMultipleBoundaries(RateMap.Pos, Boundaries(iVisit,:),' char(39) 'inside' char(39) ') )  ;' ])

end
%end


for iBound = 1: numel(Boundaries)
 hold on;
line([Boundaries(iBound) Boundaries(iBound)],[FindExtremesOfArray(get(gca,'YLim'))],...
    'Color','k','LineStyle','--');
end
axis tight square;

%set(gca,'XTick',round([min(VR.pos.xy_cm(:,1)) : Vars.VR.Display.XTickForSpikePlot : max(VR.pos.xy_cm(:,1)) ]),'YTick',round([min(VR.pos.ts(:,1)) : Vars.VR.Display.YTickForSpikePlot : max(VR.pos.ts(:,1)) ]));
set(gca,'XTick',[sort(Boundaries(1:2:end)) Boundaries(end) ],'YTick',round([min(VR.pos.ts(:,1)) : Vars.VR.Display.YTickForSpikePlot : max(VR.pos.ts(:,1)) ]));
xlabel('pos (cm)'); ylabel('FR (Hz)');
title([ 'Rate map' ]);YTicks = [  ceil(max(RateMap.map))  ] ;
if YTicks == 0;YTicks = 1; end;set(gca,'YTick',[0   YTicks],'YLim',[0 YTicks]);  


end