clf;
figure(1);
t =plot(Data.pos.pos,Data.pos.ts);
xlabel('position (cm)');
ylabel('time (s)');
hold on ;
LEGEND = [];LEGENDNames = [];
%% scatter Axona Synch time points...
Ax = scatter(Data.AxonaSynch.pos,Data.AxonaSynch.ts,'c','filled');
if ~isnan(Ax.XData);
    LEGEND=[LEGEND ;Ax];
    LEGENDNames ={'Axona'};
end;
%% scatter licks by means of raw voltage.....
PhotoLicking = find(Data.Licking.PhotoDetection.Voltage< Data.vr.PhotoLickDetection.VoltageThreshold);
PhotoLicking(PhotoLicking>length(Data.pos.pos)) = [];
ls = scatter(Data.pos.pos(PhotoLicking),Data.pos.ts(PhotoLicking));set(ls,'MarkerEdgeColor','g','SizeData',2);clear PhotoLicking;
%% ... and licking events in a given window....
l = scatter(Data.Licking.PhotoLicking.pos,Data.Licking.PhotoLicking.ts,'g','filled');
if sum(~isnan(l.XData))>0;
    LEGEND=[LEGEND; l];
    LEGENDNames(end+1) ={'PhotoLick'};
end;

%% scatter rewards...
r = scatter(Data.Reward.pos,Data.Reward.ts,'r','filled');
if ~isnan(r.XData);
    LEGEND=[LEGEND;r];
     LEGENDNames(end+1) ={'Reward'};
end
%%
% BatteryLicking = find(Data.Licking.BatteryDetection.Voltage< Data.vr.BatteryLickDetection.VoltageThreshold);
% bs = scatter(Data.pos.pos(BatteryLicking),Data.pos.ts(BatteryLicking));set(bs,'MarkerEdgeColor','b','SizeData',2);clear BatteryLicking;
% %% ... and licking events in a given window....
% bl = scatter(Data.Licking.BatteryLicking.pos,Data.Licking.BatteryLicking.ts,'b','filled');
% if sum(~isnan(bl.XData))>0;
%     LEGEND=[LEGEND; bl];
%     LEGENDNames(end+1) ={'BatteryLick'};
% end;


%% Scatter Errors (i.e. when white box was shown...)
Error = scatter(Data.vr.WhiteWallPosOnset,Data.vr.WhiteWallTimeOnset,'k','filled');
if sum(~isnan(Error.XData))>0;
    LEGEND=[LEGEND; Error];
    LEGENDNames(end+1) ={'Error'};
end;
%% Insert legend...
hold off;
axis tight;axis square;
legend(LEGEND,LEGENDNames,'box','off','Location','NorthEastOutside')
%%
% figure(2);plot(Data.Licking.PhotoDetection.ts,Data.Licking.PhotoDetection.Voltage)
% hold on ;
% plot(Data.Licking.BatteryDetection.ts,Data.Licking.BatteryDetection.Voltage)
% 
% ylim([0 15]);
% 
% scatter([Data.Reward.ts],3*ones(size(Data.Reward.ts,1),1),'g','filled')
% scatter([Data.Licking.BatteryLicking.ts],Data.vr.BatteryLickDetection.VoltageThreshold*ones(size(Data.Reward.ts,1),1),'r','filled')







%% Draw boundaries if any...

Boundaries=Data.pos.pos(find(diff(Data.pos.Environment)~=0)+1);
Boundaries = unique(round(Boundaries/10)*10);


for iBound = 1: numel(Boundaries)
 hold on;

 
line([Boundaries(iBound) Boundaries(iBound)],[min(Data.pos.ts) max(Data.pos.ts)],...
    'Color','k','LineStyle','--');
 
 
%  
% line([round( min(Data.pos.pos(Data.pos.Environment==iWorld))) round( min(Data.pos.pos(Data.pos.Environment==iWorld)))],[min(Data.pos.ts) max(Data.pos.ts)],...
%     'Color','k','LineStyle','--');
% line([round( max(Data.pos.pos(Data.pos.Environment==iWorld))) round( max(Data.pos.pos(Data.pos.Environment==iWorld)))],[min(Data.pos.ts) max(Data.pos.ts)],...
%     'Color','k','LineStyle','--');

%round(max(Data.pos.pos(Data.pos.Environment==iWorld)))
end
TitleFig = [ Data.vr.RatName(1:end-1) '_' Data.vr.Day(1:end-1) '_VR_' Data.vr.Experiment '_Session_' Data.vr.Session(1:end-1) ] ;
saveas(gcf,TitleFig,'pdf');




