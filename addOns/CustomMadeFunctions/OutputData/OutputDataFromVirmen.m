function OutputDataFromVirmen(vr)
Data = [];
% open the binary file
BehaviourData = fopen('Behaviour.data','r');
% read all data from the file into the matrix
Behaviour= transpose(fread(BehaviourData,[6,Inf],'double'));
Data.pos.ts = Behaviour(:,1); 
Data.pos.interval = [diff(Data.pos.ts);NaN]; 
Data.pos.pos = Behaviour(:,2); 

Data.pos.speed = Behaviour(:,3); 
Data.pos.speed(1)=Data.pos.speed(1+1);
Data.pos.Environment = Behaviour(:,4); 
Data.pos.Trial = Behaviour(:,5); 
Data.pos.NumberOfVisits = Behaviour(:,6); 

Data.pos.dp = Data.pos.interval.*Data.pos.speed;
Data.pos.dp(end) = Data.pos.dp(end-1);
Data.pos.CumulativeDistance = nansum(abs(Data.pos.dp)); % Cumulative distance (abs to get rid of direction (if negative it would be backward)...
clear Behaviour;fclose(BehaviourData);clear BehaviourData;

%% PhotoLicking...
PhotoLickingData = fopen('PhotoLicking.data','r');
PhotoLicking = transpose(fread(PhotoLickingData,[2,Inf],'double'));
if isempty(PhotoLicking)
   Data.Licking.PhotoLicking.ts = [NaN];
   Data.Licking.PhotoLicking.pos = [NaN];
else
   Data.Licking.PhotoLicking.ts = PhotoLicking(:,1);
   Data.Licking.PhotoLicking.pos = PhotoLicking(:,2);
end;
Data.Licking.PhotoLicking.pos(Data.Licking.PhotoLicking.ts>Data.pos.ts(end)) = [];
Data.Licking.PhotoLicking.ts(Data.Licking.PhotoLicking.ts>Data.pos.ts(end)) = [];

clear PhotoLicking; fclose(PhotoLickingData);clear PhotoLickingData;
                    %% and photo data...
PhotoData = fopen('PhotoLickingDetection.data','r');
Photo = transpose(fread(PhotoData,[2,Inf],'double'));
if isempty(Photo)
   Data.Licking.PhotoDetection.ts = [NaN];
   Data.Licking.PhotoDetection.Voltage = [NaN];
   
else
   Data.Licking.PhotoDetection.ts = Photo(:,1);
   Data.Licking.PhotoDetection.Voltage = Photo(:,2);
end  
clear Photo; fclose(PhotoData);clear PhotoData;
%% BatteryLicking
% BatteryLickingData = fopen('BatteryLicking.data','r');
% BatteryLicking = transpose(fread(BatteryLickingData,[2,Inf],'double'));
% if isempty(BatteryLicking)
%    Data.Licking.BatteryLicking.ts = [NaN];
%    Data.Licking.BatteryLicking.pos = [NaN];
% else
%    Data.Licking.BatteryLicking.ts = BatteryLicking(:,1);
%    Data.Licking.BatteryLicking.pos = BatteryLicking(:,2);
% end;
% Data.Licking.BatteryLicking.pos(Data.Licking.BatteryLicking.ts>Data.pos.ts(end)) = [];
% Data.Licking.BatteryLicking.ts(Data.Licking.BatteryLicking.ts>Data.pos.ts(end)) = [];
% 
% clear BatteryLicking; fclose(BatteryLickingData);clear BatteryLickingData;
% %% and Battery data...
% BatteryData = fopen('BatteryLickingDetection.data','r');
% Battery = transpose(fread(BatteryData,[2,Inf],'double'));
% if isempty(Battery)
%    Data.Licking.BatteryDetection.ts = [NaN];
%    Data.Licking.BatteryDetection.Voltage = [NaN];
%    
% else
%    Data.Licking.BatteryDetection.ts = Battery(:,1);
%    Data.Licking.BatteryDetection.Voltage = Battery(:,2);
% end  
% clear Battery; fclose(BatteryData);clear BatteryData;%% Axona...
% 
%% Axona Synch data...

AxonaSynchData = fopen('AxonaSynch.data','r');
AxonaSynch=transpose(fread(AxonaSynchData,[2,Inf],'double'));;
if isempty(AxonaSynch)
   Data.AxonaSynch.ts = [NaN];
   Data.AxonaSynch.pos = [NaN];
else
   Data.AxonaSynch.ts = AxonaSynch(:,1);
   Data.AxonaSynch.pos = AxonaSynch(:,2);
end;
clear AxonaSynch; fclose(AxonaSynchData);clear AxonaSynchData;

%% Reward....
RewardData = fopen('Reward.data','r');
Reward=transpose(fread(RewardData,[2,Inf],'double'));;
if isempty(Reward)
   Data.Reward.ts = [NaN];
   Data.Reward.pos= NaN;
else
   Data.Reward.ts = Reward(:,1);
   Data.Reward.pos = Reward(:,2);
end;
clear Reward; fclose(RewardData);clear RewardData;
%%
% open the binary file
% VoltageLickingData = fopen('VoltageLickingDetection.data','r');
% VoltageLicking = transpose(fread(VoltageLickingData,[2,Inf],'double'));
% if isempty(VoltageLicking)
%    Data.Licking.VoltageDetection.ts = [NaN];
%    Data.Licking.VoltageDetection.V = [NaN];
% else
%    Data.Licking.VoltageDetection.ts = VoltageLicking(:,1);
%    Data.Licking.VoltageDetection.V = VoltageLicking(:,2);
% end;
% % Data.Licking.pos(Data.Licking.ts>Data.pos.ts(end)) = [];
% % Data.Licking.ts(Data.Licking.ts>Data.pos.ts(end)) = [];
% 
% clear VoltageLicking; fclose(VoltageLickingData);clear LickingData
% %%
% VoltageData = fopen('VoltageLicking.data','r');
% Licking = transpose(fread(VoltageData,[3,Inf],'double'));
% if isempty(Licking)
%    Data.Licking.VoltageLicking.ts = [NaN];
%    Data.Licking.VoltageLicking.pos = [NaN];
%    Data.Licking.VoltageLicking.Events = [NaN];
% else
%    Data.Licking.VoltageLicking.ts = Licking(:,1);
%    Data.Licking.VoltageLicking.pos = Licking(:,2);
%    Data.Licking.VoltageLicking.Events = Licking(:,3);
% end;
% Data.Licking.VoltageLicking.pos(Data.Licking.VoltageLicking.ts>Data.pos.ts(end)) = [];
% Data.Licking.VoltageLicking.ts(Data.Licking.VoltageLicking.ts>Data.pos.ts(end)) = [];
% Data.Licking.VoltageLicking.Events(Data.Licking.VoltageLicking.ts>Data.pos.ts(end)) = [];
% 
% clear Licking; fclose(VoltageData);

%% save also the vr structure before closing it...
Data.vr = vr;

%% Save into directory previously saved...
tmpfile = ['Mouse_' vr.RatName(1:end-1) '_' vr.Day(1:end-1) '_Session_' vr.Session(1:end-1) ];
cd(vr.DirectoryName)
eval([ 'save(' char(39) tmpfile char(39) ',' char(39) 'Data' char(39) ');;' ])
cd(vr.FileStoreDirectoryName)
eval([ 'save(' char(39) tmpfile char(39) ',' char(39) 'Data' char(39) ');;' ])

DisplaySummaryOfTrial;
disp ([tmpfile ' saved into right directory' ]);


end
