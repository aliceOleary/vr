function s = MartlabOscilloscope(varargin)
if nargin==1;
InputChannel = varargin{1};
% YLim = 3;
elseif nargin==2;
InputChannel = varargin{1};
YLim = varargin{2};
end;
% figure
% ylim([YLim(1) YLim(2)]); 
% load gong.mat;
% Sound = y(1:1000);
% tempFileFid = fopen('F:\log.txt','w');  % The actu
s = daq.createSession('ni');
s.Rate= 10000;
addAnalogInputChannel(s,'Dev2',['ai' num2str(InputChannel)],'Voltage');

s.IsContinuous=1
 %lh = addlistener(s,'DataAvailable',@(src,event)logData(src,event,tempFileFid)); 
lh = addlistener(s,'DataAvailable',@plotData); 
s.startBackground;
% pause(
% s.stop
% fclose(tempFileFid)
end

 