function [vr] = ScanPhotoLickDetection(vr);%,device,InputChannel)
%% Voltage sensitive Lick detector...
sampleVolts=getsample(vr.PhotoLickDetection.daqSessLickDetection); 
% vr.PhotoLickDetection.tmpV = sampleVolts;
% vr.PhotoLickDetection.tmptimeStamp = [vr.timeElapsed]; %clock;
licking = sampleVolts > vr.PhotoLickDetection.VoltageThreshold  ;
vr.PhotoLickDetection.LickingLastLog = [vr.PhotoLickDetection.LickingLastLog(end -((vr.PhotoLickDetection.LickingLastLogNumberOfEvents)-1)) licking ] ;
licking  = diff(vr.PhotoLickDetection.LickingLastLog)>0;
%% store the voltage into the ad hoc file...
% fprintf(vr.tempFileFidTXT,'%6.2f\n', vr.PhotoLickDetection.tmpV);
% if vr.PhotoLickDetection.Licking % if there is no minimum voltage skip the logging of the time stamps...
%     if vr.PhotoLickDetection.LickEvents(vr.TrialSettings.iTrial) > 0;
%        vr.PhotoLickDetection.Licking = abs(vr.PhotoLickDetection.tmptimeStamp(end) - vr.PhotoLickDetection.LickTimeStamps(end,:))> vr.PhotoLickDetection.MinimumTimeDistance; %
%     end;
% end
%% Store the logs all together...
% vr.PhotoLickDetection.LickEvents(vr.TrialSettings.iTrial) =vr.PhotoLickDetection.LickEvents(vr.TrialSettings.iTrial) +vr.PhotoLickDetection.Licking ;
% %LickEventsOutSideRewardArea
%LickEventsInsideRewardArea


if vr.isRecording     ; 
    % vr.PhotoLickDetection.LickTimeStamps = [vr.PhotoLickDetection.LickTimeStamps; vr.PhotoLickDetection.tmptimeStamp(end)]; %this is where store updated lick timestamps
    % vr.PhotoLickDetection.LickPositions = [vr.PhotoLickDetection.LickPositions;vr.position(2)]; %this is where store updated lick positions
    %vr.PhotoLickDetection.Licks=1;
    if licking
        fwrite(vr.PhotoLickDetection.fidLicking, [vr.timeElapsed-vr.recordingStartTime 1 ],'double');
        vr.PhotoLickDetection.Licks =1;
        if vr.PhotoLickDetection.ReleaseSoundWhileDetecting
            sound(vr.PhotoLickDetection.Sound);
        end
        % % save the time stamps of the Licks...
    else
        vr.PhotoLickDetection.Licks=0;
        fwrite(vr.PhotoLickDetection.fidLicking, [vr.timeElapsed-vr.recordingStartTime 0 ],'double');
    end
else
    if licking
        vr.PhotoLickDetection.Licks =1;
        if vr.PhotoLickDetection.ReleaseSoundWhileDetecting
            sound(vr.PhotoLickDetection.Sound);
        end
    else
        vr.PhotoLickDetection.Licks =0;
    end
end



%if vr.PhotoLickDetection.Licking
%     if  vr.InGoalLocation
%            eval(['vr.PhotoLickDetection.LickEventsInsideRewardArea(vr.TrialSettings.iTrial,find(vr.WhichCueLocation),vr.EnvironmentsVisited.' vr.EnvironmentSettings.Evironments{vr.WorldIndexWithReward} ') = vr.PhotoLickDetection.LickEventsInsideRewardArea(vr.TrialSettings.iTrial,find(vr.WhichCueLocation),vr.EnvironmentsVisited.' vr.EnvironmentSettings.Evironments{vr.WorldIndexWithReward} ') + vr.PhotoLickDetection.Licking ;' ]);
%             
%            if vr.PhotoLickDetection.Licking
%            vr.PhotoLickDetection.LickTimeStampsInsideRewardArea = [vr.PhotoLickDetection.LickTimeStampsInsideRewardArea ; vr.timeElapsed];
%            vr.PhotoLickDetection.LickPositionsInsideRewardArea = [vr.PhotoLickDetection.LickPositionsInsideRewardArea ; vr.pos];
%            end
%     elseif  vr.InGoalLocation==0 & vr.currentWorld == vr.WorldIndexWithReward;    
%            eval(['vr.PhotoLickDetection.LickEventsOutSideRewardArea(vr.TrialSettings.iTrial,1,vr.EnvironmentsVisited.' vr.EnvironmentSettings.Evironments{vr.WorldIndexWithReward} ') = vr.PhotoLickDetection.LickEventsOutSideRewardArea(vr.TrialSettings.iTrial,1,vr.EnvironmentsVisited.' vr.EnvironmentSettings.Evironments{vr.WorldIndexWithReward} ') + vr.PhotoLickDetection.Licking ;' ]);
%            if vr.PhotoLickDetection.Licking
%            vr.PhotoLickDetection.LickTimeStampsOutSideRewardArea = [vr.PhotoLickDetection.LickTimeStampsOutSideRewardArea ; vr.timeElapsed];
%            vr.PhotoLickDetection.LickPositionsOutSideRewardArea = [vr.PhotoLickDetection.LickPositionsOutSideRewardArea ; vr.pos];
%            end
%     
%     end
%     
%delete stuff...
% figure(100)
% scatter(vr.position(2),vr.PhotoLickDetection.tmptimeStamp(end))
% vr.PhotoLickDetection = rmfield(vr.PhotoLickDetection,'tmpV');%vr.PhotoLickDetection = rmfield(vr.PhotoLickDetection,'tmptimeStamp');



end