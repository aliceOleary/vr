function [displacement,movementType] = moveWithRotaryEncoder(vr)

if vr.debugMode ~= 1
    % Read data from NIDAQ
    count = vr.daqSessRotEnc.inputSingleScan;
    resetCounter( vr.counterCh );
    
    % Remove NaN's from the data (these occur after NIDAQ has stopped)
    if isnan(count);  count = 0;   end
    
    % Need to control for wheel going backwards (means counts go backwards from 2^32) %
    if count > 100000;   count = count - (2^32);   end
    
    % Convert to actual position offset %
    % 4096 counts per cycle in a Kubler 05.2400.1122.1024 when used in 'X4' mode;
    % wheel circumference is 55.3cm (for nominal dia 7 inch);
    % 1 virmen unit = 1cm @ vr.movementGain = 1.
    pOff = (count/4096) * vr.wheelCircumference;
    % if mod( vr.iterations, 20 )==0  &&  pOff~=0;
    %     fprintf(1,'disp=%f2.4',pOff);
    % end
    
    % Update displacement %
    displacement = [0 pOff 0 0];
    movementType = 'displacement';
else
    displacement = [0 1 0 0];
    movementType = 'displacement';
end

