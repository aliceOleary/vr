function [velocity,movementType] = moveWithTwoMiceUDP(vr)
% Read two mouse movement data that is being streamed from Labview via UDP.
% This is a velocity-based movement function.

velocity = [0 0 0 0];
movementType = 'velocity';
offsetTheta = 0;     % This variable allows you to have detectors that are not exactly at 0 and 90 degrees (for X and Y, respectively)
                     % 0=no offset (use raw values), set 1-359 to set for detectors in other positions, or deal with dectector polarity mismatches.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) Read Data from UDP ports (one for each of X and Y).
data = zeros(2,1);
if vr.nDimensionsInVR==1;  dimsToCheck=2;   else   dimsToCheck = 1:2;  end
for ii=dimsToCheck
    nSamp = floor(  vr.XYUDPPorts(ii).bytesAvailable / 7   );  % 7 bytes per sample: format is '+0.000' followed by a newline.
    if nSamp==0
        data(ii) = 0;
    else
        sampTemp = nan(1,nSamp);
        for jj=1:nSamp
            sampTemp(jj) = str2double(  fgetl(vr.XYUDPPorts(ii))  );
        end
        data(ii) = nanmean(sampTemp);
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2) Process  raw XY input to get the final changes to the virmen velocity
% Deal with potential offset in the detectors (not at exactly 0 and 90 degrees.
data = [cosd(offsetTheta), -sind(offsetTheta); sind(offsetTheta), cosd(offsetTheta)]   *   data([1 2]);
% Set the scaling of mouse movement ('volts' in Labview) to movement in Virmen. Note that this is different to 
% 'movementGain' - this should produce the correct movement when 'movementGain' is set to 1. 
velocity(1) = -vr.opticalMouseToVRScaling*data(1);
velocity(2) = -vr.opticalMouseToVRScaling*data(2);
% Finally, the X and Y inputs need to be transformed by the mouse's current actual heading direction.
velocity(1:2) = [cos(vr.position(4)) -sin(vr.position(4)); sin(vr.position(4)) cos(vr.position(4))] * velocity(1:2)';

% disp(velocity);
