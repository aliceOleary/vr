function code = linearTrack_TW
% linearTrackWithCylinders   Code for the ViRMEn experiment linearTrackWithCylinders.
%   code = linearTrackWithCylinders   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.

% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- INITIALIZATION code: executes before the ViRMEN engine starts. %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = initializationCodeFun(vr)

% First, there are a couple of things that we only do when it is a real trial: initialise DAQ hardware, and data save file.
% Important note on 'vr.debugMode':scanimage
%    0=no debugging, all hardware is present, run it for real, 
%    1=full debug mode active, no hardware required no files written
%    2=no hardware interaction, but will save data files.
vr.debugMode = eval(vr.exper.variables.debugMode);

if vr.debugMode == 0
    vr = initialiseHardware(vr);
end

  
% (2) Get some trial name information, and 2P recording time. This needs to come first, not post-trial, as the user needs to input the current scanimage trial_XXX number %
% First, get user input on trial names etc %
trialConfigInput = inputdlg({'Mouse number','Virmen trial letter (CAPITALS please!)','2P starting trial number','2P default trial length'},'Enter trial info',[1; 1; 1; 1], ...
                            {'m234', 'A', '1', vr.exper.variables.default2PhotonRecordingDuration});
% Save these to vr. struct.
vr.virmenFileLetter = trialConfigInput{2};
vr.finalFileName = [trialConfigInput{1} '_' datestr(now,'yyyymmdd') '_' vr.virmenFileLetter];
vr.currentFileIdx2P = str2double( trialConfigInput{3} );
vr.recordingDuration2P = str2double(trialConfigInput{4});

%% Initialisation
%setup file to record black-out periods
vr.fileRoot = strcat('D:\MouseData\',trialConfigInput{1});
if ~exist(vr.fileRoot)
    mkdir(vr.fileRoot)
end

% (4c) Some other general variables %
vr.isRecording = false;      % Are we currently recording virmen data?
vr.isRecording2P = false;    % Are we also currently recording 2P data?
vr.recordingStartTime = [];      % Keep track of the start time for the virmen recording session
vr.recordingStartTime2P = [];   % Keep track of the start time for the current 2P recording session
vr.recordingExists = false;   % Set this to true if the virmen recording is run, so we know to deal with file in the termination function.
vr.currentDirection = eval(vr.exper.variables.startDirection); % The current direction of the animal on the track, 1=N (increasing y), -1=S (decreasing y)
vr.turnaroundActive = false;     % Signals whether the animal is currently in the turnaround screen-blanked period 
vr.turnaroundStartTime = [];     % Will act as a timer, such that the turnaround blanking is of the correct duration.
vr.turnaroundDelay = eval( vr.exper.variables.turnaroundDelay );  % Get the turnaround blanking duration (user-set) in a more convenient format.
vr.trackLength = eval( vr.exper.variables.trackLength );
vr.movementGain = eval( vr.exper.variables.movementGain );
vr.monitorAspectRatio = eval( vr.exper.variables.monitorAspectRatio );
vr.perspectiveScale = eval( vr.exper.variables.perspectiveScale );
vr.wheelCircumference = eval( vr.exper.variables.wheelCircumference );
if vr.debugMode~=1
    vr.tempFileDir = 'D:\';
    vr.tempFileLocation = [vr.tempFileDir 'virmenTempData.dat'];
    vr.tempFileLocationTXT = [vr.tempFileDir 'virmenTempData.txt'];
    if ~isdir(vr.tempFileDir)
        mkdir(vr.tempFileDir);
    end
    vr.tempFileFid = fopen(vr.tempFileLocation,'w');  % The actual data always goes in this temp file during the recording.
    vr.tempFileFidTXT = fopen(vr.tempFileLocationTXT,'w'); 
   %write header
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','########## Header ##########');
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Mouse ID:', trialConfigInput{1});
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Date:', datestr(now,'yyyymmdd'));
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Data file name:', vr.finalFileName);
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','############################');
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','### Imaging Session Info ###');
end

    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (3) Initialise text boxes for controls %
% (3a) Pos recording start/stop button
textWindowIdx = 5;
n=1;
vr.text(n).string = 'POS REC START';
vr.text(n).position = [-0.75 0.7];
vr.text(n).size = 0.1;
vr.text(n).color = [0 1 0];  % Should be green initially, switch to red for 'STOP'
vr.text(n).window = textWindowIdx;
% (3b) Pos recording time elapsed timer (shows trial name when not recording)
n=2;
vr.text(n).string = ['(RECORD TO FILE ' vr.virmenFileLetter, ')']; % This refers to the virmen .dat file name
vr.text(n).position = [-0.75 0.5];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];  % White
vr.text(n).window = textWindowIdx;
% (3c) 2P record start/stop button
n=3;
vr.text(n).string = '2PHOTON REC START';
vr.text(n).position = [-0.75 0];
vr.text(n).size = 0.1;
vr.text(n).color = [0.5 0.5 0.5];   % Grey initially, switch to green (for active) when virmen recording is active
vr.text(n).window = textWindowIdx;
% (3d) 2P record time elapsed timer (when not recording, displays trial # to be used next)
n=4;
vr.text(n).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
vr.text(n).position = [-0.75 -0.2];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];
vr.text(n).window = textWindowIdx;




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runtimeCodeFun(vr)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a turnaround invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.turnaroundActive
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer) > vr.turnaroundDelay
        disp('toggle off')
        % Turnaround blanking to be released: make the world visible .. %
        vr.worlds{ vr.currentWorld }.surface.visible(:) = true;
        vr.turnaroundActive = false;
        % .. with the exception of the reward on which the animal is currently sitting .. %
         
    else
        disp('toggle on')
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2) Get displacement and scale (gain)        
% disp(vr.position)%
vr.dp(2) = vr.dp(2) * vr.movementGain;
%disp(vr.dp(2))
% If using rotary encoder, need to flip the displacement when heading south 
% (so the same wheel direction keeps the mouse moving 'forward').
%vr.dp(2) = vr.dp(2) * vr.currentDirection;
% Check that we haven't run outside of the track (virmen edge detection sometimes showing bugs).

if vr.position(2) > vr.trackLength-4
    disp('teleport')
    %vr.dp(2) = 0;
    vr.position(1) =0;
    vr.position(2) = 0;   %vr.dp(2) = 0;
    vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
    vr.turnaroundTimer  = vr.timeElapsed;
    vr.turnaroundActive = true;
end


    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (5) Data recording.                                                                                        %
%%% (5a) Control of virmen recording (i.e. making a .dat file) %%%
if ~vr.isRecording 
    % If not recording, we need to check whether we need to start %
    % text(1) is 'POS REC START'
    if vr.textClicked==1  && ~strcmp(vr.text(1).string,'PLEASE EXIT VIRMEN') %Bug fix when rec clicked twice in session
        %for txt file output
        vr.sessionTime = datetime('now','Format','HH:mm:ss');
        
        vr.isRecording = 1;
        vr.recordingStartTime = vr.timeElapsed;
        vr.text(1).string = 'POS REC STOP';
        vr.text(1).color = [1 0 0];
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
        vr.text(3).color = [0 1 0];   % Set the 2P recording button to be 'active'.
    end
elseif vr.isRecording
    % If we are recording, check whether we need to stop  .. %
    if vr.textClicked==1 
        if ~strcmp(vr.text(1).string,'PLEASE EXIT VIRMEN');
            %for txt file output
            vr.sessionTime(2) = datetime('now','Format','HH:mm:ss');
            % .. if user has clicked stop then stop the recording. %
            vr.isRecording = 0;
            fclose(vr.tempFileFid);
            % Reset the text on the control window %
            %         vr.text(1).string = 'POS REC STOPPED';
            vr.text(1).string = 'PLEASE EXIT VIRMEN'; %Bug fix when rec clicked twice in session- change 
            vr.text(1).color = ones(1,3) .* 0.5;
            vr.text(3).color = ones(1,3) .* 0.5;   % 2P recodring is 'inactive'
        end
    else 
        % If neither of the above, we are in a continuing recording, we will just update the timer text %
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
    end
end
%%% (5b) Control of 2P recording %%%
if vr.isRecording % Can only record 2P whilst virmen pos recording is active
    if ~vr.isRecording2P
        % If we are not recording, check whether we need to start  .. %
        if vr.textClicked==3
            %txt file output
            vr.tempTime = datetime('now','Format','HH:mm:ss');
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' start:'], char(vr.tempTime) ); 
            
            % Below is when the user has started the recording
            vr.isRecording2P = true;
            vr.text(3).string = '2P REC STOP';
            vr.text(3).color = [1 0 0];
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            % .. and now actually start the 2P imaging %
            vr.recordingStartTime2P = vr.timeElapsed;
            if vr.debugMode == 0
                vr.daqSess2PFrameClock.resetCounters;            % Make sure the frame clock is at zero ..
                vr.daqSess2PStartStop.outputSingleScan([1 0]);   % .. and start the 2P recording.
            end  
        end
    elseif vr.isRecording2P
        % If we are recording, check whether we need to stop  .. %
        if vr.textClicked==3  || (vr.timeElapsed-vr.recordingStartTime2P)>vr.recordingDuration2P
            %txt file output
            vr.tempTime(2) = datetime('now','Format','HH:mm:ss');
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' stop:'], char(vr.tempTime(2)));
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' duration (mm:ss:ms):' ],datestr(vr.tempTime(2)-vr.tempTime(1),'MM:SS:FFF'));
            clear vr.tempTime
            %  .. below is when we were recording, but it has been stopped by timing out, or by the user clicking '2P REC STOP' %
            vr.isRecording2P = false;
            vr.currentFileIdx2P = vr.currentFileIdx2P + 1;  % Bump the Idx counter for the scanimage file.
            vr.text(3).string = '2P REC START';
            vr.text(3).color = [0 1 0];
            vr.text(4).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
            if vr.debugMode == 0
                vr.daqSess2PStartStop.outputSingleScan([0 1]);   % .. and stop the 2P recording.
            end
        else
            %  .. if we were recording but it hasn't been stopped, just update the timer
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
        end
    end     
end
%%% (5c) Finally, if we are recording, we need to actually record the data %%%
if vr.debugMode ~= 1 && vr.isRecording
    if vr.debugMode == 0 && vr.isRecording2P
        frameCount2P = vr.daqSess2PFrameClock.inputSingleScan;
        sessionIdx2P = vr.currentFileIdx2P;
    else
        frameCount2P = 0;
        sessionIdx2P = 0;
    end
    %%% FILE FORMAT IS HERE: [time, xPos, yPos, dir, xVel, yVel, trialIdx2P, frameCount2P, isReward, amountReward, isTurnAround];
    %%% The first six I want to keep standard across experiments, hence the inclusion of x pos data, even though it is irrelevant to the linear track.
    measurementsToSave = [vr.timeElapsed-vr.recordingStartTime, vr.position(1), vr.position(2), vr.position(4), vr.velocity(1),...
                            vr.velocity(2), sessionIdx2P, frameCount2P, vr.turnaroundActive ];
    if ~vr.recordingExists
        fwrite(vr.tempFileFid,length(measurementsToSave),'double');  % The very first entry (only) in the file is the number of fields.
    	vr.recordingExists = 1;                                      %  ..
    end
    fwrite(vr.tempFileFid,measurementsToSave,'double');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- TERMINATION code: executes after the ViRMEn engine stops. ---  %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = terminationCodeFun(vr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if vr.debugMode~=1 && vr.recordingExists
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Move the temp data files to the final directory %
    finalDataDir = uigetdir('C:/VR_Common/Data','Choose a folder to save the data');
    copyWorked = copyfile(vr.tempFileLocation, [finalDataDir, '/', vr.finalFileName, '.dat']);
    exper = copyVirmenObject(vr.exper); %#ok<NASGU>
    %txt output
    %txt file output
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','############################');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','### ViRMEn Session Info ####');
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','ViRMEn recording start:', char(vr.sessionTime(1)) );
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','ViRMEn recording stop:', char(vr.sessionTime(2)) );
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n','ViRMEn session duration (hh:mm:ss):', datestr(vr.sessionTime(2)-vr.sessionTime(1),'HH:MM:SS'));
    fprintf(vr.tempFileFidTXT,'%-s\r\n\r\n','############################');
    
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','##### DAT File Format ######');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','Note: First entry in DAT file is n of columns to extract');
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t\r\n\r\n','Dat file structure (column names):','Time','Xpos',...
            'Ypos','Dir','Xspeed','Yspeed','TrialID_2P','2P frame #','-isReward','rewardDropSize','-isTurn');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','#############################');
    %add some 2P settings info
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','##### 2P Settings Info ######');
    
    %if sessions were recorded add some UI defined info
    if vr.currentFileIdx2P > 1
        UIdialogStr = cell(vr.currentFileIdx2P-1,1);
        for n = 1:vr.currentFileIdx2P-1
            UIdialogStr{n,1} = ['Session ' num2str(n,'%03i') ' Power (mW):' ];
            UIdialogStr{n,2} = ['Session ' num2str(n,'%03i') ' PMT gain:' ];
        end
        TXTInput = inputdlg( UIdialogStr,'Enter additional Session info',ones(numel(UIdialogStr),1),horzcat(cellstr(repmat('50',numel(UIdialogStr)/2,1))',...
            cellstr(repmat('450',numel(UIdialogStr)/2,1))') );
        for n = 1:vr.currentFileIdx2P-1
            fprintf( vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging ' UIdialogStr{n,1}], TXTInput{n});
            fprintf( vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging ' UIdialogStr{n,2}], TXTInput{n+size(UIdialogStr,1)} );
        end
    end
    fprintf(vr.tempFileFidTXT,'%-s\t','############ END ############');
    %
    if copyWorked
        delete(vr.tempFileLocation);
        save([finalDataDir, '\', vr.finalFileName, '.mat'],'exper');  % ALso save the 'exper' structure with the data. 
        copyfile(vr.tempFileLocationTXT, [finalDataDir, '\', vr.finalFileName, '.txt']);
    else
        disp(['!!IMPORTANT NOTICE!! There was a problem moving temporary recording file to final data folder.' ...
            ' Temp files are still in present, in ' vr.tempFileDir]);
        save([vr.tempFileDir 'virmenTempData.mat'],'exper');     % Save the 'exper' structure to the temp folder, in this case.
    end 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Close all file handles %
fclose all;
