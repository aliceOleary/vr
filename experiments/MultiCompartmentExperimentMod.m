<<<<<<< HEAD
function code = MultiCompartmentExperiment
=======
function code = MultiCompartmentExperimentMod
>>>>>>> 45f0c0abd87a0664746399d4bc2fccfde6b88d8d
% MultiCompartmentExperiment   Code for the ViRMEn experiment MultiCompartmentExperiment.
%   code = MultiCompartmentExperiment   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.


% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT



% --- INITIALIZATION code: executes before the ViRMEn engine starts.
function vr = initializationCodeFun(vr);
    
vr.Experiment = 'Multicompartment';
<<<<<<< HEAD
=======

>>>>>>> 45f0c0abd87a0664746399d4bc2fccfde6b88d8d
vr=experimentCustomVals(vr);

% vr.WorldNameWithReward = 'B';
% vr.NumberOfTimesRewardEnvironmentPresented = 2;
%vr.RewardedCuesIntoRepeatedCompartments = [1,2];
% vr.pos=[];                      % we want to know where the animal is across all environments; LogBehaviouralData function will account for that...
% vr.AssignedEnvironment =[];

%% Specify Saving  data properties;
% First, there are a couple of things that we only do when it is a real trial: initialise DAQ hardware, and data save file.
% Important note on 'vr.debugMode':scanimage
%    0=no debugging, all hardware is present, run it for real, 
%    1=full debug mode active, no hardware required no files written
%    2=no hardware interaction, but will save data files.
vr.debugMode = eval(experimentCustomVals.debugMode);
if vr.debugMode == 0
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (1) Initialise DAQ hardware %
    daqreset; %reset DAQ in case it's still in use by a previous Matlab program
    % (1a) Counter input for poition reading %
    vr.daqSessRotEnc = daq.createSession('ni');
    vr.counterCh = vr.daqSessRotEnc.addCounterInputChannel('VR_6321','ctr0','Position');
    vr.counterCh.EncoderType = 'X4';
    
    % (1b) Analog output to drive reward %
    vr.rewardAO = analogoutput('nidaq','VR_6321');
    addchannel(vr.rewardAO,0,'voltage');
    set(vr.rewardAO,'samplerate',eval(vr.exper.variables.sRateSolanoid));
    vr.rewardPattern = [ ones(eval(vr.exper.variables.rewardDropSize),1).*5; 0; 0; 0; 0];
    
    % (1c) DIO for microscope start and stop signals %
    vr.daqSess2PStartStop = daq.createSession('ni');
    vr.daqSess2PStartStop.addDigitalChannel('VR_6321','port0/line0:1','outputonly');  % One line for on, one for off.
    vr.daqSess2PStartStop.outputSingleScan([0 0]);
    
    % (1d) 2P microscope frame clock input %
    vr.daqSess2PFrameClock = daq.createSession('ni');
    vr.daqSess2PFrameClock.addCounterInputChannel('VR_6321', 'ctr3', 'EdgeCount');  % Use counter 3, as ctr 1 (PFI3) has a broken contact on breakout (as of 2016-12-10), and ctr2 clashes with ctr0 when in 'position' mode.
    
end

% (2) Get some trial name information, and 2P recording time. This needs to come first, not post-trial, as the user needs to input the current scanimage trial_XXX number %
% First, get user input on trial names etc %
trialConfigInput = inputdlg({'Mouse number','Virmen trial letter (CAPITALS please!)','2P starting trial number','2P default trial length'},'Enter trial info',[1; 1; 1; 1], ...
                            {'m234', 'A', '1', experimentCustomVals.default2PhotonRecordingDuration});
% Save these to vr. struct.
vr.virmenFileLetter = trialConfigInput{2};
vr.finalFileName = [trialConfigInput{1} '_' datestr(now,'yyyymmdd') '_' vr.virmenFileLetter];
vr.currentFileIdx2P = str2double( trialConfigInput{3} );
vr.recordingDuration2P = str2double(trialConfigInput{4});


%if strcmp(char(vr.TrialSettings.movementFunctionAfterPause ) , 'moveWithRotaryEncoder');vr = RotaryEncoderSettings(vr);end
%%%%%%%%%%%%%%%% Set reward delivery system %%%%%%%%%%%%%%%%%%
% vr = RewardDeliverySettings(vr);
% %%%%%%%%%%%%%%%% Set licking detector %%%%%%%%%%%%%%%%%%%%%%%%
% %vr = LickDetectionSettings(vr);
% vr = PhotoLickDetectionSettings(vr);
% %%%%%%%%%%%%%%%% Set ephs synch locking         
% vr = SynchAxonaSettings(vr);

% (4c) Some other general variables %
vr.isRecording = false;      % Are we currently recording virmen data?
vr.isRecording2P = false;    % Are we also currently recording 2P data?
vr.recordingStartTime = [];      % Keep track of the start time for the virmen recording session
vr.recordingStartTime2P = [];   % Keep track of the start time for the current 2P recording session
vr.recordingExists = false;   % Set this to true if the virmen recording is run, so we know to deal with file in the termination function.
%vr.currentDirection = eval(vr.exper.variables.startDirection); % The current direction of the animal on the track, 1=N (increasing y), -1=S (decreasing y)
%vr.turnaroundActive = false;     % Signals whether the animal is currently in the turnaround screen-blanked period 
vr.interTrialDelayStartTime = [];     % Will act as a timer, such that the turnaround blanking is of the correct duration.
vr.interTrialDelay = eval( vr.experimentCustomVals.interTrialDelay );  % Get the turnaround blanking duration (user-set) in a more convenient format.
%vr.trackLength = eval( vr.exper.variables.trackLength );
vr.movementGain = eval( vr.experimentCustomVals.movementGain );
vr.monitorAspectRatio = eval( vr.experimentCustomVals.monitorAspectRatio );
vr.perspectiveScale = eval( vr.experimentCustomVals.perspectiveScale );
vr.wheelCircumference = eval( vr.experimentCustomVals.wheelCircumference );
if vr.debugMode~=1
    vr.tempFileDir = 'F:\';
    vr.tempFileLocation = [vr.tempFileDir 'virmenTempData.dat'];
    vr.tempFileLocationTXT = [vr.tempFileDir 'virmenTempData.txt'];
    if ~isdir(vr.tempFileDir)
        mkdir(vr.tempFileDir);
    end
    vr.tempFileFid = fopen(vr.tempFileLocation,'w');  % The actual data always goes in this temp file during the recording.
    vr.tempFileFidTXT = fopen(vr.tempFileLocationTXT,'w'); 
   %write header
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','########## Header ##########');
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Mouse ID:', trialConfigInput{1});
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Date:', datestr(now,'yyyymmdd'));
   fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Data file name:', vr.finalFileName);
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','############################');
   fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','### Imaging Session Info ###');
   
end
% (3) Initialise text boxes for controls %
% (3a) Pos recording start/stop button
textWindowIdx = 5;
n=1;
vr.text(n).string = 'POS REC START';
vr.text(n).position = [-0.75 0.7];
vr.text(n).size = 0.1;
vr.text(n).color = [0 1 0];  % Should be green initially, switch to red for 'STOP'
vr.text(n).window = textWindowIdx;
% (3b) Pos recording time elapsed timer (shows trial name when not recording)
n=2;
vr.text(n).string = ['(RECORD TO FILE ' vr.virmenFileLetter, ')']; % This refers to the virmen .dat file name
vr.text(n).position = [-0.75 0.5];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];  % White
vr.text(n).window = textWindowIdx;
% (3c) 2P record start/stop button
n=3;
vr.text(n).string = '2PHOTON REC START';
vr.text(n).position = [-0.75 0];
vr.text(n).size = 0.1;
vr.text(n).color = [0.5 0.5 0.5];   % Grey initially, switch to green (for active) when virmen recording is active
vr.text(n).window = textWindowIdx;
% (3d) 2P record time elapsed timer (when not recording, displays trial # to be used next)
n=4;
vr.text(n).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
vr.text(n).position = [-0.75 -0.2];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];
vr.text(n).window = textWindowIdx;
% (3e) Immediate reward button.
n=5;
vr.text(n).string = 'REWARD NOW';
vr.text(n).position = [-0.75 -0.7];
vr.text(n).size = 0.1;
vr.text(n).color = [0 0 1];
vr.text(n).window = textWindowIdx;
% (3f) buttons to regulate amount of reward
n=6;
if vr.currRewardDropSize > vr.maxPossRewardDropSize
    vr.text(n).color = [0.5 0.5 0.5];
    vr.currRewardDropSize = vr.maxPossRewardDropSize;
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
else
    vr.text(n).color = [0 1 0];
end
vr.text(n).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
vr.text(n).position = [0.3 -0.6];
vr.text(n).size = 0.08;
vr.text(n).window = textWindowIdx;

n=7;
vr.text(n).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
vr.text(n).position = [0.3 -0.8];
vr.text(n).size = 0.08;
if vr.currRewardDropSize <= 1
    vr.text(n).color = [0.5 0.5 0.5];
    vr.currRewardDropSize = 1;
    vr.rewardPattern = [ 5; 0; 0; 0; 0 ];
else
    vr.text(n).color = [0 1 0];
end
vr.text(n).window = textWindowIdx;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

assignin('base','vr',vr);   % For debugging purposes.
end


% --- RUNTIME code: executes on every iteration of the ViRMEn engine.
function vr = runtimeCodeFun(vr)

    %% Run teletransportation across environments...
[ vr] = RunTeletransportationMultiCompartment(vr );

%% Is the animal in the goal related area ?
if isfield(vr.worlds{vr.currentWorld}.objects.indices,'GoalCue') ;
        if vr.RewardIfLicking==0; %% This is important, it virtually shifts the location of the reward at the extremes of the rewarded area...
       vr.InFirstGoalLocation = ...
        vr.position(2) > vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(1) && ...
        vr.position(2) <  vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(1) + ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius;
             
        vr.InSecondGoalLocation  = ...
        vr.position(2) > vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(2) && ...
        vr.position(2) <  vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(2) + ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius;
        
        else
        vr.InFirstGoalLocation = ...
        vr.position(2) > vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(1) - ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius ...
             && vr.position(2) <  vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(1) + ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius ;
         
        vr.InSecondGoalLocation  = ...
        vr.position(2) > vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(2) - ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius ...
             && vr.position(2) <  vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.y(2) + ...
             vr.exper.worlds{vr.currentWorld}.objects{vr.worlds{vr.currentWorld}.objects.indices.GoalCue}.radius ;
        
        end
        vr.WhichCueLocation = [ vr.InFirstGoalLocation vr.InSecondGoalLocation ] ;
        
        
        if vr.DiscriminateRewardedAreas    
            eval(['vr.InGoalLocation = vr.WhichCueLocation(vr.RewardedCuesIntoRepeatedCompartments(vr.EnvironmentsVisited.' vr.WorldNameWithReward ')) ; ' ])
        else
            vr.InGoalLocation = sum([ vr.WhichCueLocation ]) >0;
        end
        
else
    vr.InGoalLocation = 0;
end

% %% Scan photo detector...
%     if vr.DetectLicking
%         [vr] = ScanPhotoLickDetection(vr);% %% Scan lick detection...% if vr.LickDetection.daqSessLickDetection.SamplesAcquired ~= 0% stop(vr.LickDetection.daqSessLickDetection);% [vr] = ScanLickDetection(vr);% start(vr.LickDetection.daqSessLickDetection);% end
%  %      [vr] = ScanBatteryLickDetection(vr);
%     end
%     %% Add Key pressing option...
%     vr.ManualReward = ~isnan(vr.keyPressed);
    
%% StartReward if conditions are met....
if  isfield(vr.worlds{vr.currentWorld}.objects.indices,'GoalCue') ;
    if vr.InGoalLocation ;
        [vr] = StartReward(vr);
        isReward = 1;
        vr.nRewardsGiven = vr.nRewardsGiven + 1;
    elseif vr.textClicked==5
        isReward = 1;
        vr.nRewardsGiven = vr.nRewardsGiven + 1;
    else
        isReward=0;
 end
end;

if isReward && strcmp(get(vr.rewardAO,'Running'),'Off') && vr.debugMode == 0
    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
end


% (5) Data recording.                                                                                        %
%%% (5a) Control of virmen recording (i.e. making a .dat file) %%%
if ~vr.isRecording 
    % If not recording, we need to check whether we need to start %
    % text(1) is 'POS REC START'
    if vr.textClicked==1  && ~strcmp(vr.text(1).string,'PLEASE EXIT VIRMEN') %Bug fix when rec clicked twice in session
        %for txt file output
        vr.sessionTime = datetime('now','Format','HH:mm:ss');
        
        vr.isRecording = 1;
        vr.recordingStartTime = vr.timeElapsed;
        vr.text(1).string = 'POS REC STOP';
        vr.text(1).color = [1 0 0];
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
        vr.text(3).color = [0 1 0];   % Set the 2P recording button to be 'active'.
    end
elseif vr.isRecording
    % If we are recording, check whether we need to stop  .. %
    if vr.textClicked==1 
        if ~strcmp(vr.text(1).string,'PLEASE EXIT VIRMEN');
            %for txt file output
            vr.sessionTime(2) = datetime('now','Format','HH:mm:ss');
            % .. if user has clicked stop then stop the recording. %
            vr.isRecording = 0;
            fclose(vr.tempFileFid);
            % Reset the text on the control window %
            %         vr.text(1).string = 'POS REC STOPPED';
            vr.text(1).string = 'PLEASE EXIT VIRMEN'; %Bug fix when rec clicked twice in session- change 
            vr.text(1).color = ones(1,3) .* 0.5;
            vr.text(3).color = ones(1,3) .* 0.5;   % 2P recodring is 'inactive'
        end
    else 
        % If neither of the above, we are in a continuing recording, we will just update the timer text %
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
    end
end
%%% (5b) Control of 2P recording %%%
if vr.isRecording % Can only record 2P whilst virmen pos recording is active
    if ~vr.isRecording2P
        % If we are not recording, check whether we need to start  .. %
        if vr.textClicked==3
            %txt file output
            vr.tempTime = datetime('now','Format','HH:mm:ss');
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' start:'], char(vr.tempTime) ); 
            
            % Below is when the user has started the recording
            vr.isRecording2P = true;
            vr.text(3).string = '2P REC STOP';
            vr.text(3).color = [1 0 0];
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            % .. and now actually start the 2P imaging %
            vr.recordingStartTime2P = vr.timeElapsed;
            if vr.debugMode == 0
                vr.daqSess2PFrameClock.resetCounters;            % Make sure the frame clock is at zero ..
                vr.daqSess2PStartStop.outputSingleScan([1 0]);   % .. and start the 2P recording.
            end  
        end
    elseif vr.isRecording2P
        % If we are recording, check whether we need to stop  .. %
        if vr.textClicked==3  || (vr.timeElapsed-vr.recordingStartTime2P)>vr.recordingDuration2P
            %txt file output
            vr.tempTime(2) = datetime('now','Format','HH:mm:ss');
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' stop:'], char(vr.tempTime(2)));
            fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' duration (mm:ss:ms):' ],datestr(vr.tempTime(2)-vr.tempTime(1),'MM:SS:FFF'));
            clear vr.tempTime
            %  .. below is when we were recording, but it has been stopped by timing out, or by the user clicking '2P REC STOP' %
            vr.isRecording2P = false;
            vr.currentFileIdx2P = vr.currentFileIdx2P + 1;  % Bump the Idx counter for the scanimage file.
            vr.text(3).string = '2P REC START';
            vr.text(3).color = [0 1 0];
            vr.text(4).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
            if vr.debugMode == 0
                vr.daqSess2PStartStop.outputSingleScan([0 1]);   % .. and stop the 2P recording.
            end
        else
            %  .. if we were recording but it hasn't been stopped, just update the timer
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
        end
    end     
end

%%% (5c) Finally, if we are recording, we need to actually record the data %%%
if vr.debugMode ~= 1 && vr.isRecording
    if vr.debugMode == 0 && vr.isRecording2P
        frameCount2P = vr.daqSess2PFrameClock.inputSingleScan;
        sessionIdx2P = vr.currentFileIdx2P;
    else
        frameCount2P = 0;
        sessionIdx2P = 0;
    end
    %%% FILE FORMAT IS HERE: [time, xPos, yPos, dir, xVel, yVel, trialIdx2P, frameCount2P, isReward, amountReward, isTurnAround];
    %%% The first six I want to keep standard across experiments, hence the inclusion of x pos data, even though it is irrelevant to the linear track.
    measurementsToSave = [vr.timeElapsed-vr.recordingStartTime, vr.position(1), vr.position(2), vr.position(4), vr.velocity(1),...
                            vr.velocity(2), sessionIdx2P, frameCount2P, isReward, vr.currRewardDropSize, vr.turnaroundActive ];
    if ~vr.recordingExists
        fwrite(vr.tempFileFid,length(measurementsToSave),'double');  % The very first entry (only) in the file is the number of fields.
    	vr.recordingExists = 1;                                      %  ..
    end
    fwrite(vr.tempFileFid,measurementsToSave,'double');
end
end

% --- TERMINATION code: executes after the ViRMEn engine stops.
function vr = terminationCodeFun(vr)
if strcmp(vr.RewardDelivery.daqSessRewardDelivery.Running,'on')
    stop(vr.RewardDelivery.daqSessRewardDelivery);
end;
vr = ScanAxonaSynch(vr); % close the timestamps to Axona...
fclose(vr.Behaviour.fidBehaviour);
%fclose(vr.PhotoLickDetection.fidLicking);
fclose(vr.AxonaSynch.fidSynch);
fclose(vr.PhotoLickDetection.fidLicking);
fclose(vr.PhotoLickDetection.fidDetection);
%fclose(vr.BatteryLickDetection.fidLicking);
%fclose(vr.BatteryLickDetection.fidDetection);

OutputDataFromVirmen(vr);

end
end