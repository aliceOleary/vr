function vr = recControlGeneralizing( vr )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

%% recording mode control
%control of 2P recording mode - just switch string
if ~vr.isExit && vr.textClicked==11    
    if strcmp(vr.text(12).string,'TRACK')
        vr.text(12).string = 'SWITCH';
    elseif strcmp(vr.text(12).string,'SWITCH')
        vr.text(12).string = 'STAY';
    elseif strcmp(vr.text(12).string,'STAY')
        vr.text(12).string = 'TRACK';
    end    
end
    
%% data logging
% (5) Data recording.                                                                                        %
%%% (5a) Control of virmen recording (i.e. making a .dat file) %%%
if ~vr.isRecording 
    % If not recording, we need to check whether we need to start %
    % text(1) is 'POS REC START'
    if vr.textClicked==1  && ~vr.isExit 
        %for txt file output
        vr.sessionTime = datetime('now','Format','HH:mm:ss');
       
        vr.isRecording = 1;
        vr.recordingStartTime = vr.timeElapsed;
        vr.text(1).string = 'POS REC STOP';
        vr.text(1).color = [1 0 0];
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
        vr.text(3).color = [0 1 0];   % Set the 2P recording button to be 'active'.
    end
elseif vr.isRecording
    % If we are recording, check whether we need to stop  .. %
    if vr.textClicked==1 
        if ~vr.isExit
            % sanity check as protection against accidental clicks
            vr.text(1).string = 'ARE YOU SURE - CLICK AGAIN TO STOP'; 
            vr.text(1).color = [1 1 0];  
            vr.text(1).size = 0.05;
            vr.text(1).position = [-0.8 0.75];
            vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
            vr.isExit = true; %now next click will exit virmen 
        elseif vr.isExit
            % quit for real now
            %for txt file output
            vr.sessionTime(2) = datetime('now','Format','HH:mm:ss');
            % .. if user has clicked stop then stop the recording. %
            vr.isRecording = 0;
            fclose(vr.tempFileFid);
            % Reset the text on the control window %
            vr.text(1).string = 'PLEASE EXIT VIRMEN';
            vr.text(1).position = [-0.65 0.65];
            vr.text(1).size = 0.1;
            vr.text(1).color = ones(1,3) .* 0.5;
            vr.text(3).color = ones(1,3) .* 0.5;   % 2P recodring is 'inactive'
            %also block any reward shenanigans (make zones invisible etc.) 
            vr.isRewardBlock = true;
            vr.text(8).string = 'REWARD OFF';
            vr.text(8).color = [0.5 0.5 0.5];
            if  vr.currentWorld ~=4
                vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false;
            end
        end
    else 
        % If neither of the above, we are in a continuing recording, we will just update the timer text %
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
    end
end
%%% (5b) Control of 2P recording %%%
if vr.isRecording % Can only record 2P whilst virmen pos recording is active
    if ~vr.isRecording2P
        % If we are not recording, check whether we need to start  .. %
        if vr.textClicked==3
            if vr.debugMode~=1
                %txt file output
                vr.tempTime = datetime('now','Format','HH:mm:ss');
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' start:'], char(vr.tempTime) );
            end
            %switch world according to rec mode
            if strcmp(vr.text(12).string,'SWITCH') || (strcmp(vr.text(12).string,'TRACK') && strcmp(vr.exper.worlds{vr.currentWorld}.name,'holdingEnvironment'))
                vr = worldSwitch(vr);
            end
          
            % Below is when the user has started the recording
            vr.isRecording2P = true;
            vr.text(3).string = '2P REC STOP';
            vr.text(3).color = [1 0 0];
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            % .. and now actually start the 2P imaging %
            vr.recordingStartTime2P = vr.timeElapsed;
            if vr.debugMode == 0
                vr.daqSess2PFrameClock.resetCounters;            % Make sure the frame clock is at zero ..
                vr.daqSess2PStartStop.outputSingleScan([1 0]);   % .. and start the 2P recording.
            end  
        end
    elseif vr.isRecording2P
        % If we are recording, check whether we need to stop  .. %
        if vr.textClicked==3  || (vr.timeElapsed-vr.recordingStartTime2P)>vr.recordingDuration2P
            if vr.debugMode~=1
                %txt file output
                vr.tempTime(2) = datetime('now','Format','HH:mm:ss');
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' stop:'], char(vr.tempTime(2)));
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' duration (mm:ss:ms):' ],datestr(vr.tempTime(2)-vr.tempTime(1),'MM:SS:FFF'));
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' environment:' ],vr.worldNames{ vr.currentWorld });
                clear vr.tempTime
            end
            %  .. below is when we were recording, but it has been stopped by timing out, or by the user clicking '2P REC STOP' %
            vr.isRecording2P = false;
            vr.currentFileIdx2P = vr.currentFileIdx2P + 1;  % Bump the Idx counter for the scanimage file.
            vr.text(3).string = '2P REC START';
            vr.text(3).color = [0 1 0];
            vr.text(4).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
            if vr.debugMode == 0
                vr.daqSess2PStartStop.outputSingleScan([0 1]);   % .. and stop the 2P recording.
            end
            vr.worldToggle = true;
        else
            %  .. if we were recording but it hasn't been stopped, just update the timer
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            vr.worldToggle = true; %prevents manual world switch during recording
        end
    end     
end
%%% (5c) Finally, if we are recording, we need to actually record the data %%%
if vr.debugMode ~= 1 && vr.isRecording
    if vr.debugMode == 0 && vr.isRecording2P
        frameCount2P = vr.daqSess2PFrameClock.inputSingleScan;
        sessionIdx2P = vr.currentFileIdx2P;
    else
        frameCount2P = 0;
        sessionIdx2P = 0;
    end
    %%% FILE FORMAT IS HERE: [time, xPos, yPos, dir, xVel, yVel, trialIdx2P, frameCount2P, isReward, amountReward, isTurnAround world index];
    %%% The first six I want to keep standard across experiments, hence the inclusion of x pos data, even though it is irrelevant to the linear track.
    measurementsToSave = [vr.timeElapsed-vr.recordingStartTime, vr.position(1), vr.position(2), vr.position(4), vr.velocity(1),...
                            vr.velocity(2), sessionIdx2P, frameCount2P, vr.isReward, vr.currRewardDropSize, vr.turnaroundActive, vr.currentWorld,vr.passiveReward,...
                            vr.cue_reward_zone,vr.cue_off,vr.reward_tone, vr.lick_triggered,vr.mov_cue_loc,vr.pause_exp];
    if ~vr.recordingExists
        fwrite(vr.tempFileFid,length(measurementsToSave),'double');  % The very first entry (only) in the file is the number of fields.
    	vr.recordingExists = 1;                                      %  ..
    end
    fwrite(vr.tempFileFid,measurementsToSave,'double');
end

end

