%Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runBlackout(vr)
if vr.turnaroundActive
    if ~vr.worldToggle
        tempDelay = vr.switchDelay;
    else
        tempDelay = vr.teleportDelay;
    end
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer ) > tempDelay
        % Turnaround blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(:) = true; %change this later (actaully want the reward inivisible all the time)
        vr.turnaroundActive = false;
        vr.worldToggle = true; %release toggle
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

vr.dp(2) = - vr.dp(2) * vr.movementGain;

if vr.position(2) >= vr.trackLength 
    % If so, prevent any additional movement ..
    vr.dp(2) = 0;
    
    % Teleport animal to beginning of track#
    vr.position(1) =0;
    vr.position(2) = 0;
end

end  