function vr = initialize_cue_params(vr)

% loop through worlds 
% check whether each has CUE or MOVING_CUE objects
% if so, save the triangles that correspond to these cues for each world
% will use these variables during run code to manipulate cue locations/ set
% visible/ invisible
% adding binary variable, counterbalance. When this is set to 0 do nothing,
% when 1, swap root positions of cue and distractor cue (so that 

%important variables which enter the VR struct are:
%   vr.CUE_vertices_orig: index of original pos of moving cue 
%   vr.CUE_vertices_ind: index of current pos of moving cue (update to
%   move the position of the object)
%   vr.cue_triangles: index of triangles of moving cue when in original pos
%   (for making visible/invisible)

%   vr.MOV_CUE_vertices_orig: as above but for non-reward predicting cue
%   vr.MOV_CUE_vertices_ind
%   vr.mov_cue_triangles

for i = 1:length(vr.exper.worlds)
    generalizing_flag = 0;
    for j=1:length( vr.exper.worlds{i}.objects )
        if regexp( vr.exper.worlds{i}.objects{j}.fullName, regexptranslate('wildcard','REWARD_CUE*'))
            generalizing_flag = 1;
            break
        end
    end
    if generalizing_flag == 1
        switch vr.counter_balance %in some mice want to use one cue as reward predicting, in the others, use the other one
            case 0
                cue_index = vr.worlds{i}.objects.indices.REWARD_CUE;
            case 1
                cue_index = vr.worlds{i}.objects.indices.MOVING_CUE;
        end
        cue_vertices = vr.worlds{i}.objects.vertices(cue_index,:);
        cue_vertices = cue_vertices(1): cue_vertices(end);
        vr.CUE_vertices_orig{i} = vr.worlds{i}.surface.vertices(2,cue_vertices); % indices of cue when it is in home pos
        vr.CUE_vertices_ind{i} = cue_vertices; % indices of current cue pos

        tri_match = zeros(1,size(vr.worlds{i}.surface.triangulation,2));
        for vert_ind = 1:size(cue_vertices,2)
            [r,c]=find(vr.worlds{i}.surface.triangulation == cue_vertices(vert_ind));
            if ~isempty(c)
                tri_match(c) = 1;
            end
        end
        vr.cue_triangles{i} = find(tri_match); %cue surface triangles is used to set the cue to be invisible (note we don't have to update these indices when move cue if, as currently, always move cue to home position before setting invisible)
        clear cue_vertices cue_index tri_match cue_index
    else
        vr.CUE_vertices_orig{i} = NaN;
        vr.CUE_vertices_ind{i} = NaN;
        vr.cue_triangles{i} = NaN;
    end
        % moving non-rewarding CUE
        generalizing_flag = 0;
        for j=1:length( vr.exper.worlds{i}.objects )
            if regexp( vr.exper.worlds{i}.objects{j}.fullName, regexptranslate('wildcard','REWARD_CUE*'))
                generalizing_flag = 1;
                break
            end
        end 
        if generalizing_flag == 1
            switch vr.counter_balance
                case 0
                    mov_cue_index = vr.worlds{i}.objects.indices.MOVING_CUE;
                case 1
                    mov_cue_index = vr.worlds{i}.objects.indices.REWARD_CUE;
            end
            cue_vertices = vr.worlds{i}.objects.vertices(mov_cue_index,:);
            cue_vertices = cue_vertices(1): cue_vertices(end);
            vr.MOV_CUE_vertices_orig{i} = vr.worlds{i}.surface.vertices(2,cue_vertices);
            vr.MOV_CUE_vertices_ind{i} = cue_vertices;

            tri_match = zeros(1,size(vr.worlds{i}.surface.triangulation,2));
            for vert_ind = 1:size(cue_vertices,2)
                [r,c]=find(vr.worlds{i}.surface.triangulation == cue_vertices(vert_ind));
                if ~isempty(c)
                    tri_match(c) = 1;
                end
            end
            vr.mov_cue_triangles{i} = find(tri_match);
            clear cue_vertices tri_match
        else
            vr.MOV_CUE_vertices_orig{i} = NaN;
            vr.MOV_CUE_vertices_ind{i} = NaN;
            vr.mov_cue_triangles{i} = NaN;
        end        
            
end


end