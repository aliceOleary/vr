function vr = reset_cue_reward_zones(vr)

vr.cue_reward_zone = NaN;
vr.block_counter = 1;
vr.cue_off = 1;
vr.CuerewardIsActive = 1;
vr.mov_cue_loc = NaN;

vr.worlds{vr.currentWorld}.surface.vertices(2,vr.CUE_vertices_ind{vr.currentWorld})= vr.CUE_vertices_orig{vr.currentWorld};
vr.worlds{vr.currentWorld}.surface.vertices(2,vr.MOV_CUE_vertices_ind{vr.currentWorld})= vr.MOV_CUE_vertices_orig{vr.currentWorld};

vr.worlds{vr.currentWorld}.surface.visible(vr.cue_triangles{vr.currentWorld}) = false;
vr.worlds{vr.currentWorld}.surface.visible(vr.mov_cue_triangles{vr.currentWorld}) = false;


end