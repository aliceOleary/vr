%Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runGreenTrack(vr)
%% turn around control
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a turnaround invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.turnaroundActive
    if ~vr.worldToggle
        tempDelay = vr.switchDelay;
    else
        tempDelay = vr.teleportDelay;
    end
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer ) > tempDelay
        % Turnaround blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(~vr.rewardLocInd{vr.currentWorld}) = true; %change this later (actaully want the reward inivisible all the time)
        vr.turnaroundActive = false;
        if ~vr.isRewardBlock
            %switch on rewards
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }   ) = true; %change this later
            %blank reward animal is standing on (in case he moves backwards)
        end
        vr.worldToggle = true; %release toggle
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

if ~isempty(vr.pumpTimer)
    if (vr.timeElapsed - vr.pumpTimer) > vr.PumpDelay % pumpDelay- how long the reward can sit there before pumped away
        putdata(vr.pumpAO,vr.pumpSignal);   start(vr.pumpAO);
        vr.pumpTimer = [];
    end
end

%% displacement
% (2) Get displacement and scale (gain)                                                                     %
vr.dp(2) = -vr.dp(2) * vr.movementGain;

% Check that we haven't run outside of the track (virmen edge detection
% sometimes showing bugs). Do we have to lose 4cm?
% if vr.position(2) < 1 
%     vr.position(2) = 1;   vr.dp(2) = 0;
% elseif vr.position(2) > vr.trackLength-4
%     vr.position(2) = vr.trackLength-4;   vr.dp(2) = 0;   
% end

%% rewards
% (3) Detection and control of rewards
% activate/deactivate reward
if vr.textClicked==8
    if vr.isRewardBlock && ~vr.isExit
        vr.text(8).string = 'REWARD ON';
        vr.text(8).color = [0 1 0];
        vr.worlds{ vr.currentWorld }.surface.visible(   vr.rewardLocInd{ vr.currentWorld }  ) = true;   %change this later
        vr.isRewardBlock = false;
    else
        vr.text(8).string = 'REWARD OFF';
        vr.text(8).color = [0.5 0.5 0.5];    
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false; 
        vr.isRewardBlock = true;
    end
end
%increase reward manually 
if ~vr.isExit && (vr.textClicked==6 || vr.textClicked==7)
    if vr.textClicked==6
        vr.currRewardDropSize = vr.currRewardDropSize + 1;
        if vr.currRewardDropSize > vr.maxPossRewardDropSize
            vr.text(6).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = vr.maxPossRewardDropSize;
        end
        if vr.currRewardDropSize > 1
            vr.text(7).color = [0 1 0];
        end
    end
    %decrease reward manually
    if vr.textClicked==7
        vr.currRewardDropSize = vr.currRewardDropSize - 1;
        if vr.currRewardDropSize <= 1
            vr.text(7).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = 1;
        end
        if vr.currRewardDropSize < vr.maxPossRewardDropSize
            vr.text(6).color = [0 1 0];
        end
    end
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
    vr.text(6).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
    vr.text(7).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
end

if vr.DetectLicking==1
    if vr.PhotoLickDetection.Licks  
                vr.licksCurrentTrial=vr.licksCurrentTrial+1;
                vr.text(16).string = ['NUMBER OF LICKS: ' num2str(vr.licksCurrentTrial)];
    end
end
% Test whether the the next active reward has been reached, if so, give it.

 % This is the case where the animal has reached the reward zone %
    %now add lick detection requirement
hFun = @gt; 
h2Fun = @lt; 
if vr.passiveReward== 0
    
    if hFun( vr.position(2)+ vr.rewardTriggerRadius, vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) )...
            && h2Fun ( vr.position(2), vr.rewardLocations{ vr.currentWorld }( vr.nextReward )+((vr.rewardTriggerRadius)) )

        if vr.PhotoLickDetection.Licks  
            if ~vr.isRewardBlock && vr.rewardIsActive{vr.currentWorld}
                vr.isReward = 1;
                disp(vr.position(2));
                vr.text(16).color = [0 1 0];
            end
        end
    end
    
elseif vr.passiveReward == 1  
    if hFun( vr.position(2) - vr.rewardTriggerRadius, vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) ) 
        if ~vr.isRewardBlock && vr.rewardIsActive{vr.currentWorld}
            vr.isReward = 1;
            
        end
    else
    end
    
elseif vr.passiveReward == 2
    if hFun( vr.position(2) + vr.rewardTriggerRadius, vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) )...
            && h2Fun ( vr.position(2), vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) + ((vr.rewardTriggerRadius)))

        if vr.PhotoLickDetection.Licks  
            if ~vr.isRewardBlock && vr.rewardIsActive{vr.currentWorld}
                vr.isReward = 1;
               
                vr.text(16).color = [0 1 0];
            end
        end
    end
    if hFun( vr.position(2) - vr.rewardTriggerRadius, vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) ) 
        if ~vr.isRewardBlock && vr.rewardIsActive{vr.currentWorld}
            vr.isReward = 1;
            
        end
    end
end
   
if ~vr.isRewardBlock && vr.textClicked==5
    % This is the case where the experimenter has clicked the 'REWARD NOW' button. %
    vr.isReward = 2;
end

% If there is a reward, drive solenoid % || h2Fun( vr.position(2), vr.rewardLocations{ vr.currentWorld }( vr.nextReward )+vr.rewardTriggerRadius )
%if vr.isReward && vr.debugMode == 0 && strcmp(get(vr.rewardAO,'Running'),'Off')
if vr.isReward > 0 && strcmp(get(vr.rewardAO,'Running'),'Off')

    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
    if vr.reward_tone
        sound(vr.reward_tone_sound.sound,vr.reward_tone_sound.fs)
    end
    %Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
    if vr.isReward == 1
        vr.rewardIsActive{vr.currentWorld} = 0; %animal can only recieve one reward per visit to reward zone. N.B this will need to be adjusted if nreward zones > 1
    end
%          vr.pumpTimer = vr.timeElapsed;
    vr.isReward = 0;
end

%% End of track control
% (4) Has the end of the track been reached?
if vr.position(2) >= vr.trackLength 
    % If so, prevent any additional movement ..
    vr.dp(2) = 0;
    
    % Teleport animal to beginning of track#
     vr.position(1) =0;
    vr.position(2) = 0;%vr.position(2)-200;
     vr.licksCurrentTrial=0;
     vr.text(16).color = [1 0 0];
     vr.text(16).string = ['NUMBER OF LICKS: ' num2str(vr.licksCurrentTrial)];
%     % Make the world invisible, and set the timer for turning the world visible again %
    vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
    
    vr.turnaroundTimer  = vr.timeElapsed;
    vr.turnaroundActive = true;
    % Reset the rewards as active %
    vr.rewardIsActive{vr.currentWorld} = 1;
    vr.linearTrackLap= vr.linearTrackLap+1;
    vr.text(19).string = ['RUNS: ' num2str(vr.linearTrackLap)];
%  
   
%     
%     if ~vr.isRewardBlock
%         vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;
%     end
end
end  