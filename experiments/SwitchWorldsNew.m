function vr = SwitchWorldsNew(vr)
%control of world switch for standard VR

%% world control

if vr.textClicked==9 
    vr.currentWorld = 2;
    vr.text(9).color = ([255 255 0]./255);
    vr.text(10).color = [1 1 1];
    vr.text(15).color = [1 1 1];
elseif vr.textClicked==10 
    vr.currentWorld = 1;
    vr.text(10).color = ([255 255 0]./255);
    vr.text(9).color = [1 1 1];
    vr.text(15).color = [1 1 1]; 
     vr.text(17).color = [1 1 1]; 
    vr.nextReward = 1;
elseif vr.textClicked==17 
    vr.currentWorld = 3;
    vr.text(17).color = ([255 255 0]./255);
    vr.text(9).color = [1 1 1];
    vr.text(10).color = [1 1 1];
    vr.text(15).color = [1 1 1];
    vr.nextReward = 1;
elseif vr.textClicked==15 
    vr.currentWorld = 4;
    vr.text(15).color = ([255 255 0]./255);
    vr.text(10).color = [1 1 1];
    vr.text(9).color = [1 1 1];
    vr.text(17).color = [1 1 1];
elseif vr.textClicked==28 
    vr.currentWorld = 5;
    vr.text(28).color = ([255 255 0]./255);
    vr.text(15).color = [1 1 1];
    vr.text(10).color = [1 1 1];
    vr.text(9).color = [1 1 1];
    vr.text(17).color = [1 1 1];
end
    


%blank all worlds
for i = 1:size(vr.worlds,2)
    vr.worlds{ i }.surface.visible(:) = false;
end
%activate things for transfer delay - use same logic as for turn around
vr.turnaroundTimer = vr.timeElapsed;
vr.turnaroundActive = true;
%set to start pos
vr.position = vr.exper.worlds{vr.currentWorld}.startLocation;



%in the hold environment the reward needs to be moved back to original
%location
if vr.textClicked==9 
    %if ~vr.isRewardBlock
    %shift first reward after flip, so it is right in front of animal
    
    vr.nextReward = 1;

    %shift reward
    vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1})...
        = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) + 30;
    vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) + 30;
    vr.isfirstRewardShift = true; %flag shift
% else
%     
%      vr.nextReward = 1;
%    
%     
%     end

    
    
    
vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{2})...
    = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) + vr.interRewardDist;
vr.rewardLocations{vr.currentWorld }(2) =  vr.rewardLocations{vr.currentWorld }(1) + vr.interRewardDist;
% %     vr.isfirstRewardShift = true; %flag shift
end

%toggle will block clicking switch world in delay
vr.worldToggle = false;
%vr.isfirstRewardShift = true;
%reset cue reward params
if ~isnan(vr.cue_triangles{ vr.currentWorld}) % resetting these indices only makes sense for generalization tracjs
    vr = reset_cue_reward_zones(vr);
end

end