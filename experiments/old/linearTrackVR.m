function code = linearTrackVR
% linearTrackWithCylinders   Code for the ViRMEn experiment linearTrackWithCylinders.
%   code = linearTrackWithCylinders   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.

% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- INITIALIZATION code: executes before the ViRMEN engine starts. %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = initializationCodeFun(vr)

%% hardware initialisation
% First, there are a couple of things that we only do when it is a real trial: initialise DAQ hardware, and data save file.
% Important note on 'vr.debugMode':scanimage
%    0=no debugging, all hardware is present, run it for real, 
%    1=full debug mode active, no hardware required no files written
%    2=no hardware interaction, but will save data files.
vr.debugMode = eval(vr.exper.variables.debugMode);

if vr.debugMode == 0
    vr = initialiseHardware(vr);
end

%% UI input for log file
% (2) Get some trial name information, and 2P recording time. This needs to come first, not post-trial, as the user needs to input the current scanimage trial_XXX number %
% First, get user input on trial names etc %
trialConfigInput = inputdlg({'Mouse number','Virmen trial letter (CAPITALS please!)','2P starting trial number','2P default trial length'},'Enter trial info',[1; 1; 1; 1], ...
                            {'m234', 'A', '1', vr.exper.variables.default2PhotonRecordingDuration});
% Save these to vr. struct.
vr.virmenFileLetter = trialConfigInput{2};
vr.finalFileName = [trialConfigInput{1} '_' datestr(now,'yyyymmdd') '_' vr.virmenFileLetter];
vr.currentFileIdx2P = str2double( trialConfigInput{3} );
vr.recordingDuration2P = str2double(trialConfigInput{4});

%% Initialisation
% (4) Initialise some key variables in the vr. structure. %
% (4a) Rewards - world specific:
for i = 1:length(vr.exper.worlds)
    for j=1:length( vr.exper.worlds{i}.objects )
        if regexp( vr.exper.worlds{i}.objects{j}.fullName, regexptranslate('wildcard','rewardZone*') ); ind=j; break; end
    end
    
    vr.rewardLocations{i} = vr.exper.worlds{i}.objects{ind}.y;  % This contains a 1:nReward list of the y locations of the reward zone centres.
    vr.nRewardZones{i} = length(vr.rewardLocations{i});
    %
    vr.rewardIsActive{i} = false(vr.nRewardZones{i},1);  % This will control: a) is it visible, b) can it drive the solenoid output
    %
    % (4b) Rewards: also need the 'raw' vertices and triangles that constitute the reward zones, so that they can be made invisible when inactive.
    % This is tricky, as all of the different vertices and triangles are grouped together under one object, so we have to first
    % retrive them, then split them up on the basis of their location in the world.
    temp_fNames = fieldnames(vr.worlds{i}.objects.indices);
    ind = ~cellfun('isempty',strfind(temp_fNames,'rewardZone')); %as reward zones are called differently in each world this needs to be checked a bit cumbersome
    clear temp_fNames
    
    rewardVerInd = vr.worlds{i}.objects.vertices(ind,:);   % = the 'start' and 'stop' inds of the vertices that correspond to the rewardZone object
    rVert = vr.worlds{i}.surface.vertices(:,rewardVerInd(1):rewardVerInd(2));  % These are now the actual vertices
    rr = eval( vr.exper.variables.rewardRadius );  % This is the reward radius, NOTE, it is a string in vr., you need to 'eval' it.
    vr.rewardTriIndByZone{i} = cell(length(vr.rewardLocations{i}),1);        % This is going to be the indices of triangles which belong to which reward zone
    for k=1:length(vr.rewardLocations{i})                               % based on the position on the track.
        % For each reward zone, which vertices are within poistion +/- radius?
        singleZoneVertInd = find(  rVert(2,:)>vr.rewardLocations{i}(k)-(rr*1.2) & rVert(2,:)<vr.rewardLocations{i}(k)+(rr*1.2)  )  +  rewardVerInd(1)  -   1;   % Need to add 'rewardVerInd(1)-1', so that it becomes an index into the *full* vertex array, not just that for reward zone.
        % .. unfortunately, only triangles, not vertices have the 'visibility' property, so we now need to work out  which traingles use these vertices ..
        temp = ismember( vr.worlds{i}.surface.triangulation, singleZoneVertInd );
        vr.rewardTriIndByZone{i}{k} = any( temp, 1 );   % And then store this index
        %save index of first zone - we need that for shifting that around
        if k == 1
            vr.firstRewardVertices{i} = singleZoneVertInd;
        end
    end
end
%some general reward related stuff (global)
vr.nRewardsGiven = 0;  % This keeps track of the total number of rewards recieved by the mouse
vr.rewardTriggerRadius = eval( vr.exper.variables.rewardTriggerRadius );  % This sets how close to the reward centre to actually trigger the reward.
vr.currRewardDropSize = eval(vr.exper.variables.rewardDropSize); %for updating the reward drop size
vr.maxPossRewardDropSize = eval( vr.exper.variables.maxPossRewardDropSize ); %max possible reward drop size (5)
vr.sRateSolanoid = eval(vr.exper.variables.sRateSolanoid);
vr.rewardShift = eval( vr.exper.variables.firstRewardShift );
vr.rewardZoneSize = eval(vr.exper.variables.rewardHeight);
vr.isRewardBlock = true;      % blocks reward activation
vr.isRewardShift = false; %state variable for reward shift

% (4c) Some other general variables %
vr.isRecording = false;      % Are we currently recording virmen data?
vr.isExit = false;           % fail safe so exiting includes sanity check
vr.isRecording2P = false;    % Are we also currently recording 2P data?
vr.recordingStartTime = [];      % Keep track of the start time for the virmen recording session
vr.recordingStartTime2P = [];   % Keep track of the start time for the current 2P recording session
vr.recordingExists = false;   % Set this to true if the virmen recording is run, so we know to deal with file in the termination function.
vr.currentDirection = eval(vr.exper.variables.startDirection); % The current direction of the animal on the track, 1=N (increasing y), -1=S (decreasing y)
vr.turnaroundActive = false;     % Signals whether the animal is currently in the turnaround screen-blanked period 
vr.turnaroundStartTime = [];     % Will act as a timer, such that the turnaround blanking is of the correct duration.
vr.turnaroundDelay = eval( vr.exper.variables.turnaroundDelay );  % Get the turnaround blanking duration (user-set) in a more convenient format.
vr.trackLength = eval( vr.exper.variables.trackLength );
vr.movementGain = eval( vr.exper.variables.movementGain );
vr.monitorAspectRatio = eval( vr.exper.variables.monitorAspectRatio );
vr.perspectiveScale = eval( vr.exper.variables.perspectiveScale );
vr.wheelCircumference = eval( vr.exper.variables.wheelCircumference );
if vr.debugMode~=1
    vr.tempFileDir = 'F:\';
    vr.tempFileLocation = [vr.tempFileDir 'virmenTempData.dat'];
    vr.tempFileLocationTXT = [vr.tempFileDir 'virmenTempData.txt'];
    if ~isdir(vr.tempFileDir)
        mkdir(vr.tempFileDir);
    end
    vr.tempFileFid = fopen(vr.tempFileLocation,'w');  % The actual data always goes in this temp file during the recording.
    vr.tempFileFidTXT = fopen(vr.tempFileLocationTXT,'w');
    %write header
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','########## Header ##########');
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Mouse ID:', trialConfigInput{1});
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Date:', datestr(now,'yyyymmdd'));
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','Data file name:', vr.finalFileName);
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','############################');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','### Imaging Session Info ###');
end

%switch to holding env
vr.currentWorld = strcmp(cellfun(@(x) x.name,vr.exper.worlds,'UniformOutput',false),'holdingEnvironment'); 
vr.worldToggle = true; %will prevent world to be switched within switch over delay
vr.switchDelay = eval( vr.exper.variables.worldSwitchDelay ); %delay when switching between worlds
vr.worldNames = cellfun(@(x) x.name,vr.exper.worlds,'UniformOutput',false); %world name is not easily accessible otherwise

%% Textbox controls
% (3) Initialise text boxes for controls %
% (3a) Pos recording start/stop button
textWindowIdx = [5, 6, 7];
n=1;
vr.text(n).string = 'POS REC START';
vr.text(n).position = [-0.65 0.65];
vr.text(n).size = 0.1;
vr.text(n).color = [0 1 0];  % Should be green initially, switch to red for 'STOP'
vr.text(n).window = textWindowIdx(1);
% (3b) Pos recording time elapsed timer (shows trial name when not recording)
n=2;
vr.text(n).string = ['(RECORD TO FILE ' vr.virmenFileLetter, ')']; % This refers to the virmen .dat file name
vr.text(n).position = [-0.73 0.5];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];  % White
vr.text(n).window = textWindowIdx(1);
% (3c) 2P record start/stop button
n=3;
vr.text(n).string = '2PHOTON REC START';
vr.text(n).position = [-0.85 0.05];
vr.text(n).size = 0.1;
vr.text(n).color = [0.5 0.5 0.5];   % Grey initially, switch to green (for active) when virmen recording is active
vr.text(n).window = textWindowIdx(1);
% (3d) 2P record time elapsed timer (when not recording, displays trial # to be used next)
n=4;
vr.text(n).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
vr.text(n).position = [-0.84 -0.1];
vr.text(n).size = 0.08;
vr.text(n).color = [1 1 1];
vr.text(n).window = textWindowIdx(1);
% (3e) button will change what happens when  recording is started
n=11;
vr.text(n).string = '2PHOTON REC MODE';
vr.text(n).position = [-0.85 -0.57];
vr.text(n).size = 0.1;
vr.text(n).color = [1 1 1];  
vr.text(n).window = textWindowIdx(1);
% modes: 'switch' - switch world from current world; 'stay' - record in
% current world; 'track' - record on track (i.e. switch when in HP, stay
% when already on track
n=12;
vr.text(n).string = 'TRACK';
vr.text(n).position = [-0.3 -0.8];
vr.text(n).size = 0.1;
vr.text(n).color = [1 0 0];   
vr.text(n).window = textWindowIdx(1);
% (3f) Immediate reward button.
n=5;
vr.text(n).string = 'REWARD NOW';
vr.text(n).position = [-0.5 -0.65];
vr.text(n).size = 0.1;
vr.text(n).color = [0 0 1];
vr.text(n).window = textWindowIdx(2);
% (3g) buttons to regulate amount of reward
n=6;
if vr.currRewardDropSize > vr.maxPossRewardDropSize
    vr.text(n).color = [0.5 0.5 0.5];
    vr.currRewardDropSize = vr.maxPossRewardDropSize;
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
else
    vr.text(n).color = [0 1 0];
end
vr.text(n).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
vr.text(n).position = [-0.6 0.2];
vr.text(n).size = 0.1;
vr.text(n).window = textWindowIdx(2);

n=7;
vr.text(n).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
vr.text(n).position = [-0.6 -0.05];
vr.text(n).size = 0.1;
if vr.currRewardDropSize <= 1
    vr.text(n).color = [0.5 0.5 0.5];
    vr.currRewardDropSize = 1;
    vr.rewardPattern = [ 5; 0; 0; 0; 0 ];
else
    vr.text(n).color = [0 1 0];
end
vr.text(n).window = textWindowIdx(2);
% this button will activate feeder so animal can be rewarded
n=8;
vr.text(n).string = 'REWARD OFF';
vr.text(n).position = [-0.5 0.65];
vr.text(n).size = 0.1;
vr.text(n).color = [0.5 0.5 0.5];
vr.text(n).window = textWindowIdx(2);
% button to switch manually between worlds
n=9;
vr.text(n).string = 'SWITCH WORLD';
vr.text(n).position = [-0.6 0.65];
vr.text(n).size = 0.1;
vr.text(n).color = [1 0 0];
vr.text(n).window = textWindowIdx(3);
% name of current world
n=10;
vr.text(n).string = 'HOLD';
vr.text(n).position = [-0.18 0.3];
vr.text(n).size = 0.1;
vr.text(n).color = [0.8 0.8 0.8];
vr.text(n).window = textWindowIdx(3);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (5) Finally, we actually need to initialise some things about the first 
%     run in the world.
% Set the starting reward to be inactive and invisible, and set vr.nextReward correctly %
if vr.currentDirection == 1         % Depending the starting direction, inactive the first reward the animal will see at the end of the track.
    vr.nextReward = 2;                
else 
    vr.nextReward = vr.nRewardZones{ vr.currentWorld } - 1;  
end

for i = 1:length(vr.exper.worlds)
    %set all rewards to eb invisible
    tempInd =  [ vr.rewardTriIndByZone{ i }{  ~vr.rewardIsActive{ i } } ];
    vr.rewardLocInd{i} =  any( reshape( tempInd,length(vr.worlds{ i }.surface.visible),[] ),2 ); %save index for convenience
    vr.worlds{ i }.surface.visible( vr.rewardLocInd{i} ) = false;  % Set rewards to be invisible.
end
assignin('base','vr',vr);   % For debugging purposes.

%% Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runtimeCodeFun(vr)

%% world control
%Check if world has to be switched
if vr.textClicked==9 && vr.worldToggle
    if strcmp(vr.text(10).string,'HOLD')
        vr.currentWorld = strcmp(vr.worldNames,'linearTrack'); 
        vr.text(10).string = 'TRACK';
    else
        vr.currentWorld = strcmp(vr.worldNames,'holdingEnvironment'); 
        vr.text(10).string = 'HOLD';
    end
    %blank both worlds
    vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
    vr.worlds{ ~vr.currentWorld }.surface.visible(:) = false;
    %activate things for transfer delay - use same logic as for turn around
    vr.turnaroundTimer = vr.timeElapsed;
    vr.turnaroundActive = true;
    %set to start pos
    vr.position = vr.exper.worlds{vr.currentWorld}.startLocation;
    vr.currentDirection = eval(vr.exper.variables.startDirection);    
    
    if ~vr.isRewardBlock
        %shift first reward after flip, so it is right in front of animal
        if vr.currentDirection == 1
            vr.nextReward = 1;
        else
            vr.nextReward = vr.nRewardZones{ vr.currentWorld };
        end
        %shift reward
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld}) + vr.rewardShift;
        vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) + vr.rewardShift;
        vr.isRewardShift = true; %flag shift
    else
        if vr.currentDirection == 1
            vr.nextReward = 2;
        else
            vr.nextReward = vr.nRewardZones{ vr.currentWorld } - 1;
        end
    end
    %toggle will block clicking switch world in delay
    vr.worldToggle = false;
end
%we need this index quite a lot, so store it
vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{  vr.nextReward };

%%
if vr.isExit && ~isnan(vr.textClicked) && vr.textClicked~=1 && ~strcmp(vr.text(1).string,'PLEASE EXIT VIRMEN') 
    vr.text(1).string = 'POS REC STOP';
    vr.text(1).color = [1 0 0];
    vr.text(1).position = [-0.65 0.65];
    vr.text(1).size = 0.1;
    vr.isExit = false;
end

%% turn around control
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a turnaround invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.turnaroundActive
    if ~vr.worldToggle
        tempDelay = vr.switchDelay;
    else
        tempDelay = vr.turnaroundDelay(vr.currentWorld);
    end
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer) > tempDelay
        % Turnaround blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(~vr.rewardLocInd{vr.currentWorld}) = true;
        vr.turnaroundActive = false;
        if ~vr.isRewardBlock
            %switch on next possible reward
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd   ) = true;
        end
        
        vr.worldToggle = true; %release toggle
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

%% displacement
% (2) Get displacement and scale (gain)                                                                     %
vr.dp(2) = -vr.dp(2) * vr.movementGain;
% If using rotary encoder, need to flip the displacement when heading south 
% (so the same wheel direction keeps the mouse moving 'forward').
vr.dp(2) = vr.dp(2) * vr.currentDirection;
% Check that we haven't run outside of the track (virmen edge detection sometimes showing bugs).
if vr.position(2) < 1 
    vr.position(2) = 1;   vr.dp(2) = 0;
elseif vr.position(2) > vr.trackLength-4 && strcmp(vr.exper.worlds{vr.currentWorld}.name,'linearTrack')
    vr.position(2) = vr.trackLength-4;   vr.dp(2) = 0;   
end

%% rewards
% (3) Detection and control of rewards
% activate/deactivate reward
if vr.textClicked==8
    if vr.isRewardBlock && ~vr.isExit
        vr.text(8).string = 'REWARD ON';
        vr.text(8).color = [0 1 0];

        vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;  
        vr.isRewardBlock = false;
    else
        vr.text(8).string = 'REWARD OFF';
        vr.text(8).color = [0.5 0.5 0.5];    
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false; 
        vr.isRewardBlock = true;
    end
end
%increase reward manually 
if ~vr.isExit && (vr.textClicked==6 || vr.textClicked==7)
    if vr.textClicked==6
        vr.currRewardDropSize = vr.currRewardDropSize + 1;
        if vr.currRewardDropSize > vr.maxPossRewardDropSize
            vr.text(6).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = vr.maxPossRewardDropSize;
        end
        if vr.currRewardDropSize > 1
            vr.text(7).color = [0 1 0];
        end
    end
    %decrease reward manually
    if vr.textClicked==7
        vr.currRewardDropSize = vr.currRewardDropSize - 1;
        if vr.currRewardDropSize <= 1
            vr.text(7).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = 1;
        end
        if vr.currRewardDropSize < vr.maxPossRewardDropSize
            vr.text(6).color = [0 1 0];
        end
    end
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
    vr.text(6).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
    vr.text(7).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
end

% Test whether the the next active reward has been reached, if so, give it.
if vr.currentDirection==1;  hFun = @gt;  else  hFun = @lt;  end

if hFun( vr.position(2)+(vr.rewardTriggerRadius*vr.currentDirection), vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) )
    % This is the case where the animal has reached the reward zone %
    isReward = 1;
    % Make this reward zone invisible %
    vr.worlds{ vr.currentWorld }.surface.visible( vr.rewardTriIndByZone{ vr.currentWorld }{vr.nextReward} ) = 0;
    vr.rewardIsActive{ vr.currentWorld }( vr.nextReward ) = false;
    % Set the next zone to test for %
    vr.nextReward = vr.nextReward + vr.currentDirection;  % .. as vr.currentDireciton is coded as 1 or -1.
    %shift back
    if vr.isRewardShift
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld}) - vr.rewardShift;
        vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) - vr.rewardShift;
        vr.isRewardShift = false;
        if ~vr.isRewardBlock
            %turn on the next one here
            vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{  vr.nextReward };
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;
        end
    end
    
    if ~vr.isRewardBlock && strcmp(vr.exper.worlds{vr.currentWorld}.name,'holdingEnvironment')
        vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{  vr.nextReward };
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;  
    end   
       
elseif vr.textClicked==5
    % This is the case where the experimenter has clicked the 'REWARD NOW' button. %
    isReward = 1;
else
    isReward = 0;
end

if vr.isRewardBlock
    isReward = 0;
end
% If there is a reward, drive solenoid %
if isReward && vr.debugMode == 0 && strcmp(get(vr.rewardAO,'Running'),'Off')
    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
    %Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
end

%% End of track control
% (4) Has the end of the track been reached? 
if strcmp(vr.exper.worlds{vr.currentWorld}.name,'holdingEnvironment')
    if vr.position(2) >= 200 + vr.exper.worlds{vr.currentWorld}.startLocation(2)
        %reset to beginning
        vr.position(1) = 0;
        vr.position(2) = vr.position(2) - 200; %back at original start +/- jitter

        vr.turnaroundActive = false;
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = false;
        
        vr.nextReward = 2;
        if ~vr.isRewardBlock
            vr.currRewardInd  =  vr.rewardTriIndByZone{ vr.currentWorld }{  vr.nextReward };
            vr.worlds{ vr.currentWorld }.surface.visible(   vr.currRewardInd  ) = true;
        end
        
    end
    
elseif strcmp(vr.exper.worlds{vr.currentWorld}.name,'linearTrack')
    
    if any(vr.nextReward==[0 vr.nRewardZones{ vr.currentWorld }+1])
        % If so, prevent any additional movement ..
        vr.dp(2) = 0;
        
        % Turn the animal around
        vr.position(4) = vr.position(4) - pi;  % Turn around
        vr.currentDirection = -vr.currentDirection;  % Also update the direction flag.
        % Make the world invisible, and set the timer for turning the world visible again %
        vr.worlds{ vr.currentWorld }.surface.visible(:) = false;

        vr.turnaroundTimer = vr.timeElapsed;
        vr.turnaroundActive = true;
        % Reset the rewards as active %
        vr.rewardIsActive{vr.currentWorld}(:) = true;
        vr.nextReward = 2; % direction is always == 1 in HP
        if ~vr.isRewardBlock
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;
        end
    end   
end  

%% recording mode control
%control of 2P recording mode - just switch string
if ~vr.isExit && vr.textClicked==11    
    if strcmp(vr.text(12).string,'TRACK')
        vr.text(12).string = 'SWITCH';
    elseif strcmp(vr.text(12).string,'SWITCH')
        vr.text(12).string = 'STAY';
    elseif strcmp(vr.text(12).string,'STAY')
        vr.text(12).string = 'TRACK';
    end    
end
    
%% data logging
% (5) Data recording.                                                                                        %
%%% (5a) Control of virmen recording (i.e. making a .dat file) %%%
if ~vr.isRecording 
    % If not recording, we need to check whether we need to start %
    % text(1) is 'POS REC START'
    if vr.textClicked==1  && ~vr.isExit 
        %for txt file output
        vr.sessionTime = datetime('now','Format','HH:mm:ss');
       
        vr.isRecording = 1;
        vr.recordingStartTime = vr.timeElapsed;
        vr.text(1).string = 'POS REC STOP';
        vr.text(1).color = [1 0 0];
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
        vr.text(3).color = [0 1 0];   % Set the 2P recording button to be 'active'.
    end
elseif vr.isRecording
    % If we are recording, check whether we need to stop  .. %
    if vr.textClicked==1 
        if ~vr.isExit
            % sanity check as protection against accidental clicks
            vr.text(1).string = 'ARE YOU SURE - CLICK AGAIN TO STOP'; 
            vr.text(1).color = [1 1 0];  
            vr.text(1).size = 0.05;
            vr.text(1).position = [-0.8 0.75];
            vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
            vr.isExit = true; %now next click will exit virmen 
        elseif vr.isExit
            % quit for real now
            %for txt file output
            vr.sessionTime(2) = datetime('now','Format','HH:mm:ss');
            % .. if user has clicked stop then stop the recording. %
            vr.isRecording = 0;
            fclose(vr.tempFileFid);
            % Reset the text on the control window %
            vr.text(1).string = 'PLEASE EXIT VIRMEN';
            vr.text(1).position = [-0.65 0.65];
            vr.text(1).size = 0.1;
            vr.text(1).color = ones(1,3) .* 0.5;
            vr.text(3).color = ones(1,3) .* 0.5;   % 2P recodring is 'inactive'
            %also block any reward shenanigans (make zones invisible etc.) 
            vr.isRewardBlock = true;
            vr.text(8).string = 'REWARD OFF';
            vr.text(8).color = [0.5 0.5 0.5];
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false;     
        end
    else 
        % If neither of the above, we are in a continuing recording, we will just update the timer text %
        vr.text(2).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) ' SEC' ];
    end
end
%%% (5b) Control of 2P recording %%%
if vr.isRecording % Can only record 2P whilst virmen pos recording is active
    if ~vr.isRecording2P
        % If we are not recording, check whether we need to start  .. %
        if vr.textClicked==3
            if vr.debugMode~=1
                %txt file output
                vr.tempTime = datetime('now','Format','HH:mm:ss');
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' start:'], char(vr.tempTime) );
            end
            
            if strcmp(vr.text(12).string,'SWITCH') || (strcmp(vr.text(12).string,'TRACK') && strcmp(vr.exper.worlds{vr.currentWorld}.name,'holdingEnvironment'))
                
                if strcmp(vr.text(10).string,'HOLD')
                    vr.currentWorld = strcmp(vr.worldNames,'linearTrack');
                    vr.text(10).string = 'TRACK';
                else
                    vr.currentWorld = strcmp(vr.worldNames,'holdingEnvironment');
                    vr.text(10).string = 'HOLD';
                end
                %blank both worlds
                vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
                vr.worlds{ ~vr.currentWorld }.surface.visible(:) = false;
                %activate things for transfer delay
                vr.turnaroundTimer = vr.timeElapsed;
                vr.turnaroundActive = true;
                %set to start pos
                vr.position = vr.exper.worlds{vr.currentWorld}.startLocation;
                vr.currentDirection = eval(vr.exper.variables.startDirection);

                %shift first reward after flip, so it is right in front of
                %animal
                if vr.currentDirection == 1
                    vr.nextReward = 1;
                else
                    vr.nextReward = vr.nRewardZones{ vr.currentWorld };
                end                
                vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld})...
                        = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.firstRewardVertices{vr.currentWorld}) + vr.rewardShift;
                vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) + vr.rewardShift; %also need to shift location
                vr.isRewardShift = true; %flag shift
                %toggle will block clicks in delay
                vr.worldToggle = false;
            end
            
            % Below is when the user has started the recording
            vr.isRecording2P = true;
            vr.text(3).string = '2P REC STOP';
            vr.text(3).color = [1 0 0];
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            % .. and now actually start the 2P imaging %
            vr.recordingStartTime2P = vr.timeElapsed;
            if vr.debugMode == 0
                vr.daqSess2PFrameClock.resetCounters;            % Make sure the frame clock is at zero ..
                vr.daqSess2PStartStop.outputSingleScan([1 0]);   % .. and start the 2P recording.
            end  
        end
    elseif vr.isRecording2P
        % If we are recording, check whether we need to stop  .. %
        if vr.textClicked==3  || (vr.timeElapsed-vr.recordingStartTime2P)>vr.recordingDuration2P
            if vr.debugMode~=1
                %txt file output
                vr.tempTime(2) = datetime('now','Format','HH:mm:ss');
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' stop:'], char(vr.tempTime(2)));
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' duration (mm:ss:ms):' ],datestr(vr.tempTime(2)-vr.tempTime(1),'MM:SS:FFF'));
                fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging session ' num2str(vr.currentFileIdx2P,'%03i')  ' environment:' ],vr.worldNames{ vr.currentWorld });
                clear vr.tempTime
            end
            %  .. below is when we were recording, but it has been stopped by timing out, or by the user clicking '2P REC STOP' %
            vr.isRecording2P = false;
            vr.currentFileIdx2P = vr.currentFileIdx2P + 1;  % Bump the Idx counter for the scanimage file.
            vr.text(3).string = '2P REC START';
            vr.text(3).color = [0 1 0];
            vr.text(4).string = ['(' num2str(vr.recordingDuration2P) 'SEC, TO FILE ' num2str(vr.currentFileIdx2P,'%03i'), ')'];
            if vr.debugMode == 0
                vr.daqSess2PStartStop.outputSingleScan([0 1]);   % .. and stop the 2P recording.
            end
            vr.worldToggle = true;
        else
            %  .. if we were recording but it hasn't been stopped, just update the timer
            vr.text(4).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime2P) ) '/' num2str(vr.recordingDuration2P) ' SEC' ];
            vr.worldToggle = false; %prevents manual world switch during recording
        end
    end     
end
%%% (5c) Finally, if we are recording, we need to actually record the data %%%
if vr.debugMode ~= 1 && vr.isRecording
    if vr.debugMode == 0 && vr.isRecording2P
        frameCount2P = vr.daqSess2PFrameClock.inputSingleScan;
        sessionIdx2P = vr.currentFileIdx2P;
    else
        frameCount2P = 0;
        sessionIdx2P = 0;
    end
    %%% FILE FORMAT IS HERE: [time, xPos, yPos, dir, xVel, yVel, trialIdx2P, frameCount2P, isReward, amountReward, isTurnAround world index];
    %%% The first six I want to keep standard across experiments, hence the inclusion of x pos data, even though it is irrelevant to the linear track.
    measurementsToSave = [vr.timeElapsed-vr.recordingStartTime, vr.position(1), vr.position(2), vr.position(4), vr.velocity(1),...
                            vr.velocity(2), sessionIdx2P, frameCount2P, isReward, vr.currRewardDropSize, vr.turnaroundActive, vr.currentWorld ];
    if ~vr.recordingExists
        fwrite(vr.tempFileFid,length(measurementsToSave),'double');  % The very first entry (only) in the file is the number of fields.
    	vr.recordingExists = 1;                                      %  ..
    end
    fwrite(vr.tempFileFid,measurementsToSave,'double');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- TERMINATION code: executes after the ViRMEn engine stops. ---  %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = terminationCodeFun(vr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if vr.debugMode~=1 && vr.recordingExists
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Move the temp data files to the final directory %
    finalDataDir = uigetdir('C:/VR_Common/Data','Choose a folder to save the data');
    copyWorked = copyfile(vr.tempFileLocation, [finalDataDir, '/', vr.finalFileName, '.dat']);
    exper = copyVirmenObject(vr.exper); %#ok<NASGU>
    %txt output
    %txt file output
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','############################');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','### ViRMEn Session Info ####');
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','ViRMEn recording start:', char(vr.sessionTime(1)) );
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n','ViRMEn recording stop:', char(vr.sessionTime(2)) );
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n','ViRMEn session duration (hh:mm:ss):', datestr(vr.sessionTime(2)-vr.sessionTime(1),'HH:MM:SS'));
    fprintf(vr.tempFileFidTXT,'%-s\r\n\r\n','############################');
    
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','##### DAT File Format ######');
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','Note: First entry in DAT file is n of columns to extract');
    fprintf(vr.tempFileFidTXT,'%-s\t %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t   %-s\t  %-s\t  %-s\t\r\n\r\n','Dat file structure (column names):','Time','Xpos',...
            'Ypos','Dir','Xspeed','Yspeed','TrialID_2P','2P frame #','-isReward','rewardDropSize','-isTurn', 'WorldIndex');
    for i = 1:length(vr.worldNames)  
        fprintf(vr.tempFileFidTXT,'%-s\t %s\t %s\t\r\n\r\n','World index:',num2str(i),vr.worldNames{i});
    end
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','#############################');
    %add some 2P settings info
    fprintf(vr.tempFileFidTXT,'%-s\t\r\n\r\n','##### 2P Settings Info ######');
    
    %if sessions were recorded add some UI defined info
    if vr.currentFileIdx2P > 1
        UIdialogStr = cell(vr.currentFileIdx2P-1,3);
        for n = 1:vr.currentFileIdx2P-1
            UIdialogStr{n,1} = ['Session ' num2str(n,'%03i') ' Power (mW):' ];
            UIdialogStr{n,2} = ['Session ' num2str(n,'%03i') ' PMT gain:' ];
            UIdialogStr{n,3} = ['Session ' num2str(n,'%03i') ' FOV #:' ];
        end
        TXTInput = inputdlg( UIdialogStr,'Enter additional Session info',ones(numel(UIdialogStr),1),horzcat(cellstr(repmat('50',numel(UIdialogStr)/size(UIdialogStr,2),1))',...
            cellstr(repmat('450',numel(UIdialogStr)/size(UIdialogStr,2),1))',cellstr(repmat('1',numel(UIdialogStr)/size(UIdialogStr,2),1))'));
        for n = 1:vr.currentFileIdx2P-1
            fprintf( vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging ' UIdialogStr{n,1}], TXTInput{n});
            fprintf( vr.tempFileFidTXT,'%-s\t %-s\t\r\n',['Imaging ' UIdialogStr{n,2}], TXTInput{n+size(UIdialogStr,1)} );
            fprintf( vr.tempFileFidTXT,'%-s\t %-s\t\r\n\r\n',['Imaging ' UIdialogStr{n,3}], TXTInput{n+2*size(UIdialogStr,1)} );
        end
    end
    fprintf(vr.tempFileFidTXT,'%-s\t','############ END ############');
    %
    if copyWorked
        delete(vr.tempFileLocation);
        save([finalDataDir, '\', vr.finalFileName, '.mat'],'exper');  % ALso save the 'exper' structure with the data. 
        copyfile(vr.tempFileLocationTXT, [finalDataDir, '\', vr.finalFileName, '.txt']);
    else
        disp(['!!IMPORTANT NOTICE!! There was a problem moving temporary recording file to final data folder.' ...
            ' Temp files are still in present, in ' vr.tempFileDir]);
        save([vr.tempFileDir 'virmenTempData.mat'],'exper');     % Save the 'exper' structure to the temp folder, in this case.
    end 
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Close all file handles %
fclose all;
