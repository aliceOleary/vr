function code = linearTrack_TW
% linearTrackWithCylinders   Code for the ViRMEn experiment linearTrackWithCylinders.
%   code = linearTrackWithCylinders   Returns handles to the functions that ViRMEn
%   executes during engine initialization, runtime and termination.

% Begin header code - DO NOT EDIT
code.initialization = @initializationCodeFun;
code.runtime = @runtimeCodeFun;
code.termination = @terminationCodeFun;
% End header code - DO NOT EDIT


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- INITIALIZATION code: executes before the ViRMEN engine starts. %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = initializationCodeFun(vr)

% First, there are a couple of things that we only do when it is a real
% trial: initialise DAQ hardware, and data save file.
vr.debugMode = eval(vr.exper.variables.debugMode);
if ~vr.debugMode
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % (1) Initialise DAQ hardware %
    daqreset; %reset DAQ in case it's still in use by a previous Matlab program
    % (1a) Counter input for poition reading %
    vr.daqSessRotEnc = daq.createSession('ni');
    vr.counterCh = vr.daqSessRotEnc.addCounterInputChannel('VR_6321','ctr0','Position');
    vr.counterCh.EncoderType = 'X4';
    
    % (1b) Analog output to drive reward %
    vr.rewardAO = analogoutput('nidaq','VR_6321');
    addchannel(vr.rewardAO,0,'voltage');
    set(vr.rewardAO,'samplerate',10);
    vr.rewardPattern = [ ones(eval(vr.exper.variables.rewardDropSize),1).*5; 0; 0; 0; 0];
    
    % (1c) DIO for microscope start and stop signals %
    vr.daqSess2PStartStop = daq.createSession('ni');
    vr.daqSess2PStartStop.addDigitalChannel('VR_6321','port0/line0:1','outputonly');  % One line for on, one for off.
    vr.daqSess2PStartStop.outputSingleScan([0 0]);
    
    % (1d) 2P microscope frame clock input %
    vr.daqSess2PFrameClock = daq.createSession('ni');
    vr.daqSess2PFrameClock.addCounterInputChannel('VR_6321', 'ctr3', 'EdgeCount');  % Use counter 3, as ctr 1 (PFI3) has a broken contact on breakout (as of 2016-12-10), and ctr2 clashes with ctr0 when in 'position' mode.
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (2) Initialise datasaving location. %
if ~vr.debugMode
    vr.pathName = [vr.exper.variables.baseDataDirectory '\' vr.exper.variables.mouseName];
    vr.fileBaseName = [vr.exper.variables.mouseName '_' datestr(now,'yyyymmdd') '_'];
    d = dir( vr.pathName );   s = struct2cell(d);  f = s(1,:);
    existingDataToday = f( strncmp(vr.fileBaseName, f, length(vr.fileBaseName)) );
    if ~isempty(existingDataToday)
        trialIdxList = cellfun( @(s) str2double(s(end-5:end-4)), existingDataToday);  %% IMPORTANT: assumes end of file name is .._NN.dat (where N is numerical)
        vr.currentFileIdx = max(trialIdxList) + 1;
    else
        vr.currentFileIdx = 1;
    end
    vr.currentFid = fopen([vr.pathName, '\', vr.fileBaseName, num2str(vr.currentFileIdx,'%02i'), '.dat'],'w');
    vr.isStarting = true;
else
    vr.currentFileIdx = 1; % Need this even in debug mode, for correct text display.
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (4) Initialise some key variables in the vr. structure. %
% (4a) Rewards:
for ii=1:length( vr.exper.worlds{ vr.currentWorld }.objects );
    if strcmp( vr.exper.worlds{ vr.currentWorld }.objects{ii}.fullName, 'rewardZone' );   ind=ii;  break;   end
end
vr.rewardLocations = vr.exper.worlds{ vr.currentWorld }.objects{ind}.y;  % This contains a 1:nReward list of the y locations of the reward zone centres.
vr.nRewardZones = length(vr.rewardLocations);
vr.rewardIsActive = true(vr.nRewardZones,1);  % This will control: a) is it visible, b) can it drive the solenoid output
vr.nRewardsGiven = 0;  % This keeps track of the total number of rewards recieved by the mouse
vr.rewardTriggerRadius = eval( vr.exper.variables.rewardTriggerRadius );  % This sets how close to the reward centre to actually trigger the reward.

% (4b) Rewards: also need the 'raw' vertices and triangles that constitute the reward zones, so that they can be made invisible when inactive.
% This is tricky, as all of the different vertices and triangles are grouped together under one object, so we have to first
% retrive them, then split them up on the basis of their location in the world.
ind = vr.worlds{ vr.currentWorld }.objects.indices.rewardZone;
rewardVerInd = vr.worlds{ vr.currentWorld }.objects.vertices(ind,:);   % = the 'start' and 'stop' inds of the vertices that correspond to the rewardZone object
rVert = vr.worlds{ vr.currentWorld }.surface.vertices(:,rewardVerInd(1):rewardVerInd(2));  % These are now the actual vertices
rr = eval( vr.exper.variables.rewardRadius );  % This is the reward radius, NOTE, it is a string in vr., you need to 'eval' it.
vr.rewardTriIndByZone = cell(length(vr.rewardLocations),1);        % This is going to be the indices of triangles which belong to which reward zone 
for ii=1:length(vr.rewardLocations);                               % based on the position on the track.
    % For each reward zone, which vertices are within poistion +/- radius? 
    singleZoneVertInd = find(  rVert(2,:)>vr.rewardLocations(ii)-(rr*1.1) & rVert(2,:)<vr.rewardLocations(ii)+(rr*1.1)  )  +  rewardVerInd(1)  -   1;   % Need to add 'rewardVerInd(1)-1', so that it becomes an index into the *full* vertex array, not just that for reward zone.
    % .. unfortunately, only triangles, not vertices have the 'visibility' property, so we now need to work out  which traingles use these vertices ..
    temp = ismember( vr.worlds{ vr.currentWorld }.surface.triangulation, singleZoneVertInd ); 
    vr.rewardTriIndByZone{ii} = any( temp, 1 );   % And then store this index
end

% (4c) Some other general variables %
vr.isRecording = false;      % Are we currently recording data?
vr.recordingStartTime = [];  % Keep track of the start time for recording session
vr.recordingDuration = eval( vr.exper.variables.recordingDuration );  % How long to record data for?
vr.currentDirection = eval(vr.exper.variables.startDirection); % The current direction of the animal on the track, 1=N (increasing y), -1=S (decreasing y)
vr.turnaroundActive = false;     % Signals whether the animal is currently in the turnaround screen-blanked period 
vr.turnaroundStartTime = [];     % Will act as a timer, such that the turnaround blanking is of the correct duration.
vr.turnaroundDelay = eval( vr.exper.variables.turnaroundDelay );  % Get the turnaround blanking duration (user-set) in a more convenient format.
vr.movementGain = eval( vr.exper.variables.movementGain );
vr.monitorAspectRatio = eval( vr.exper.variables.monitorAspectRatio );
vr.perspectiveScale = eval( vr.exper.variables.perspectiveScale );


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (3) Initialise text boxes for controls %
vr.text(1).string = 'REC START';
vr.text(1).position = [-0.75 0.7];
vr.text(1).size = 0.1;
vr.text(1).color = [0 1 0];
vr.text(1).window = 5;
vr.text(2).string = ['(' num2str(vr.recordingDuration) 'SEC, TO FILE ' num2str(vr.currentFileIdx,'%02i'), ')'];
vr.text(2).position = [-0.75 0.5];
vr.text(2).size = 0.075;
vr.text(2).color = [1 1 1];
vr.text(2).window = 5;
vr.text(3).string = 'REC STOP';
vr.text(3).position = [-0.75 0];
vr.text(3).size = 0.1;
vr.text(3).color = [1 0 0];
vr.text(3).window = 5;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% (5) Finally, we actually need to initialise some things about the first 
%     run in the world.
% Set the starting reward to be inactive and invisible, and set vr.nextReward correctly %
if vr.currentDirection == 1         % Depending the starting direction, inactive the first reward the animal will see at the end of the track.
    vr.nextReward = 2;                
    vr.rewardIsActive(1) = false;
else 
    vr.nextReward = vr.nRewardZones-1;  
    vr.rewardIsActive(end) = false;
end
vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardTriIndByZone{  ~vr.rewardIsActive  }  ) = false;  % Set the inactive reward to be invisible.

assignin('base','vr',vr);   % For debugging purposes.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runtimeCodeFun(vr)


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% First, test if we are waiting in a turnaround invisble world epoch. %
% If so, keep the position at the end of the track, but also check whether the
% timer has timed out, in which case make the world visible again.
if vr.turnaroundActive
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer) > vr.turnaroundDelay
        % Turnaround blanking to be released: make the world visible .. %
        vr.worlds{ vr.currentWorld }.surface.visible(:) = true;
        vr.turnaroundActive = false;
        % .. with the exception of the reward on which the animal is currently sitting .. %
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardTriIndByZone{  ~vr.rewardIsActive  }  ) = false;      
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
    
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Scale displacement (gain) %
vr.dp(2) = vr.dp(2) * vr.movementGain;
% If using rotary encoder, need to flip the displacement when heading south 
% (so the same wheel direction keeps the mouse moving 'forward').
vr.dp(2) = vr.dp(2) * vr.currentDirection;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Test whether the the next active reward has been reached, if so, give it.
if vr.currentDirection==1;  hFun = @gt;  else  hFun = @lt;  end
if hFun( vr.position(2)+(vr.rewardTriggerRadius*vr.currentDirection), vr.rewardLocations( vr.nextReward ) )
    isReward = 1;
    % Drive solenoid %
    if~vr.debugMode
        putdata(vr.rewardAO,vr.rewardPattern);
    end
    % Make this reward zone invisible %
    vr.worlds{ vr.currentWorld }.surface.visible( vr.rewardTriIndByZone{vr.nextReward} ) = 0;
    vr.rewardIsActive( vr.nextReward ) = false;
    % Set the next zone to test for %
    vr.nextReward = vr.nextReward + vr.currentDirection;  % .. as vr.currentDireciton is coded as 1 or -1.
    % Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
else
    isReward = 0;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Has the end of the track been reached? %
if any(vr.nextReward==[0 vr.nRewardZones+1]);
    % If so, prevent any additional movement ..
    vr.dp(2) = 0;
    % Turn the animal around
    vr.position(4) = vr.position(4) - pi;  % Turn around
    vr.currentDirection = -vr.currentDirection;  % Also update the direction flag.
    % Make the world invisible, and set the timer for turning the world visible again %
    vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
    vr.turnaroundTimer = vr.timeElapsed;
    vr.turnaroundActive = true;
    % Reset the rewards as active %
    vr.rewardIsActive(:) = true;
    if vr.currentDirection == 1
        vr.nextReward = 2;                
        vr.rewardIsActive(1) = false;
    else 
        vr.nextReward = vr.nRewardZones-1;  
        vr.rewardIsActive(end) = false;
    end
end
    
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Control of recording %
if ~vr.isRecording 
    if vr.textClicked==1
        % If not recording, we need to check whether we need to start %
        vr.isRecording = 1;
        vr.recordingStartTime = vr.timeElapsed;
        vr.text(1).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) '/' num2str(vr.recordingDuration) ];
        if ~vr.debugMode
            vr.daqSess2PFrameClock.resetCounters;            % Make sure the frame clock is at zero ..
            vr.daqSess2PStartStop.outputSingleScan([1 0]);   % .. and start the 2P recording.
        end
    end
elseif vr.isRecording
    if vr.textClicked==3 || (vr.timeElapsed-vr.recordingStartTime)>vr.recordingDuration
        % If we are recording, check whether we need to stop %
        vr.isRecording = 0;
        % Deal with closing the last file and opening the next one, ready for the next recording 
        vr.currentFileIdx = vr.currentFileIdx + 1;
        if ~vr.debugMode
            vr.daqSess2PStartStop.outputSingleScan([0 1]);   % Stop the 2P recording.
            fclose(vr.currentFid); 
            exper = copyVirmenObject(vr.exper); %#ok<NASGU>
            save([vr.pathName, '\', vr.fileBaseName, num2str(vr.currentFileIdx) '.mat'],'exper'); % Save the 'exper' VR object as well.
            vr.currentFid = fopen([vr.pathName, '\', vr.fileBaseName, num2str(vr.currentFileIdx,'%02i')  '.dat'],'w');
        end
        % Reset the text on the control window %
        vr.text(1).string = 'REC START';
        vr.text(2).string = ['(' num2str(vr.recordingDuration) 'SEC, TO FILE ' num2str(vr.currentFileIdx,'%02i'), ')'];
    else 
        % If neither of the above, we are in a continuing recording, we will just update the timer text %
        vr.text(1).string = [ num2str( round(vr.timeElapsed - vr.recordingStartTime) ) '/' num2str(vr.recordingDuration) ];
    end
end
% Finally, we we are recording, we need to actually record the data %
if ~vr.debugMode && vr.isRecording
    measurementsToSave = [vr.timeElapsed-vr.recordingStartTime, vr.daqSess2PFrameClock.inputSingleScan, vr.position(2), vr.velocity(2), isReward];
    fwrite(vr.currentFid,measurementsToSave,'double');
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- TERMINATION code: executes after the ViRMEn engine stops. ---  %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = terminationCodeFun(vr)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Close file and DAQ objects correctly %
if ~vr.debugMode
    fclose all;
%     fid = fopen([vr.pathname '\' vr.filename '.dat']);
%     data = fread(fid,'double');
%     num = data(1);
%     data = data(2:end);
%     data = reshape(data,num,numel(data)/num);
%     assignin('base','data',data);
    fclose all;
    
%     vr.window.Dispose;
    
    
%     answer = inputdlg({'Rat number','Comment'},'Question',[1; 5]);
%     if ~isempty(answer)
%         comment = answer{2}; %#ok<NASGU>
%         save([vr.pathname '\' vr.filename '.mat'],'comment','-append')
%         if ~exist([vr.pathname '\' answer{1}],'dir')
%             mkdir([vr.pathname '\' answer{1}]);
%         end
%         movefile([vr.pathname '\' vr.filename '.mat'],[vr.finalPathname '\' answer{1} '\' vr.filename '.mat']);
%         movefile([vr.pathname '\' vr.filename '.dat'],[vr.finalPathname '\' answer{1} '\' vr.filename '.dat']);
%     end
%     
%     disp([answer{1} ' - ' num2str(sum(data(6,:)))])
end