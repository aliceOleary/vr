%Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runTime_linTrack(vr)

%% turn around control
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a turnaround invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.turnaroundActive
    if ~vr.worldToggle
        tempDelay = vr.switchDelay;
    else
        tempDelay = vr.turnaroundDelay;
    end
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer) > tempDelay
        % Turnaround blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(~vr.rewardLocInd{vr.currentWorld}) = true;
        vr.turnaroundActive = false;
        if ~vr.isRewardBlock
            %switch on rewards
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }   ) = true;
            %blank reward animal is standing on (in case he moves backwards)
            if vr.worldToggle && vr.currentDirection == 1
                vr.worlds{ vr.currentWorld }.surface.visible( vr.rewardTriIndByZone{ vr.currentWorld } {1} ) = false;
            elseif vr.worldToggle && vr.currentDirection == -1
                vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardTriIndByZone{ vr.currentWorld } {end} ) = false;
            end
        end
        vr.worldToggle = true; %release toggle
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

%% displacement
% (2) Get displacement and scale (gain)                                                                     %
vr.dp(2) = -vr.dp(2) * vr.movementGain;
% If using rotary encoder, need to flip the displacement when heading south 
% (so the same wheel direction keeps the mouse moving 'forward').
vr.dp(2) = vr.dp(2) * vr.currentDirection;
% Check that we haven't run outside of the track (virmen edge detection sometimes showing bugs).
if vr.position(2) < 1 
    vr.position(2) = 1;   vr.dp(2) = 0;
elseif vr.position(2) > vr.trackLength-4
    vr.position(2) = vr.trackLength-4;   vr.dp(2) = 0;   
end

%% rewards
% (3) Detection and control of rewards
% activate/deactivate reward
if vr.textClicked==8
    if vr.isRewardBlock && ~vr.isExit
        vr.text(8).string = 'REWARD ON';
        vr.text(8).color = [0 1 0];

        vr.worlds{ vr.currentWorld }.surface.visible(   vr.rewardLocInd{ vr.currentWorld }  ) = true;  
        vr.isRewardBlock = false;
    else
        vr.text(8).string = 'REWARD OFF';
        vr.text(8).color = [0.5 0.5 0.5];    
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false; 
        vr.isRewardBlock = true;
    end
end
%increase reward manually 
if ~vr.isExit && (vr.textClicked==6 || vr.textClicked==7)
    if vr.textClicked==6
        vr.currRewardDropSize = vr.currRewardDropSize + 1;
        if vr.currRewardDropSize > vr.maxPossRewardDropSize
            vr.text(6).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = vr.maxPossRewardDropSize;
        end
        if vr.currRewardDropSize > 1
            vr.text(7).color = [0 1 0];
        end
    end
    %decrease reward manually
    if vr.textClicked==7
        vr.currRewardDropSize = vr.currRewardDropSize - 1;
        if vr.currRewardDropSize <= 1
            vr.text(7).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = 1;
        end
        if vr.currRewardDropSize < vr.maxPossRewardDropSize
            vr.text(6).color = [0 1 0];
        end
    end
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
    vr.text(6).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
    vr.text(7).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
end

% Test whether the the next active reward has been reached, if so, give it.
vr.isReward = 0;
if vr.currentDirection==1;  hFun = @gt;  else  hFun = @lt;  end

if hFun( vr.position(2)+(vr.rewardTriggerRadius*vr.currentDirection), vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) )
    % This is the case where the animal has reached the reward zone %
    if ~vr.isRewardBlock
        vr.isReward = 1;
    end
    % Make this reward zone invisible %
    vr.worlds{ vr.currentWorld }.surface.visible( vr.rewardTriIndByZone{ vr.currentWorld }{vr.nextReward} ) = 0;
    vr.rewardIsActive{ vr.currentWorld }( vr.nextReward ) = false;
    % Set the next zone to test for %
    vr.nextReward = vr.nextReward + vr.currentDirection;  % .. as vr.currentDireciton is coded as 1 or -1.
    %shift back
    if vr.isfirstRewardShift
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) - 30;
        vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) - 30;
        vr.isfirstRewardShift = false;
        if ~vr.isRewardBlock
            %turn on the next one here
            vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{  vr.nextReward };
            vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;
        end
    end       
elseif ~vr.isRewardBlock && vr.textClicked==5
    % This is the case where the experimenter has clicked the 'REWARD NOW' button. %
    vr.isReward = 1;
end

% If there is a reward, drive solenoid %
if vr.isReward && vr.debugMode == 0 && strcmp(get(vr.rewardAO,'Running'),'Off')
    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
    %Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
end

%% End of track control
% (4) Has the end of the track been reached?
if any(vr.nextReward==[0 vr.nRewardZones{ vr.currentWorld }+1])
    % If so, prevent any additional movement ..
    vr.dp(2) = 0;
    
    % Turn the animal around
    vr.position(4) = vr.position(4) - pi;  % Turn around
    vr.currentDirection = -vr.currentDirection;  % Also update the direction flag.
    % Make the world invisible, and set the timer for turning the world visible again %
    vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
    
    vr.turnaroundTimer = vr.timeElapsed;
    vr.turnaroundActive = true;
    % Reset the rewards as active %
    vr.rewardIsActive{vr.currentWorld}(:) = true;
    
    if vr.currentDirection == 1
        vr.nextReward = 2;                
        vr.rewardIsActive{vr.currentWorld}(1) = false;
    else 
        vr.nextReward = vr.nRewardZones{ vr.currentWorld } - 1;  
        vr.rewardIsActive{vr.currentWorld}(end) = false;
    end
%     
%     if ~vr.isRewardBlock
%         vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;
%     end
end
end  


