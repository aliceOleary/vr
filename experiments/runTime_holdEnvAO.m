%Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runTime_holdEnvAO(vr)

%% turn around control
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a teleport invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.teleportActive
    % First, test whether we need to release the teleport blanking or not %
    if (vr.timeElapsed - vr.teleportTimer) > vr.switchDelay
        % teleport blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(~vr.rewardLocInd{vr.currentWorld}) = true;
        vr.teleportActive = false;
        if ~vr.isRewardBlock
            %switch on next possible reward
            vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;
        end
        
        vr.worldToggle = true; %release toggle
    else
        % Still in teleport: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

%% displacement
% (2) Get displacement and scale (gain)                                                                     %
vr.dp(2) = -vr.dp(2) * vr.movementGain;

% Check that we haven't run outside of the track (virmen edge detection
% sometimes showing bugs). 5, so that animal can't go backwards far enough
% to see end of sidewalls
if vr.position(2) < 5 
    vr.position(2) = 5;   vr.dp(2) = 0;
end

%% rewards
% (3) Detection and control of rewards
% activate/deactivate reward
if vr.textClicked==8
    if vr.isRewardBlock && ~vr.isExit
        vr.text(8).string = 'REWARD ON';
        vr.text(8).color = [0 1 0];

        vr.worlds{ vr.currentWorld }.surface.visible( vr.rewardLocInd{ vr.currentWorld } ) = true;  
        vr.isRewardBlock = false;
    else
        vr.text(8).string = 'REWARD OFF';
        vr.text(8).color = [0.5 0.5 0.5];    
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false; 
        vr.isRewardBlock = true;
    end
end
%increase reward manually 
if ~vr.isExit && (vr.textClicked==6 || vr.textClicked==7)
    if vr.textClicked==6
        vr.currRewardDropSize = vr.currRewardDropSize + 1;
        if vr.currRewardDropSize > vr.maxPossRewardDropSize
            vr.text(6).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = vr.maxPossRewardDropSize;
        end
        if vr.currRewardDropSize > 1
            vr.text(7).color = [0 1 0];
        end
    end
    %decrease reward manually
    if vr.textClicked==7
        vr.currRewardDropSize = vr.currRewardDropSize - 1;
        if vr.currRewardDropSize <= 1
            vr.text(7).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = 1;
        end
        if vr.currRewardDropSize < vr.maxPossRewardDropSize
            vr.text(6).color = [0 1 0];
        end
    end
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
    vr.text(6).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
    vr.text(7).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
end

%increase reward distance manually 
if ~vr.isExit && (vr.textClicked==13 || vr.textClicked==14)
    if vr.textClicked==13
        vr.interRewardDist = vr.interRewardDist + 25;
        vr.text(14).color = [0 1 0];
    end
    %decrease reward distance manually
    if vr.textClicked==14
        vr.interRewardDist = vr.interRewardDist - 25;
        if vr.interRewardDist <= 25
            vr.text(14).color = [0.5 0.5 0.5];
            vr.interRewardDist = 25;
        end
    end
    vr.text(13).string = ['FURTHER (' num2str(vr.interRewardDist) 'CM)'];
    vr.text(14).string = ['CLOSER (' num2str(vr.interRewardDist) 'CM)'];
end

% Test whether the the next active reward has been reached, if so, give it.
hFun = @gt; 
if hFun( vr.position(2)+(vr.rewardTriggerRadius), vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) )
  % This is the case where the animal has reached the reward zone %
    if ~vr.isRewardBlock && vr.rewardIsActive{vr.currentWorld}( vr.nextReward )
        vr.isReward = 1;
    end
    % Make this reward zone invisible %
    vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd  ) = 0;
    vr.rewardIsActive{ vr.currentWorld }( vr.nextReward ) = false;
    
    %first case normal shift of reward
    if ~vr.isfirstRewardShift
        tempShift = (vr.rewardLocations{vr.currentWorld }(vr.nextReward) - vr.position( 2 )) + vr.interRewardDist;
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{2})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{ 2 }) + tempShift;
        vr.rewardLocations{vr.currentWorld }(vr.nextReward) =  vr.rewardLocations{vr.currentWorld }( 2 ) + tempShift;   
    else
        %2nd case - shift back first reward indices + location
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) - 30;
        vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) - 30;
        vr.isfirstRewardShift = false;
        vr.nextReward = vr.nextReward + 1;  % .. as direction is always 1
    end
    
    if ~vr.isRewardBlock
        vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{ 2 };
        vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;  
    end   
       
elseif ~vr.isRewardBlock && vr.textClicked==5
    % This is the case where the experimenter has clicked the 'REWARD NOW' button. %
    vr.isReward = 2;
end

% If there is a reward, drive solenoid %
if vr.isReward >0 && vr.debugMode == 0 && strcmp(get(vr.rewardAO,'Running'),'Off')
    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
    %Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
    if vr.isReward == 1
        vr.rewardIsActive{vr.currentWorld} = 0; %animal can only recieve one reward per visit to reward zone. N.B this will need to be adjusted if nreward zones > 1
    end
     vr.isReward = 0;
end

%% End of track control
% (4) Has the end of the track been reached? 
if vr.position(2) >= vr.SewerTeleportPoint + vr.exper.worlds{vr.currentWorld}.startLocation(2)

    %reset to beginning
    vr.position(1) = 0;
    vr.position(2) = vr.position(2) - vr.SewerTeleportPoint; %back at original start +/- jitter
    
    vr.teleportActive = false;
    vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd   ) = false;
    %shift rewards appropriately
    vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld} { 2 })...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld} { 2 } ) - vr.SewerTeleportPoint;
    vr.rewardLocations{vr.currentWorld } ( 2 ) =  vr.rewardLocations{vr.currentWorld } ( 2 ) - vr.SewerTeleportPoint;
    
    vr.nextReward = 2;
    if ~vr.isRewardBlock
        vr.currRewardInd  =  vr.rewardTriIndByZone{ vr.currentWorld }{ 2 };
        vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;
    end
    
end


end

