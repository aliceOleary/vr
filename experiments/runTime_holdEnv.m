%Run time
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% --- RUNTIME code: executes on every iteration of the ViRMEn engine. %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function vr = runTime_holdEnv(vr)

%% turn around control
% (1) Control of pause/blackscreen while the animal gets turned around at the ends of the track.           %
% Test if we are waiting in a turnaround invisble world epoch. If so, keep the position 
% at the end of the track, but also check whether the timer has timed out, in which case 
% make the world visible again.
if vr.turnaroundActive
    % First, test whether we need to release the turnaround blanking or not %
    if (vr.timeElapsed - vr.turnaroundTimer) > vr.switchDelay
        % Turnaround blanking to be released: make the world visible except rewards .. %
        vr.worlds{ vr.currentWorld }.surface.visible(~vr.rewardLocInd{vr.currentWorld}) = true;
        vr.turnaroundActive = false;
        if ~vr.isRewardBlock
            %switch on next possible reward
            vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;
        end
        
        vr.worldToggle = true; %release toggle
    else
        % Still in turnaround: keep the movement at zero (keep the animal at the beginning of the track, regardless of wheel movement). %
        vr.dp(2) = 0;
    end
end

%% displacement
% (2) Get displacement and scale (gain)

vr.dp(2) = -vr.dp(2) * vr.movementGain;

% Check that we haven't run outside of the track (virmen edge detection
% sometimes showing bugs). 5, so that animal can't go backwards far enough
% to see end of sidewalls
if vr.position(2) < 5 
    vr.position(2) = 5;   vr.dp(2) = 0;
end

%% rewards
% (3) Detection and control of rewards
% activate/deactivate reward
if vr.textClicked==8
    if vr.isRewardBlock && ~vr.isExit
        vr.text(8).string = 'REWARD ON';
        vr.text(8).color = [0 1 0];

        vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd  ) = true;  
      
        vr.isRewardBlock = false;
    else
        vr.text(8).string = 'REWARD OFF';
        vr.text(8).color = [0.5 0.5 0.5];    
        vr.worlds{ vr.currentWorld }.surface.visible(  vr.rewardLocInd{ vr.currentWorld }  ) = false; 
        vr.isRewardBlock = true;
    end
end
%increase reward manually 
if ~vr.isExit && (vr.textClicked==6 || vr.textClicked==7)
    if vr.textClicked==6
        vr.currRewardDropSize = vr.currRewardDropSize + 1;
        if vr.currRewardDropSize > vr.maxPossRewardDropSize
            vr.text(6).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = vr.maxPossRewardDropSize;
        end
        if vr.currRewardDropSize > 1
            vr.text(7).color = [0 1 0];
        end
    end
    %decrease reward manually
    if vr.textClicked==7
        vr.currRewardDropSize = vr.currRewardDropSize - 1;
        if vr.currRewardDropSize <= 1
            vr.text(7).color = [0.5 0.5 0.5];
            vr.currRewardDropSize = 1;
        end
        if vr.currRewardDropSize < vr.maxPossRewardDropSize
            vr.text(6).color = [0 1 0];
        end
    end
    vr.rewardPattern = [ ones(vr.currRewardDropSize,1).*5; 0; 0; 0; 0];
    vr.text(6).string = ['MORE (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
    vr.text(7).string = ['LESS (' num2str(vr.currRewardDropSize*1000/vr.sRateSolanoid) 'MS)'];
end

%increase reward distance manually 
if ~vr.isExit && (vr.textClicked==13 || vr.textClicked==14)
    if vr.textClicked==13
        vr.interRewardDist = vr.interRewardDist + 25;
        vr.text(14).color = [0 1 0];
    end
    %decrease reward distance manually
    if vr.textClicked==14
        vr.interRewardDist = vr.interRewardDist - 25;
        if vr.interRewardDist <= 25
            vr.text(14).color = [0.5 0.5 0.5];
            vr.interRewardDist = 25;
        end
    end
    vr.text(13).string = ['FURTHER (' num2str(vr.interRewardDist) 'CM)'];
    vr.text(14).string = ['CLOSER (' num2str(vr.interRewardDist) 'CM)'];
end

% Test whether the the next active reward has been reached, if so, give it.
vr.isReward = 0;
if vr.position(2)+ 3 > vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) 
%if vr.position(2)+(vr.rewardTriggerRadius) > vr.rewardLocations{ vr.currentWorld }( vr.nextReward ) 
    % This is the case where the animal has reached the reward zone %
    if ~vr.isRewardBlock
        vr.isReward = 1;
    end
    % Make this reward zone invisible %
    vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd  ) = 0;
    vr.rewardIsActive{ vr.currentWorld }( vr.nextReward ) = false;
    
    %first case normal shift of reward
    if ~vr.isfirstRewardShift
        tempShift = (vr.rewardLocations{vr.currentWorld }(vr.nextReward) - vr.position( 2 )) + vr.interRewardDist;
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{2})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{ 2 }) + tempShift;
        vr.rewardLocations{vr.currentWorld }(vr.nextReward) =  vr.rewardLocations{vr.currentWorld }( 2 ) + tempShift;   
    else
        %2nd case - shift back first reward indices + location
        vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1})...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) - 30;
        vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) - 30;
        vr.isfirstRewardShift = false;
        vr.nextReward = vr.nextReward + 1;  % .. as direction is always 1
    end
    
    if ~vr.isRewardBlock
        vr.currRewardInd =  vr.rewardTriIndByZone{ vr.currentWorld }{ 2 };
        vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;  
    end   
       
elseif ~vr.isRewardBlock && vr.textClicked==5
    % This is the case where the experimenter has clicked the 'REWARD NOW' button. %
    vr.isReward = 1;
end

% If there is a reward, drive solenoid %
if vr.isReward && vr.debugMode == 0 && strcmp(get(vr.rewardAO,'Running'),'Off')
    putdata(vr.rewardAO,vr.rewardPattern);   start(vr.rewardAO);
    if vr.reward_tone
        sound(vr.reward_tone_sound.sound,vr.reward_tone_sound.fs)
    end
    %Keep a record of how many rewards %
    vr.nRewardsGiven = vr.nRewardsGiven + 1;
end

if vr.DetectLicking==1
    if vr.PhotoLickDetection.Licks  
            vr.licksCurrentTrial=vr.licksCurrentTrial+1;
            vr.text(16).string = ['NUMBER OF LICKS: ' num2str(vr.licksCurrentTrial)];
    end
end
%% End of track control
% (4) Has the end of the track been reached? 

if vr.position(2) >= vr.teleportPoint + vr.exper.worlds{vr.currentWorld}.startLocation(2)

    %reset to beginning
    vr.position(1) = 0;
    vr.position(2) = vr.position(2) - vr.teleportPoint; %back at original start +/- jitter
    
    vr.turnaroundActive = false;
    vr.worlds{ vr.currentWorld }.surface.visible(  vr.currRewardInd   ) = false;
    %shift rewards appropriately
    vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld} { 2 })...
            = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld} { 2 } ) - vr.teleportPoint;
    vr.rewardLocations{vr.currentWorld } ( 2 ) =  vr.rewardLocations{vr.currentWorld } ( 2 ) - vr.teleportPoint;
    
    vr.nextReward = 2;
    if ~vr.isRewardBlock
        vr.currRewardInd  =  vr.rewardTriIndByZone{ vr.currentWorld }{ 2 };
        vr.worlds{ vr.currentWorld }.surface.visible( vr.currRewardInd ) = true;
    end
    
end


end

