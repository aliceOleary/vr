function vr = worldSwitch(vr)
%control of world switch for standard VR

%% world control

if strcmp(vr.text(10).string,'HOLD')
    vr.currentWorld = strcmp(vr.worldNames,'linearTrack');
    vr.text(10).string = 'TRACK';
else
    vr.currentWorld = strcmp(vr.worldNames,'holdingEnvironment');
    vr.text(10).string = 'HOLD';
end
%blank both worlds
vr.worlds{ vr.currentWorld }.surface.visible(:) = false;
vr.worlds{ ~vr.currentWorld }.surface.visible(:) = false;
%activate things for transfer delay - use same logic as for turn around
vr.turnaroundTimer = vr.timeElapsed;
vr.turnaroundActive = true;
%set to start pos
vr.position = vr.exper.worlds{vr.currentWorld}.startLocation;
vr.currentDirection = eval(vr.exper.variables.startDirection);

if ~vr.isRewardBlock
    %shift first reward after flip, so it is right in front of animal
    if vr.currentDirection == 1
        vr.nextReward = 1;
    else
        vr.nextReward = vr.nRewardZones{ vr.currentWorld };
    end
    %shift reward
    vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1})...
        = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) + 30;
    vr.rewardLocations{vr.currentWorld }(1) =  vr.rewardLocations{vr.currentWorld }(1) + 30;
    vr.isfirstRewardShift = true; %flag shift
else
    if vr.currentDirection == 1
        vr.nextReward = 2;
    else
        vr.nextReward = vr.nRewardZones{ vr.currentWorld } - 1;
    end
end
%in the hold environment the reward needs to be moved back to original
%location
if strcmp(vr.text(10).string,'HOLD')
    vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{2})...
        = vr.worlds{ vr.currentWorld }.surface.vertices(2,vr.rewardVertices{vr.currentWorld}{1}) + vr.interRewardDist;
    vr.rewardLocations{vr.currentWorld }(2) =  vr.rewardLocations{vr.currentWorld }(1) + vr.interRewardDist;
end

%toggle will block clicking switch world in delay
vr.worldToggle = false;


end


